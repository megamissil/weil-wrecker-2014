<?php
    include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
    $aFeatured = pageByCategory( 'AUCTION' , 'ANY' , 0 , 1 , 'PUBL_ASC');
    $cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php include('header.php'); ?>

<main class="container">
    <section class="slider-wrapper"><!-- START SLICK SLIDER -->
       <div>
            <figure class="image-cover">
                <img src="img/slider/slider-6.jpg" alt="" />
                <figcaption>
                    <h2>Serving Greater Birmingham Since 1977</h2>
                    <a href="about.php" class="button-icon radius hidden-xs">Read More <span><img src="img/read-more-icon-black.png" alt="read more" /></span></a>
                </figcaption>
            </figure>
        </div>

        <div>
            <figure class="image-cover">
                <img src="img/slider/slider-10.jpg" alt="" />
                <figcaption>
                    <h2>35+ Acre Facilities</h2>
                    <a href="services.php" class="button-icon radius hidden-xs">Read More <span><img src="img/read-more-icon-black.png" alt="read more" /></span></a>
                </figcaption>
            </figure>
        </div>
        <div>
            <figure class="image-cover">
                <img src="img/slider/slider-3.jpg" alt="" />
                <figcaption>
                   <h2>Indoor Storage for 532 Vehicles</h2>
                    <a href="storage.php" class="button-icon radius hidden-xs">Read More <span><img src="img/read-more-icon-black.png" alt="read more" /></span></a>
                </figcaption>
            </figure>
        </div>
        <div>
            <figure class="image-cover">
                <img src="img/slider/slider-15.jpg" alt="" />
                <figcaption>
                   <h2>Outdoor Storage for 1244 Vehicles </h2>
                    <a href="storage.php" class="button-icon radius hidden-xs">Read More <span><img src="img/read-more-icon-black.png" alt="read more" /></span></a>
                </figcaption>
            </figure>
        </div>
        <div>
            <figure class="image-cover">
                <img src="img/slider/slider-11.jpg" alt="" />
                <figcaption>
                    <h2>Over 120 Towing and Recovery Vehicles</h2>
                    <a href="recovery.php" class="button-icon radius hidden-xs">Read More <span><img src="img/read-more-icon-black.png" alt="read more" /></span></a>
                </figcaption>
            </figure>
        </div>
        <div>
            <figure class="image-cover">
                <img src="img/slider/slider-13.jpg" alt="" />
                <figcaption>
                    <h2>We Serve Multiple Municipalities</h2>
                    <a href="impounds.php" class="button-icon radius hidden-xs">Read More <span><img src="img/read-more-icon-black.png" alt="read more" /></span></a>
                </figcaption>
            </figure>
        </div>
        <div>
            <figure class="image-cover">
                <img src="img/slider/slider-12.jpg" alt="" />
                <figcaption>
                    <h2>Upcoming Auctions</h2>
                    <a href="calendar-auction-web.pdf" class="button-icon radius hidden-xs" target="_blank">View the Calendar <span><img src="img/slider/cta-auction-icon.svg" alt="auction" /></span></a>
                </figcaption>
            </figure>
        </div>
    </section><!-- END SLICK SLIDER -->
        
    <section class="logos-banner hidden-xs"><!-- START LOGOS BANNER -->
        <img src="img/logos-banner.jpg" alt="" width="100%"/>
    </section>

    <!-- START PANELS SECTION -->
    <section class="panels-index hidden-xs">
        <div class="row">
            <div class="col-sm-6 col-md-3 panels">
                <a href="towing.php">
                    <img src="img/tow-panel.jpg" alt="" width="100%"/>
                    <article class="panel-caption">
                        <h3>Towing and Recovery</h3>
                        <p class="hidden-xs hidden-sm hidden-md">Our fleet includes light, medium, and heavy duty rollbacks and wreckers that are operated by professionally trained drivers.</p>
                    </article>
                </a>
            </div>
            <div class="col-sm-6 col-md-3 panels">
                <a href="impounds.php">
                    <img src="img/impound-panel.jpg" alt="" width="100%"/>
                    <article class="panel-caption">
                        <h3>Impound Release</h3>
                        <p class="hidden-xs hidden-sm hidden-md">An impounded vehicle requires a release from the muncipality from which the impounding tow was ordered.</p>
                    </article>
                </a>
            </div>

            <div class="clearfix visible-sm-block"></div>

            <div class="col-sm-6 col-md-3 panels">
                <a href="auctions.php">
                    <img src="img/auction-panel.jpg" alt="" width="100%"/>
                    <article class="panel-caption">
                        <h3>Vehicle Auctions</h3>
                        <p class="hidden-xs hidden-sm hidden-md">Abandoned vehicle auctions are held on the 1<sup>st</sup> & 3<sup>rd</sup> Wednesday of every month at 11:00AM.</p>
                    </article>
                </a>
            </div>
            <div class="col-sm-6 col-md-3 panels">
                <a href="services.php">
                    <img src="img/services-panel.jpg" alt="" width="100%"/>
                    <article class="panel-caption">
                        <h3>Services Provided</h3>
                        <p class="hidden-xs hidden-sm hidden-md">We provide many services, including: towing, vehicle recovery, storage, vehicle impounds, auctions, and equipment sales.</p>
                    </article>
                </a>
            </div>
        </div>
    </section>
    <!-- END PANELS SECTION -->

    <!-- START WELCOME SECTION -->
    <section class="welcome">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">

                    <h2><span class="headline-icon"><img src="img/crane-icon-white.svg" alt="welcome icon" /></span>Need Immediate Assistance? <br class="visible-xs"> No Problem.</h2>

                    <p id="welcome-msg">The Mission of Weil Wrecker Service, Professional Towing Service, C &amp; W Wrecker Service, and B &amp; W Wrecker Service is to provide the best towing and recovery service possible.  From every day vehicle towing to big truck towing, it is our purpose to fulfill our mission in a timely, professional, and courteous manner.<br><br>

                    Whatever we do or say will be based upon a foundation of honesty, integrity, and morality.</p><br />
                    <div class="row">
                        <div class="col-lg-4 pull-right hidden-sm hidden-xs">
                            <a href="services.php" id="see-services" class="button radius">See our services</a>
                        </div>
                        <div class="col-center visible-sm visible-xs">
                            <a href="services.php" id="see-services" class="button radius">See our services</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-md-7 col-lg-7">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-md-5 col-lg-5">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append-touch visible-xs"><!--MOBILE SURVEY SECTION -->
                    <a href="satisfaction-survey.php">
                        <h2>Happy with your experience?</h2>
                    </a>
                </div>
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <h2>Auction Vehicles of Interest</h2>
                       <?php 
                            $current =  productCategory( 'CURRENT_AUCTION' , 'RAND' , $_GET['search'] , $_GET['cnt'] , 1 );
                            foreach($current['products']['results'] as $cKey => $product) {
                                $product = (object) $product; ?>
                                <div class="sub-slider"><!-- START SLICK SLIDER -->
                                    <a href="/auctions/" class="auction-item"><img src="<?= $product->thumb ?>" alt="vehicle" width="100%"></a>
                                </div>
                                <article class="sidebar-featured">
                                    <h3><?php echo $product->addl_add_make . ' ' . $product->addl_addl_model; ?></h3>
                        <?php } ?>
                            <p>Over 100 new cars, trucks, and other vehicles are added weekly.</p>

                            <a href="/auctions" class="button radius">See all vehicles</a>
                        </article>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include('footer.php'); ?>