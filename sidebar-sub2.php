<?php
require_once($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');

?>

    <div class="faq-side">
        <a href="/faq.php"><span>FAQ</span>
        <p>Find more information on our Frequently Asked Questions page</p></a>
    </div>
<h2>Contact Us</h2>

<a href="https://www.google.com/maps/place/3400+2nd+Ave+S,+Birmingham,+AL+35222/@33.520021,-86.784107,17z/data=!4m2!3m1!1s0x88891bad7e1b5d4f:0x44d101a5f540df2c" target="_blank"><img src="/img/weil-map.png" alt="find us on Google" width="100%" /></a>
<article class="logos">
    <img src="/img/weil-logo.png" alt="" />
    <p class="yellow">(205) 251-4060</p>
    <p>3400 Second Avenue South, <br />Birmingham, AL 35222</p>
</article>
<article class="logos">
    <img src="/img/protow-logo.png" alt="" />
    <p class="yellow">(205) 985-7177</p>
    <p>1872 Lorna Crest Road, <br />Hoover, AL 35244</p>
</article>

<a href="https://www.google.com/maps/place/1872+Lornacrest+Rd,+Birmingham,+AL+35244/@33.3679256,-86.814123,14z/data=!4m2!3m1!1s0x88892224d0cfd18b:0x400c2cfa34adec1c?hl=en" target="_blank"><img src="/img/protow-map.png" alt="pro tow map" width="100%" /></a>

<h2>Auction Vehicles of Interest</h2>

<?php 
$current =  productCategory( 'CURRENT_AUCTION' , 'RAND' , $_GET['search'] , $_GET['cnt'] , 1 );
foreach($current['products']['results'] as $cKey => $product) {
    $product = (object) $product;
?>
<div class="sub-slider"><!-- START SLICK SLIDER -->
    <div>
        <a href="/auctions/" class="auction-item"><img src="<?= $product->thumb ?>" alt="vehicle" width="100%"></a>
    </div>
</div>
<article class="sidebar-featured">
    <h3><?php echo $product->addl_add_make . ' ' . $product->addl_addl_model; ?></h3>
<?php } ?>
    <p>Over 100 new cars, trucks, and other vehicles are added weekly.</p>

    <a href="/auctions/" class="button radius">See all vehicles</a>
</article>