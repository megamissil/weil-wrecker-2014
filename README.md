![Alt text](http://afcprimed.zeekeeinteractive.com/img/zeekee-readme.jpg)

# Weil Wrecker Repo #

##### Description
2014 Weil Wrecker redesign. Bootstrap 3 using Sass
##### CMS
Tyfoon
##### FTP
ziws9.onyxlight.net/zeekee9
##### Responsive
Yes _(Bootstrap v3.1.1 SASS)_

##### Date of Repo Creation
JUL 11 2014

## Project Team ##
**Designer**
Martin Castre

**Developer**
Martin Castre

**AE**
David Barry



***

### When to contribute ###

* Any change/edits to code, no matter how minor
* Adding new assets such as images, plugins, etc.
* General code audit and review
* Collaboration between team members

### How do I get set up? ###

To get the most out of the Git experience, check out the [Bitbucket tutorial here](https://www.atlassian.com/git/tutorial/git-basics)

* Create a new project folder on your machine to drop site files in
* Open Terminal (Mac) or Command Prompt (Windows)
* cd to the folder
* Do a git clone to copy the entire project to your local machine
* Make your edits in your text editor
* do a git add . or git add .file_name for individual files
* then git commit -m "Commit Message (This must be a quick but concise description of the edits you made)"
* finally push your changes by doing a git push -u origin master
* Please see Martin or Colin for issues, conflicts, ideas, or questions.