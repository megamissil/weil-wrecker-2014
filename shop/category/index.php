<?php
if ( substr( $_GET['cat'] , -1 ) == '/' ) { $_GET['cat'] = substr( $_GET['cat'], 0, -1 ); }            
$category = (object) productCategory( str_replace('-', '_', $_GET['cat']), 'CATEGORY_ASC', $_GET['search'], $_GET['cnt'], $productsPerPage);

if (!empty($category->title) ) {	
	$cDir = '/shop';		
	$output->title = html_link( $cDir , 'Shop' );	
	foreach ( $category->directory AS $nKey => $cValue ) {		
		$cDir .= '/' . str_replace('_', '-', strtolower($cValue));
		$output->title .= ' > ' . html_link ( $cDir , $category->directory_titles[$nKey]);		
	}						
} else {
	$output->title = 'Shop';
}

/**
 * client over-ride to remove auctions.
 * 
 */
if (strtoupper($output->title) == 'SHOP') {
	redirect('/shop/parts/');
	//$category = (object) productCategory( 'PARTS', 'CATEGORY_ASC', $_GET['search'], $_GET['cnt'], $productsPerPage);
}
 
include dirname(__FILE__) . '/../_header.php';

include "category/$framework/index.php";

include dirname(__FILE__) . '/../_footer.php';
