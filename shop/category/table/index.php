<?php 
include '_header.php';
?>
<div class="shop-pg">
	<div class="row">
		<div class="large-12 columns">
			<div class="main-container"> <!-- MAIN CONTAINER FOR CONTENT -->
				<div class="row">
					<div class="large-10 large-offset-2 columns right mod-pad">
						<div class="divider-bar">   <!-- TITLE BAR FOR SUBPAGES -->
							<h1><?=$aOutput['title'] ?></h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="large-2 columns hide-for-small"> <!-- SIDE BAR NAVIGATION -->
						<?php
							include"../sidebar.php";
						?>
					</div> <!-- SIDE BAR NAVIGATION END -->
					
					<div class="large-10 columns">
							<div class="selection-wrap">								
									<?php
									if ( sizeof( $aCategory['sub'] ) > 0 ) {
										$loopCount = 0;
										foreach ( $aCategory['sub'] AS $cKey => $aValue ) {
											empty($_GET['cat']) ? $url = strtolower($aValue['code']) : $url = strtolower($_GET['cat'] . '/' . $aValue['code']);
											$loopCount++;
											echo ($loopCount === 1) ? '<div class="row">' : false;
										?>
										<div class="large-<?= $frameworkSettings->foundation->categoryColumns ?> <?= $frameworkSettings->foundation->categoryCentered ?> columns href" data-href="<?= $url ?>">
											<?php echo !empty($aValue['thumb']) ? '<img src="' . $aValue['thumb'] . '">' : '<img src="/' . $rootDirectory . '/images/missing.gif">'; ?>											
											<h3>
												<?= $aValue['title']; ?>
											</h3>
											<?php echo !empty($aValue['msg']) ? $aValue['msg'] : false; ?>
										</div>
										<?php	
										echo ($loopCount === 4) ? '</div>' : false;
										($loopCount === 4) ? $loopCount = 0 : false;
										}
										echo ($loopCount !== 4) ? '</div>' : false;
									}

									if ( $aCategory['products']['results'] > 0 ) {
										foreach ( $aCategory['products']['results'] AS $nKey => $product) { 
											$product = (object) $product;
											$loopCount = 0;
											empty($_GET['cat']) ? $url = '/' . $rootDirectory . '/' . $product->tyfoonurl : $url = strtolower('/' . $rootDirectory . '/' . $_GET['cat'] . '/' . $product->tyfoonurl);
											echo ($loopCount === 1) ? '<div class="row">' : false;
										?>
										<div class="large-<?= $frameworkSettings->foundation->productColumns ?> columns href" data-href="<?= $url ?>">
											<div class="shop-prod">
												<div class="view">View Product</div>												
												<img src="<?= $product->thumb ?>" alt="">
												<h4>
													<?= $product->title ?>
												</h4>
												<p>
													<?= _CURRENCY_SYMBOL ?> <?= number_format( $product->price_retail, 2) ?>
												</p>	
											</div>				
										</div>
										<?php 
										echo ($loopCount === 4) ? '</div>' : false;
										($loopCount === 4) ? $loopCount = 0 : false;
										}
										echo ($loopCount !== 4) ? '</div>' : false;
									}
									?>
									
								</div>
							</div>
						</div>
					</div>
					<div class="large-2 columns show-for-small"> <!-- SIDE BAR NAVIGATION -->
						<?php
							
						?>
					</div> <!-- SIDE BAR NAVIGATION END -->
				</div>
			</div> <!-- MAIN CONTAINER FOR CONTENT -->
		</div>
	</div> <!-- ROW END -->
</div>

<?php
	
include "_footer.php";
