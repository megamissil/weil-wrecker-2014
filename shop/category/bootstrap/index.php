<h1>
	<span class="headline-icon"><img src="/img/tow-icon.png" alt="towing icon" /></span>
	<?= $output->title ?>
</h1>
<br />
<?php 
$item = 0;
foreach ($category->sub as $n => $subCategory) {
	$item++;
	echo ($item == 1) ? '<div class="row">' : false;
?>
	<div class="col-sm-3">	
		<div class="panel panel-default">				
			<div class="panel-body text-center">
				<?php echo html_link('/' . $webroot . '/' . $n . '/', '<img src="/tyfoon/site/products/images/' . $subCategory['photo_path'] . '" class="img img-responsive">'); ?><br />					
			</div>
			<div class="panel-footer text-center">
				<?php echo html_link('/' . $webroot . '/' . $n . '/', $subCategory['title']); ?><br />
			</div>
		</div>							
	</div>
	<?php
	echo ($item == 4) ? '</div>' : false;
	($item == 4) ? $item = 0 : false;
}
echo ($item > 0 && $item < 4) ? '</div>' : false;

if (sizeof($category->products['results']) > 0) {
	$item = 0;
	foreach ($category->products['results'] as $n => $product) {
		$item++;
		$product = (object) $product;
		$link = './' . strtolower($product->sku) . '~' . urlencode(strtolower(trim($product->title))) . '.htm';
		echo ($item == 1) ? '<div class="row">' : false;
	?>
		<div class="col-sm-3">
			<div class="thumbnail">
 				<a href="<?= $link ?>">
      				<img src="<?= $product->image; ?>" class="img img-responsive" /> 
	      			<br />
    	  			<?= $product->title; ?>
      			</a>
				<br />
				<?php if ($member['memberid'] > 0) { ?>
				<strong>
					<?= _CURRENCY_SYMBOL ?> <?= number_format($product->price_retail, 2); ?>
				</strong>
				<?php } ?>
    		</div>			
    	</div>
<?php 
		echo ($item == 4) ? '</div>' : false;
		($item == 4) ? $item = 0 : false;
	}
	echo ($item > 0 && $item < 4) ? '</div>' : false;
} else {
	echo '<div class="row col-sm-12 well">There are no products associated with this category.</div>';
}
?>