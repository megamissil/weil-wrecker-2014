
<div class="row">
	<div class="small-12 columns">
		<h1>
			<?= $output->title ?>
		</h1>
	</div>
</div>

<div class="row">					
	<div class="small-12 columns">

	<?php
	
	if ( sizeof($category->sub) > 0) {
		$breakRow = (12/$categoryColumnn);
		$loopCount = 0;
		foreach ($category->sub AS $cKey => $aValue ) {
			$aValue['code'] = str_replace('_','-', $aValue['code']);											
			empty($_GET['cat']) ? $url = '/' . $webroot . '/' . strtolower($aValue['code']) : $url = '/' . $webroot . '/' . strtolower($_GET['cat'] . '/' . $aValue['code']);
			$loopCount++;
			echo ($loopCount === 1) ? '<div class="row">' : false;
		?>
		<div class="small-<?= $categoryColumnn ?> text-center columns href" data-href="<?= $url ?>">
			<?php echo !empty($aValue['thumb']) ? '<img src="' . $aValue['thumb'] . '">' : '<img src="/' . $webroot . '/images/missing.gif">'; ?>											
			<h3>
				<?= $aValue['title']; ?>
			</h3>
			<?php echo !empty($aValue['msg']) ? $aValue['msg'] : false; ?>
		</div>
		<?php	
		echo ($loopCount === $breakRow) ? '</div>' : false;
		($loopCount === $breakRow) ? $loopCount = 0 : false;
		}
	echo ($loopCount !== $breakRow) ? '</div>' : false;
	}
	

	if ($category->products['results'] > 0 ) {
		$loopCount = 0;
		$breakRow = (12/$productsColumnn);

		foreach ($category->products['results'] AS $nKey => $product) { 
			$loopCount++;
			$product = (object) $product;			
			empty($_GET['cat']) ? $url = '/' . $webroot . '/' . $product->tyfoonurl : $url = strtolower('/' . $webroot . '/' . $_GET['cat'] . '/' . $product->tyfoonurl);
			echo ($loopCount === 1) ? '<div class="row">' : false;
			($category->products['total'] === sizeof($category->products['results'])) ? $end = 'end' : false;
		?>
		<div class="small-<?= $productsColumnn ?> <?= $end ?> text-center left columns href" data-href="<?= $url ?>">			
			<img src="<?= $product->image ?>" alt="">
			<h4>
				<?= $product->title ?>
			</h4>
			<p>
				<?= _CURRENCY_SYMBOL ?> <?= number_format( $product->price_retail, 2) ?>
			</p>	
		</div>
			<?php 
			echo ($loopCount === $breakRow) ? '</div>' : false;
			($loopCount === $breakRow) ? $loopCount = 0 : false;
		}
		echo ($loopCount !== $breakRow) ? '</div>' : false;
	}
	?>	
		
	</div>
</div>

<div class="row">					
	<div class="small-12 text-right columns">
	<?php 
	$pages = (object) pagination($_GET['cnt'], $category->products['total'], $productsPerPage); ?>
		<div class="pagination-centered"> 
			<ul class="pagination"> 
		  		<li class="arrow"><a href="?cnt=<?= $pages->prev ?>">&laquo;</a></li>
		  		<?php foreach ($pages->pages as $n => $page) { 
		  			$page = (object) $page;
		  			?>
					<li class="<?php echo $page->cnt == $_GET['cnt'] ? 'current' : false; ?>"><a href="?cnt=<?= $page->cnt ?>"><?= $page->pg ?></a></li>
				<?php } ?>
		    	<li class="arrow"><a href="?cnt=<?= $pages->next ?>">&raquo;</a></li>
		  	</ul>
		</div>
	</div>
</div>