<?php
// ============================================================================
// ============================================================================
//           NAME: Check Out - Shipping Option Included
//   DATE CREATED: 
//  DATE MODIFIED: 
//        PURPOSE: 
//                
// ============================================================================
// ============================================================================

require_once '../connect.php';

$aBasket = ordersCartContents();

include 'variables.php';

if ( FOONSTER_ORDERS_SECURE == 1 ) {
	( $_SERVER['HTTPS'] != "on" ) ? redirect( "https://$_SERVER[HTTP_HOST]/$rootDirectory/basket" ) : false;
}   

// OFFLINE CHECK

if (FOONSTER_ORDERS_DISABLE === 1) { include 'offline.php';  exit; }

if ( $_POST['cSubmit'] == 'update basket' ) {
	ordersUpdateCart( $_POST );
	redirect( '/' . $webroot . '/basket');
} else { 
         
	if ( $aBasket['cartid'] == 0 ) { 	
		setcookie( FOONSTER_LICENSE_KEY , '' , 86400 , '/' ); 
		redirect( '/');
	}

	if ( $aMember['handle'] != NULL ) {
		$cBasket = ucfirst( $aMember['handle'] ) . "'s basket : " . html_link('basket.php' , $aBasket['items'] . ' items' );
	} else {
		$cBasket = 'My Basket : ' . html_link('basket.php' , $aBasket['items'] . ' items');
	}

   	if ( $_REQUEST['step'] == 'gotocheckout' ) {
   
		ordersUpdateCart( $_POST );
		$aBasket = ordersCartContents();
		$aBasket['ship_carrier'] = '';
		$aBasket['ship_charge'] = '';      
		$aBasket['taxes'] = 0;
      
		$_POST = $aBasket;
      
		if ( $_POST['ship_name_salutation'] == NULL ) { $_POST['ship_name_salutation'] = $aMember['name_salutation']; }
		if ( $_POST['ship_name_first'] == NULL ) { $_POST['ship_name_first'] = $aMember['name_first']; }
		if ( $_POST['ship_name_last'] == NULL ) { $_POST['ship_name_last'] = $aMember['name_last']; }
		if ( $_POST['ship_email'] == NULL ) { $_POST['ship_email'] = $aMember['email']; }
		if ( $_POST['ship_company'] == NULL ) { $_POST['ship_company'] = $aMember['account']['title']; }

		if ( $_POST['ship_address_1'] == NULL ) { 
		
			$_POST['ship_address_1'] = $aMember['addresses'][1]['line1']; 
			$_POST['ship_address_2'] = $aMember['addresses'][1]['line2']; 
			$_POST['ship_address_3'] = $aMember['addresses'][1]['line3']; 
			$_POST['ship_address_city'] = $aMember['addresses'][1]['city']; 
			$_POST['ship_address_stateid'] = $aMember['addresses'][1]['stateid']; 
			$_POST['ship_address_postal_code'] = $aMember['addresses'][1]['postalcode']; 
			$_POST['ship_address_phone'] = $aMember['addresses'][1]['phone']; 
			
		}
		
		include "screens/$framework/screen-1.php"; 

   } elseif( $_GET['step'] == 'setshipping' ) {
      
		$_POST = ordersCartContents();
		$_POST['ship_carrier'] = '';
		$_POST['ship_charge'] = 0;
		$_POST['taxes'] = 0;   
		include "screens/$framework/screen-1.php"; 
		exit;
   }
   elseif( $_GET['step'] == 'setbilling' ) {
      
		$_POST = ordersCartContents();
		$_POST['cc_number'] = '';
		include "screens/$framework/screen-3.php"; 
		exit;
   
   } elseif( $_POST['step'] == 'setshippingaddress' )  {
	
		sql_quote( $_POST );

		// check if email is filled out.
   
      	!is_valid_email_address( $_POST['ship_email'] ) ? $_POST['ship_email'] = '' : false;
   	
      	$states = sql_query( "SELECT COUNT(*) AS cnt FROM states WHERE countryid = '$_POST[ship_address_country]'" );
      	
      	$lstate = true;
      	
      	( $states['cnt'] > 0 && $_POST['ship_address_stateid'] == 0 ) ? $lstate = false : false;
      	         
      	if ( 			
			$lstate &&
	      	$_POST['ship_name_first'] != NULL && 
    	  	$_POST['ship_name_last'] != NULL && 
      		$_POST['ship_address_1'] != NULL && 
	      	$_POST['ship_address_postal_code'] != NULL 
    	  	&& $_POST['ship_address_phone'] != NULL && 
      		$_POST['ship_email'] != NULL ) {
      
			$aState = sql_query( "SELECT code FROM states WHERE stateid='$_POST[ship_address_stateid]' LIMIT 1" );
				
			$_POST['ship_address_state'] = $aState['code'];

			$_POST['handle_charge'] = $aBasket['cart_handling'];
		
			( $_POST['gift'] == 0 ) ? $_POST['gift_charge'] = 0 : false;
				            
			$cQuery = sql_prep_update_query ( 'orders_carts' , $_POST );
            
			mysql_query( "UPDATE orders_carts SET $cQuery WHERE cartid='$aBasket[cartid]' AND orderid = 0 LIMIT 1" );
		
			$aBasket = ordersCartContents();
			
			                                   
			include "screens/$framework/screen-2.php"; 
         
			exit;
      
      	}
      	else {
            
			$errorMessage = 'There is an issue with your information, please review the form and correct the data for any field marked in red.  Thank You.';

			if ( $_POST['ship_name_first'] == NULL ) { $aForm['ship_name_first']['style'] = 'ferror align-right'; $aForm['ship_name_first']['msg'] = 'missing'; }
			if ( $_POST['ship_name_last'] == NULL ) { $aForm['ship_name_last']['style'] = 'ferror align-right'; $aForm['ship_name_last']['msg'] = 'missing'; } 
			if ( $_POST['ship_address_1'] == NULL ) { $aForm['ship_address_1']['style'] = 'ferror align-right'; $aForm['ship_address_1']['msg'] = 'missing'; } 
			if ( $_POST['ship_address_city'] == NULL ) { $aForm['ship_address_city']['style'] = 'ferror align-right'; $aForm['ship_address_city']['msg'] = 'missing'; } 
			if ( $_POST['ship_address_postal_code'] == NULL ) { $aForm['ship_address_postal_code']['style'] = 'ferror align-right'; $aForm['ship_address_postal_code']['msg'] = 'missing'; } 
			if ( $_POST['ship_address_phone'] == NULL ) { $aForm['ship_address_phone']['style'] = 'ferror align-right'; $aForm['ship_address_phone']['msg'] = 'missing'; } 
			if ( $_POST['ship_email'] == NULL ) { $aForm['ship_email']['style'] = 'ferror align-right'; $aForm['ship_email']['msg'] = 'missing'; }
			if ( !$lstate ) { $aForm['ship_address_state']['style'] = 'ferror align-right'; $aForm['ship_address_state']['msg'] = 'missing'; }
			include "screens/$framework/screen-1.php";          
			exit;      
		}
	
	}
	elseif( $_POST['step'] == 'setshippingmethod' ) {
      
		if ( 
			SHIPPING_QTY_RATE == 0 && 
			SHIPPING_WEIGHT_RATE == 0 && 
			SHIPPING_USPS_RATE == 0 && 
			SHIPPING_PRICE_RATE == 0 && 
			SHIPPING_BASEWEIGHT_RATE == 0 && 
			SHIPPING_FDX_RATE == 0 && 
			SHIPPING_MIN_RATE == 0 && 
			SHIPPING_FLAT_RATE == 0 && 
			SHIPPING_UPS_RATE == 0 ) 
		{ 
	
			$_POST['ship_carrier'] = 'FREE SHIPPING';

			$_POST['ship_charge'] = 0;

			$aTax = $aBasket;
			$aTax['amount'] = $aBasket['cart_taxbase'];
			$aTax['ship_charge'] = ($_POST['ship_charge'] + $aBasket['cart_handling']);				
			$_POST['taxes'] = calcTax($aTax);				   	   	    
   	   	           	    
       	    sql_quote( $_POST );
       	    
			$cQuery = sql_prep_update_query ( 'orders_carts' , $_POST );
            
			mysql_query( "UPDATE orders_carts SET $cQuery WHERE cartid='$aBasket[cartid]' AND orderid = 0 LIMIT 1" );
       		 
			$aBasket = ordersCartContents();
			
			if ( $_POST['bill_name_salutation'] == NULL ) { $_POST['bill_name_salutation'] = $aMember['name_salutation']; }
			if ( $_POST['bill_name_first'] == NULL ) { $_POST['bill_name_first'] = $aMember['name_first']; }
			if ( $_POST['bill_name_last'] == NULL ) { $_POST['bill_name_last'] = $aMember['name_last']; }
			if ( $_POST['bill_email'] == NULL ) { $_POST['bill_email'] = $aMember['email']; }
			if ( $_POST['bill_company'] == NULL ) { $_POST['bill_company'] = $aMember['account']['title']; }
			
			if ( $_POST['cc_number'] == NULL ) 
			{
			
				$_POST['cc_type'] = $aMember['cc_type'];

				$_POST['cc_cvv2'] = $aMember['cc_cvv2'];
				
				if ( $aMember['cc_exp'] == '0000-00-00' ) {
				
					$aMember['cc_exp'] = date( "Y-m-d" );
				}
				
				$_POST['cc_exp_month'] = date( 'm' , strtotime( $aMember['cc_exp'] ) );

				$_POST['cc_exp_year'] = date( 'Y' , strtotime( $aMember['cc_exp'] ) );

				$_POST['cc_number'] = fencrypt( $aMember['cc_number'] , $aMember['passcode'] , 'd' );
			
			}				
			
			if ( !is_numeric( $_POST['cc_number'] ) ) {
			
				$_POST['cc_number'] = '';
			
			}

			if ( $_POST['bill_address_1'] == NULL ) { 
		
				$_POST['bill_address_1'] = $aMember['addresses'][1]['line1']; 
				$_POST['bill_address_2'] = $aMember['addresses'][1]['line2']; 
				$_POST['bill_address_3'] = $aMember['addresses'][1]['line3']; 
				$_POST['bill_address_city'] = $aMember['addresses'][1]['city']; 
				$_POST['bill_address_stateid'] = $aMember['addresses'][1]['stateid']; 
				$_POST['bill_address_postal_code'] = $aMember['addresses'][1]['postalcode']; 
				$_POST['bill_address_phone'] = $aMember['addresses'][1]['phone']; 
			
			}	
									
			include "screens/$framework/screen-3.php"; 
			exit;

		} else {
		
			list( $_POST['ship_carrier'] , $_POST['ship_charge'] ) = preg_split( "/--/" , $_POST['shipping'] );
			
			// DEPENDING UPON LEGAL CHANGES TO INTERNET ECOMMERCE
			// THIS SECTION MAY NEED TO GET MORE ROBUST IN BOTH PRODUCT DIFF AND OTHER CONSIDERATIONS.
									
			if  ( $_POST['ship_carrier'] != NULL ) {

				// add check to ensure no spoofing of cost.

				$aTax = array( 
					'amount' => $aBasket['cart_taxbase'] , 
					'ship_charge' => ( $_POST['ship_charge'] + $aBasket['cart_handling'] ),
					'postalcode' => $aBasket['ship_address_postal_code'],
					'state' => $aBasket['ship_address_state'],
					'country' => $aBasket['ship_address_country']
				);


				$_POST['taxes'] = calcTax($aTax);	
   	   	           	    
				sql_quote( $_POST );
				
				$cQuery = sql_prep_update_query ( 'orders_carts' , $_POST );
				            
				mysql_query( "UPDATE orders_carts SET $cQuery WHERE cartid='$aBasket[cartid]' AND orderid = 0 LIMIT 1" );

				$aBasket = ordersCartContents();
				
				$_POST = $aBasket;
				
				if ( !is_numeric( $_POST['cc_number'] ) ) {
			
					$_POST['cc_number'] = '';
			
				}
							
				include "screens/$framework/screen-3.php"; 
		         
				exit;
			
			}
			else {
			
		         if (empty($_POST['ship_carrier'])) { 		         
		         	$errorMessage = 'You have not selected an applicable shipping option.';		         
		         }
		         include "screens/$framework/screen-2.php";          
        		 exit;			
			}
		}

	}
	elseif( $_POST['step'] == 'setbilling' ) {

      	!is_valid_email_address( $_POST['bill_email'] ) ? $_POST['bill_email'] = '' : false;             	

		if ( 
			$_POST['bill_name_first'] != NULL && 
			$_POST['bill_name_last'] != NULL && 
			$_POST['bill_address_1'] != NULL && 
			$_POST['bill_address_postal_code'] != NULL && 
			$_POST['bill_address_phone'] != NULL && 
			$_POST['bill_email'] != NULL ) {
		
			$aState = sql_query( "SELECT code FROM states WHERE stateid='$_POST[bill_address_stateid]' LIMIT 1" );
				
			$_POST['bill_address_state'] = $aState['code'];
					
			if ($_POST['payment_method'] == 'PAYMENT_PO') {
			
				if ( $aMember['account']['enable_purchase_orders'] || $aMember['enable_purchase_orders'] ) 
				{
				
					if ( $_POST['ptype'] == 1 ) 
					{
				
						$_POST['payment_gateway'] = 'INVOICE' ;
					
					 	sql_quote( $_POST ); 
         
    	    		 	$cQuery = sql_prep_update_query ( 'orders_carts' , $_POST );
            
			         	mysql_query( "UPDATE orders_carts SET $cQuery WHERE cartid='$aBasket[cartid]' AND orderid = 0 LIMIT 1" );
         
        			 	$aBasket = ordersCartContents();
                                    
		        	 	include "screens/$framework/screen-4.php"; 
         	
        			 	exit;
        			 	
        			 }
        			 else {
        			 
						$errorMessage = 'You have not stated clearly that you want to use a purchase order.';

        			 	include "screens/$framework/screen-3.php"; 

        			 }
				
				} else {
				
					$errorMessage = 'You are <b>NOT</b> authorized to use purchase orders.';
					$_POST['taxes'] = $aBasket['taxes'];
					$_POST['ship_carrier'] = $aBasket['ship_carrier'];
					$_POST['ship_charge'] = $aBasket['ship_charge'];
					include "screens/$framework/screen-3.php"; 
					
					exit;
									
				}
			
			} elseif ( $_POST['payment_method'] == 'PAYMENT_MO' ) { // this will be for check support
			
				sql_quote( $_POST ); 
         
        		$cQuery = sql_prep_update_query ( 'orders_carts' , $_POST );
            
		       	mysql_query( "UPDATE orders_carts SET $cQuery WHERE cartid='$aBasket[cartid]' AND orderid = 0 LIMIT 1" );
         
        		$aBasket = ordersCartContents();
                                    
		        include "screens/$framework/screen-4.php"; 
         
        		exit;			

			} else {


				if ( 
					strlen( $_POST['cc_number'] ) >= 10 
					&& strlen( $_POST['cc_cvv2'] ) >= 3 
					&& $_POST['cc_type'] != NULL 
					&& $_POST['cc_name_on_card'] != NULL 
					&& time() < mktime(0, 0, 0, $_POST['cc_exp_month'] , 1 , $_POST['cc_exp_year'] ) 
					&& $_POST['cc_exp_year'] != NULL && 
					cc_validate_number( $_POST['cc_number'] , $_POST['cc_type'] , $error )	
					)
				{
          
         			$_POST['cc_number'] = fencrypt( $_POST['cc_number'] , $aBasket['session'] );		 
				 	sql_quote( $_POST ); 
         
        		 	$cQuery = sql_prep_update_query ( 'orders_carts' , $_POST );

		         	mysql_query( "UPDATE orders_carts SET $cQuery WHERE cartid='$aBasket[cartid]' AND orderid = 0 LIMIT 1" );
         
        		 	$aBasket = ordersCartContents();
         
		         	$aBasket['cc_number'] = substr( fencrypt( $aBasket['cc_number'] , $aBasket['session'] , 'd' ) , -4 );
                           
		         	include "screens/$framework/screen-4.php"; 
         
        		 	exit;
			
				}
				else {
				
					$errorMessage = 'We have detected errors and have marked them.  Please resolve the missing or invalid information.';
            
					if ( $_POST['cc_name_on_card'] == NULL ) { $aForm['cc_name_on_card']['style'] = 'ferror align-right'; $aForm['cc_name_on_card']['label'] = 'Missing Name as Appears on Card'; }			        
			        if ( strlen( $_POST['cc_number'] ) < 10 ) { $aForm['cc_number']['style'] = 'ferror align-right'; $aForm['cc_number']['label'] = 'Missing Card Number'; }
	    		    if ( $error != NULL ) { $aForm['cc_number']['style'] = 'ferror align-right'; $aForm['cc_number']['label'] = $error; }
			        if ( strlen( $_POST['cc_cvv2'] ) < 3 ) { $aForm['cc_cvv2']['style'] = 'ferror align-right'; $aForm['cc_cvv2']['label'] = 'Missing Security Code'; }
    			    if ( $_POST['cc_type'] == NULL ) { $aForm['cc_type']['style'] = 'ferror align-right'; $aForm['cc_type']['label'] = 'Missing Type'; }
			        if ( time() > mktime(0, 0, 0, $_POST['cc_exp_month'] , 1 , $_POST['cc_exp_year'] ) ) { $aForm['cc_exp_month']['style'] = 'ferror align-right'; $aForm['cc_exp_month']['label'] = 'Invalid Expiration Date'; }
			        if( !cc_validate_number( $_POST['cc_number'] , $_POST['cc_type'] , $error ) )
			        {
			        
						$aForm['cc_number']['style'] = 'ferror align-right'; $aForm['cc_number']['label'] = 'Invalid Card Number';
			        
			        }
				
					$_POST['taxes'] = $aBasket['taxes'];
					$_POST['ship_carrier'] = $aBasket['ship_carrier'];
					$_POST['ship_charge'] = $aBasket['ship_charge'];
					include "screens/$framework/screen-3.php"; 
					
					exit;
      
				}
			
			}
      
		} else {
			
			$errorMessage = 'Required fields are missing.';

			if ( $_POST['bill_name_first'] == NULL ) { $aForm['bill_name_first']['style'] = 'ferror align-right'; $aForm['bill_name_first']['label'] = 'Missing First Name'; }
			if ( $_POST['bill_name_first'] == NULL ) { $aForm['bill_name_first']['style'] = 'ferror align-right'; $aForm['bill_name_first']['label'] = 'Missing First Name'; }
			if ( $_POST['bill_name_last'] == NULL ) { $aForm['bill_name_last']['style'] = 'ferror align-right'; $aForm['bill_name_last']['label'] = 'Missing Last Name'; } 
			if ( $_POST['bill_address_1'] == NULL ) { $aForm['bill_address_1']['style'] = 'ferror align-right'; $aForm['bill_address_1']['label'] = 'Missing Address'; } 
			if ( $_POST['bill_address_city'] == NULL ) { $aForm['bill_address_city']['style'] = 'ferror align-right'; $aForm['bill_address_city']['label'] = 'Missing City'; } 
			if ( $_POST['bill_address_postal_code'] == NULL ) { $aForm['bill_address_postal_code']['style'] = 'ferror align-right'; $aForm['bill_address_postal_code']['label'] = 'Missing Postal Code'; } 
			if ( $_POST['bill_address_phone'] == NULL ) { $aForm['bill_address_phone']['style'] = 'ferror align-right'; $aForm['bill_address_phone']['label'] = 'Missing Phone'; } 
			if ( $_POST['bill_email'] == NULL ) { $aForm['bill_email']['style'] = 'ferror align-right'; $aForm['bill_email']['label'] = 'Missing Email'; }
			$_POST['taxes'] = $aBasket['taxes'];
			$_POST['ship_carrier'] = $aBasket['ship_carrier'];
			$_POST['ship_charge'] = $aBasket['ship_charge'];
			include "screens/$framework/screen-3.php"; 
			
			exit;
      
		}


   }
   elseif( $_REQUEST['step'] == 'processorder' && $aBasket['session'] != NULL ) {
   
		if ( isset( $_POST['toc'] ) ) {
			if (!ordersAllItemsAvailable()) {
				redirect('basket.php?error=' . urlencode('One of the items you were purchasing has sold out.  We have removed it from your cart.'));
				exit;
			}

			$aBasket['amount'] = $_POST['amount']; // - THIS WILL BE REMOVED AT ONE DAY.		
			$aBasket['memberid'] = $aMember['memberid'];						
			$aBasket['accountid'] = $aMember['accountid'];
					
			if ( 
				$aBasket['payment_method'] == 'PAYMENT_PO' || 
				$aBasket['payment_method'] == 'PAYMENT_CHECK_MO' || 
				$aBasket['payment_method'] == 'CAPTURE' ) 
			{
													
				$aOrder = ordersPostTransaction( $aBasket );
				$aBasket['orderid'] = $aOrder['response'];
				$reciept = ordersReceipt( $aBasket['orderid'] );
										
				if (ORDERS_EMAIL_NOTIFY_CUSTOMER) {            
					$aEmail = array();
    	    		$aEmail['to'] = $aTransaction['bill_email'];
			    	$aEmail['from'] = ORDERS_CUSTOMER_EMAIL_FROM;
        			$aEmail['subject'] = sprintf("%07d",$aTransaction['orderid']).' : '.ORDERS_CUSTOMER_EMAIL_SUBJECT;
 					$aEmail['html'] = $reciept['customer']['email'];
		        	sendmail( $aEmail );
				}

				if (ORDERS_EMAIL_NOTIFY_STAFF) {
					$aEmail = array();
					$aEmail['to'] = ORDERS_STAFF_EMAIL_TO;
					$aEmail['from'] = ORDERS_STAFF_EMAIL_TO;
					$aEmail['cc'] = ORDERS_STAFF_EMAIL_CC;
					$aEmail['bcc'] = ORDERS_STAFF_EMAIL_BCC;            
					$aEmail['subject'] = sprintf("%07d",$aTransaction['orderid']).' : '.ORDERS_STAFF_EMAIL_SUBJECT;
 					$aEmail['html'] = $reciept['staff']['email'];						               
					sendmail( $aEmail );					
				}
            
        	    setcookie( FOONSTER_LICENSE_KEY , '' , 86400 , '/' );
            		            		            		            		
				include "screens/$framework/screen-5.php"; 
					
				exit;			
			
			} else { // assume the rest use the credit cartd 
			
				mysql_query( "INSERT INTO orders_transactions VALUES ( NULL , '$aBasket[cartid]', '$aBasket[session]' );" );
								
				if ( mysql_affected_rows() == 1 ) {
			
					$aTransaction = ordersAuthorization( $aBasket );
				
					if ( $aTransaction['authorized'] == 1 ) { // some servers do not assume 1 is true.
				
						$aTransaction = array_merge( $_POST , $aTransaction );
						$aTransaction['memberid'] = $aMember['memberid'];
						$aTransaction['accountid'] = $aMember['accountid'];
						$aOrder = ordersPostTransaction( $aTransaction );
						$aTransaction['orderid'] = $aOrder['response'];
        	    		$aTransaction['cc_number'] = substr( fencrypt( $aTransaction['cc_number'] , $aTransaction['session'] , 'd' ) , -4 );
						$aBasket['cc_number'] = $aTransaction['cc_number'];
						$aBasket['orderid'] = $aTransaction['orderid'];
						$reciept = ordersReceipt( $aBasket['orderid'] );
				
						if ( ORDERS_EMAIL_NOTIFY_CUSTOMER == 1 ) {
            
				            $aEmail = array();
    	    			    $aEmail['to'] = $aTransaction['bill_email'];
			    	        $aEmail['from'] = ORDERS_CUSTOMER_EMAIL_FROM;
        				    $aEmail['subject'] = sprintf("%07d",$aTransaction['orderid']) . ' : ' . ORDERS_CUSTOMER_EMAIL_SUBJECT;
 							$aEmail['html'] = $reciept['customer']['email'];
		            				        			            		            
		        		    sendmail( $aEmail );
            
    		     		}

						if (ORDERS_EMAIL_NOTIFY_STAFF == 1) {
            
							$aEmail = array();
							$aEmail['to'] = ORDERS_STAFF_EMAIL_TO;
							$aEmail['from'] = ORDERS_STAFF_EMAIL_TO;
							$aEmail['cc'] = ORDERS_STAFF_EMAIL_CC;
							$aEmail['bcc'] = ORDERS_STAFF_EMAIL_BCC;            
							$aEmail['subject'] = sprintf("%07d",$aTransaction['orderid']) . ' : ' . ORDERS_STAFF_EMAIL_SUBJECT;
 							$aEmail['html'] = $reciept['staff']['email'];
		            		
							sendmail( $aEmail );
					
						}
        	    		setcookie( FOONSTER_LICENSE_KEY , '' , 86400 , '/' );
            			$aBasket = array_merge( $aBasket , $aTransaction );
						include "screens/$framework/screen-5.php"; 
						exit;				
					} else {				
						mysql_query( "DELETE FROM orders_transactions WHERE session='$aBasket[session]' AND cartid='$aBasket[cartid]' LIMIT 1" );
						$aBasket['cc_number'] = substr( fencrypt( $aBasket['cc_number'] , $aBasket['session'] , 'd' ) , -4 );
						$_POST = $aBasket;
						$errorMessage = 'CC Failure:'.$aTransaction['response_code'];
						include "screens/$framework/screen-4.php"; 
						exit;
					}
				} else {
        	    	setcookie( FOONSTER_LICENSE_KEY , '' , 86400 , '/' );
					redirect( 'basket.php?error='.urlencode( 'This session has already been used.' ) );				
					exit;			
				}				
			}				
		} else {		
			$aBasket['cc_number'] = substr( fencrypt( $aBasket['cc_number'] , $aBasket['session'] , 'd' ) , -4 );			
			$_POST = $aBasket;
      		$errorMessage = 'You have to agree to the terms and conditions of sale to proceed.';
			include "screens/$framework/screen-4.php"; 
		
		}
   } else {
      redirect( '/' . $webroot . '/basket');
   }
}
