<?php
	
	$cPageTitle = 'step 5 of 5 : ' . sprintf("%07d",$aBasket['orderid']).' ORDER COMPLETED';

   	include '_header.php'; 
   
?>

<h1>
	<?=$cPageTitle ?>
</h1>


<div class="txtbox">
	Congratulations, your order is complete and your can use the following to reference.
</div>

<?php include( 'checkout-cart-contents-final.php' ); ?>	

	<table align="center" width="100%" cellpadding="5" cellspacing="5">
		<tr>
			<td class="headline-md dashed" width="50%">
				Shipping Information			
			</td>
			<td class="headline-md dashed" width="50%">
				Billing Information						
			</td>
		</tr>

		<tr valign="top">
			<td class="fcontent bborder">
				<table cellpadding="5" cellspacing="0" width="100%">
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							to:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['ship_name_salutation'] ?> <?=$aBasket['ship_name_first'] ?> <?=$aBasket['ship_name_last'] ?> <?=$aBasket['ship_name_suffix'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							address:
						</td>
						<td class="fcontent">
							<?=$aBasket['ship_address_1'] ?><br/>
							<?php
							if ( $aBasket['ship_address_2'] != NULL ) {
				
								echo $aBasket['ship_address_2'].'<br/>';
				
							}
				
							if ( $aBasket['ship_address_3'] != NULL ) {
				
								echo $aBasket['ship_address_3'].'<br/>';
				
							}
							?>
							<?=$aBasket['ship_address_city'] ?> <?=$aBasket['ship_address_state'] ?> <?=$aBasket['ship_address_postal_code'] ?><br/>
							<?=$aBasket['ship_country_iso_3166'] ?>
						</td>
					</tr>
					<?php
					
					if ( $aBasket['ship_company'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right nobr" width="25%">
							<?=$aForm['ship_company']['label'] ?>:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['ship_company'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>
					<tr valign="top">
						<td class="flabel align-right">
							phone:
						</td>
						<td class="fcontent">
							<?=$aBasket['ship_address_phone'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							email:
						</td>
						<td class="fcontent">
							<?=$aBasket['ship_email'] ?><br />
						</td>
					</tr>
					
					<?php
					
					if ( $aBasket['ship_msg'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right">
							special&nbsp;instructions:
						</td>
						<td class="fcontent">
							<?=nl2br( trim( $aBasket['ship_msg'] )) ?>
						</td>
					</tr>
					<?php
					
					}
					
					if ( $aBasket['gift'] ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel dashed align-right">
							Gift Options
						</td>
						<td class="flabel dashed">
							&nbsp;
						</td>
					</tr>	
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							receipt:
						</td>
						<td class="fcontent" width="75%">
							<?php
							if ( $aBasket['gift_receipt'] ) {
					
								echo 'Yes<br />';
						
							}
							else {
						
								echo 'No<br />';
					
							}
							?>
						</td>
					</tr>					
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							wrapping:
						</td>
						<td class="fcontent" width="75%">
							<?php
							if ( $aBasket['gift_wrap'] ) {
					
								echo 'Yes<br />';
						
							}
							else {
						
								echo 'No<br />';
					
							}
							?>
						</td>
					</tr>					
					
					<tr valign="top">
						<td class="flabel align-right">
							message:
						</td>
						<td class="fcontent">
							<?=nl2br( trim( $aBasket['gift_msg'] ) ) ?>
						</td>
					</tr>
					<?php
										
				}					

					?>								
				</table>
			</td>
			<td class="fcontent bborder">
			
				<table cellpadding="5" cellspacing="0" width="100%">
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							to:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['bill_name_salutation'] ?> <?=$aBasket['bill_name_first'] ?> <?=$aBasket['bill_name_last'] ?> <?=$aBasket['bill_name_suffix'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							address:
						</td>
						<td class="fcontent">
							<?=$aBasket['bill_address_1'] ?><br/>
							<?php
							if ( $aBasket['bill_address_2'] != NULL ) {
				
								echo $aBasket['bill_address_2'].'<br/>';
				
							}
				
							if ( $aBasket['bill_address_3'] != NULL ) {
				
								echo $aBasket['bill_address_3'].'<br/>';
				
							}
							?>
							<?=$aBasket['bill_address_city'] ?> <?=$aBasket['bill_address_state'] ?> <?=$aBasket['bill_address_postal_code'] ?><br/>
							<?=$aBasket['bill_country_iso_3166'] ?>
						</td>
					</tr>
					<?php
					
					if ( $aBasket['bill_company'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right nobr" width="25%">
							<?=$aForm['bill_company']['label'] ?>:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['bill_company'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>
					
					<?php
					
					if ( $aBasket['referral'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right nobr" width="25%">
							<?=$aForm['referral']['label'] ?>:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['referral'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>					
									
					<tr valign="top">
						<td class="flabel align-right">
							phone:
						</td>
						<td class="fcontent">
							<?=$aBasket['bill_address_phone'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							email:
						</td>
						<td class="fcontent">
							<?=$aBasket['bill_email'] ?><br />
							order confirmation and order status will be sent to this address.
						</td>
					</tr>
					
					<?php
					
					if ( $aBasket['payment_method'] == 'PAYMENT_MO' ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel dashed" colspan="2">
							You are paying by Check
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							<?=$aForm['check_route_num']['label'] ?>
						</td>
						<td class="fcontent">
							<?=$aBasket['check_route_num'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							<?=$aForm['check_account_num']['label'] ?>
						</td>
						<td class="fcontent">
							<?=$aBasket['check_account_num'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							<?=$aForm['check_num']['label'] ?>
						</td>
						<td class="fcontent">
							<?=$aBasket['check_num'] ?>
						</td>
					</tr>

					<?php
					
					}
					
					?>
					
					
					<?php
					
					if ( $aBasket['payment_method'] == 'PAYMENT_PO' ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel dashed" colspan="2">
							You are submitting a purchase order
						</td>
					</tr>
					<?php
					
					if ( $aBasket['ext_ref_number'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right">
							reference:
						</td>
						<td class="fcontent">
							<?=$aBasket['ext_ref_number'] ?>
						</td>
					</tr>
					<?php
					
					}
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right">
							&nbsp;
						</td>
						<td class="fcontent">
							<b><i><?=$aMember['name_first'] ?> <?=$aMember['name_last'] ?></i></b> you are authorized to order products using a purchase order.  
							please note that a purchase order is non-binding until we have notified you that it has been accepted.
	
						</td>
					</tr>
										
					<?php
					
					}
					
					?>
					
					<?php
					
					if ( $aBasket['payment_method'] == 'PAYMENT_CC' ) {
					
					?>
					
					
					<tr valign="top">
						<td class="flabel dashed" colspan="2">
							You are paying by credit card
						</td>
					</tr>	
					
					<tr valign="top">
						<td class="flabel align-right">
							type:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_type'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							number:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_number'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							exp date:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_exp_month'] ?>/<?=$aBasket['cc_exp_year'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							cvv2 code:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_cvv2'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>
					
					<?php
					
					if ( $aBasket['bill_msg'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							special&nbsp;instructions:
						</td>
						<td class="fcontent" width="75%">
							<?=nl2br( trim( $aBasket['bill_msg'] )) ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>					
					
				</table>
			</td>
		</tr>
	</table>
	
<?php 	include( '_footer.php' ); ?>	