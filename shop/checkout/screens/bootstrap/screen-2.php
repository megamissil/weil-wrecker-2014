<?php
	
$lFreeShipping = false;

foreach ( $aBasket['basket_promos'] AS $nPromoId => $aPromo ) 
{

	if ( $aPromo['free_shipping'] ) 
	{ 
	
		$lFreeShipping = true; 
	
	}	

}

$count = 0;

foreach ( $aBasket['products'] AS $id => $product ) 
{

	if ( $product['shipping_free'] == 0 )
	{

		$count += $product['qty'];

	}


}

$cPageTitle = 'step 2 of 5 : select shipping method';

include '_header.php'; 
   
?>

<h1>
	<?=$cPageTitle ?>
</h1>

<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="foonster" onsubmit="return validate(this);">
	<input type="hidden" name="step" value="setshippingmethod">
	<table width="100%" cellpadding="5" cellspacing="0" border="0">
		<tr valign="top">
			<td class="fcartnav">
				<?=$cCartTitle ?>
			</td>
		</tr>
	</table>
	<?php
		
		echo !empty( $_POST['error'] ) ? '<div class="ferror">' . $_POST['error'] . '</div>' : false;
					?>		
	
	<div class="bluebox" style="min-height: 100px; width: 98%;">
		<table width="100%">
			<tr valign="top">
				<td width="25%">
				
					<b class="size16">Shipping to:</b>
					<br />
					<?=$aBasket['ship_name_salutation'] ?> <?=$aBasket['ship_name_first'] ?> <?=$aBasket['ship_name_last'] ?> <?=$aBasket['ship_name_suffix'] ?><br />
					<?=$aBasket['ship_address_1'] ?><br/>
					<?php
					echo ( !empty( $aBasket['ship_address_2'] ) ) ? $aBasket['ship_address_2'] . '<br />' : false ; 
				
					echo ( !empty( $aBasket['ship_address_3'] ) ) ? $aBasket['ship_address_3'] . '<br />' : false ; 
					?>
					<?=$aBasket['ship_address_city'] ?> <?=$aBasket['ship_address_state'] ?> <?=$aBasket['ship_address_postal_code'] ?> <?=$aBasket['ship_country_iso_3166'] ?><br />
					<b>P:</b><?=$aBasket['ship_address_phone'] ?><br />
					<b>E:</b><?=$aBasket['ship_email'] ?><br />
					<?php
						echo strlen( $aBasket['ship_company'] ) > 2 ? $aBasket['ship_company'] . '<br />' : false;
					?>		
				</td>
				<td width="75%">
				<?php
		
				// start shipping calculations
		
				if ( $lFreeShipping || $count == 0 )
				{
		
					echo 'Congratulations your order has qualified for <b>FREE SHIPPING</b>, so proceed to the payment information.';
					echo '<input type="hidden" name="shipping" value="FREE SHIPPING--0.00">';
			
				}
				else {
				
					echo '<p> We have calculated the following shipping methods and costs to your location.</p>';
		
					if ( 
						SHIPPING_FLAT_RATE
						|| SHIPPING_BASEWEIGHT_RATE
						|| SHIPPING_PRICE_RATE
						|| SHIPPING_QTY_RATE
						|| SHIPPING_FDX_RATE
						|| SHIPPING_UPS_RATE
						|| SHIPPING_USPS_RATE
						) 
					{ 	
			
						$aMethods = array();
			
						echo '<table>';
			
						if ( SHIPPING_FLAT_RATE )
						{
								
							$aMethods = shipping_flat_rates( $aBasket );
										
						}

						if ( SHIPPING_PRICE_RATE )
						{
				
							$aMethods = calcPriceShipping( $aBasket );
							           				           				
						}
				
				/*

				if ( SHIPPING_QTY_RATE )
				{
				
					$aTemp = calcQtyShipping( $aBasket );
					
					if ( sizeof( $aTemp ) > 0 )
					{
					
						foreach ( $aTemp AS $c => $v )
						{
						
							$aMethods[$c] = $v;
						
						}
					
					}				
           				
				}
				
				*/
								
				foreach ( $aMethods AS $n => $aValue )
				{
								
					echo '<tr valign="top">'."\n";
					echo '<td width="1">'."\n";
					echo '<input type="radio" name="shipping" value="' . stripslashes( $aValue['title'] . '--' . $aValue['rate'] ).'">'."\n";
					echo '</td>'."\n";
					echo '<td width="1" class="fcontent"><nobr>'."\n";
					echo '$ ' . number_format( $aValue['rate'] , 2 ) . '</nobr></td><td width="*" class="fcontent">: '.stripslashes( $aValue['title'] );						
					echo !empty( $aValue['msg'] ) ? '<ul>'.nl2br( stripslashes( $aValue['msg'] ) ) . '</ul>' : false;
					echo '</td>'."\n";
					echo '</tr>'."\n";

				}

				if ( SHIPPING_FDX_RATE )
				{

					$aRates = shipping_fedex_rates( $aBasket );
					
					if ( sizeof( $aRates ) > 0 ) 
		         	{
 
						foreach ( $aRates AS $cLabel => $aRate ) {
												
							if ( $cLabel == 'ERROR' ) 
							{
								echo '<tr valign="top">'."\n";
								echo '<td width="1">'."\n";
								echo '&nbsp;';
								echo '</td>'."\n";
								echo '<td width="1"><nobr>'."\n";
								echo $aRate;
								echo '</nobr></td>'."\n";
								echo '</tr>'."\n";							
							
							}
							else {
																                   					
								echo '<tr valign="top">'."\n";
								echo '<td width="1">'."\n";
								echo '<input type="radio" name="shipping" value="' . stripslashes( $cLabel . '--' . $aRate['RATE'] ).'">'."\n";
								echo '</td>'."\n";
								echo '<td width="1"><nobr>'."\n";
								echo '$ ' . number_format( $aRate['RATE'] , 2 ).'</nobr></td><td width="*" class="fcontent">' . html_entity_decode( stripslashes( $cLabel ) );
								echo '</nobr></td>'."\n";
								echo '</tr>'."\n";
								
							}
    					
						}
						
					}
				
				}

				if ( SHIPPING_UPS_RATE )
				{
				
					$aRates = shipping_ups_rates( $aBasket );
					
					if ( sizeof( $aRates ) > 0 ) 
		         	{
 
						foreach ( $aRates AS $cLabel => $aRate ) {
												
							if ( $cLabel == 'ERROR' ) 
							{
								echo '<tr valign="top">'."\n";
								echo '<td width="1">'."\n";
								echo '&nbsp;';
								echo '</td>'."\n";
								echo '<td width="1"><nobr>'."\n";
								echo $aRate;
								echo '</nobr></td>'."\n";
								echo '</tr>'."\n";							
							
							}
							else {
																                   					
								echo '<tr valign="top">'."\n";
								echo '<td width="1">'."\n";
								echo '<input type="radio" name="shipping" value="' . stripslashes( $cLabel . '--' . $aRate['RATE'] ).'">'."\n";
								echo '</td>'."\n";
								echo '<td width="1"><nobr>'."\n";
								echo '$ ' . number_format( $aRate['RATE'] , 2 ).'</nobr></td><td width="*" class="fcontent">' . html_entity_decode( stripslashes( $cLabel ) );
								echo '</nobr></td>'."\n";
								echo '</tr>'."\n";
								
							}
    					
						}
						
					}
								
				
				}

				if ( SHIPPING_USPS_RATE )
				{
				
					$aRates = shipping_usps_rates( $aBasket , $aBasket['ship_address_postal_code'] , $nWeight, 'ALL' );
						         	
		         	if ( sizeof( $aRates ) > 0 ) 
		         	{
 
						foreach ( $aRates AS $cLabel => $aRate ) {
												
							if ( $cLabel == 'ERROR' ) 
							{
								echo '<tr valign="top">'."\n";
								echo '<td width="1">'."\n";
								echo '&nbsp;';
								echo '</td>'."\n";
								echo '<td width="1"><nobr>'."\n";
								echo $aRate;
								echo '</nobr></td>'."\n";
								echo '</tr>'."\n";							
							
							}
							else {
																                   					
								echo '<tr valign="top">'."\n";
								echo '<td width="1">'."\n";
								echo '<input type="radio" name="shipping" value="' . stripslashes( $cLabel . '--' . $aRate['RATE'] ).'">'."\n";
								echo '</td>'."\n";
								echo '<td width="1"><nobr>'."\n";
								echo '$ ' . number_format( $aRate['RATE'] , 2 ).'</nobr></td><td width="*" class="fcontent">' . html_entity_decode( stripslashes( $cLabel ) );
								echo '</nobr></td>'."\n";
								echo '</tr>'."\n";
								
							}
    					
						}
					
					}
				
				}

				echo '</table>';
		
			}
			else {
			
				echo 'Congratulations your order has qualified for <b>FREE SHIPPING</b>, so proceed to the payment information.';
				echo '<input type="hidden" name="shipping" value="FREE SHIPPING--0.00">';
			
			}
		
		}
		
		// end shipping calculations
		
		if ( $aBasket['cart_handling'] > 0 )
		{
		
			echo '<p>There is a handling charge of ' . number_format( $aBasket['cart_handling'] , 2 ) . ' USD associated with this order.</p>';
		
		}
		
		?>					
		<p />		
		<input type="submit" value="proceed payment information " class="fbtn" style="margin-right: 10px;">				
				</td>
			</tr>
		</table>
	</div>		
</form>

<script language="JavaScript" type="text/javascript">

function showGift( cValue ) {

	if ( cValue == '1' ) {
	
		document.getElementById('giftoptions').style.display = 'block';
			
	}
	else {
	
		document.getElementById('giftoptions').style.display = 'none';
			
	}

}

</script>


<?php 

	include 'checkout-cart-contents.php'; 

	include '_footer.php';