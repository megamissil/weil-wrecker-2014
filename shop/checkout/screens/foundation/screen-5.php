<?php
	
$cPageTitle = 'step 5 of 5 : ' . sprintf("%07d",$aBasket['orderid']).' ORDER COMPLETED';
include '../_header.php';    
?>

<div class="row">
	<div class="large-12 columns">
		<h1>
			<?=$cPageTitle ?>
		</h1>
	</div>
</div>
<div class="row">
	<div class="large-12 columns">
		<p>Congratulations, your order is complete and your can use the following to reference.</p>
	</div>
</div>
	
<?php 

include '../basket/contents.php';

include '../_footer.php';
