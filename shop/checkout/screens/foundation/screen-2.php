<?php

$lFreeShipping = false;

$aMethods = array();

foreach ( $aBasket['basket_promos'] AS $nPromoId => $aPromo ) {
	if ( $aPromo['free_shipping'] ) {
		$lFreeShipping = true;
	}
}

$count = 0;

foreach ( $aBasket['products'] AS $id => $product ) {
	if ( $product['shipping_free'] == 0 ) {
		$count += $product['qty'];
	}
}


if ( $lFreeShipping || $count == 0 ) {

	$aMethods = array(
		'FREE SHIPPING' =>
		array('RATE' => 0.00,
			'EXTENDEDTEXT' => 'Congratulations your order has qualified for <b>FREE SHIPPING</b>, so proceed to the payment information.'));

} else {

	if (
		SHIPPING_FLAT_RATE ||
		SHIPPING_BASEWEIGHT_RATE ||
		SHIPPING_PRICE_RATE ||
		SHIPPING_QTY_RATE ||
		SHIPPING_FDX_RATE ||
 		SHIPPING_UPS_RATE ||
		SHIPPING_USPS_RATE) {

		if ( SHIPPING_FLAT_RATE ) {
			$aMethods = shipping_flat_rates($aBasket);
		}

		if ( SHIPPING_PRICE_RATE ) {
			$aMethods = calcPriceShipping( $aBasket );
		}

		if (SHIPPING_FDX_RATE) {
			$aRates = shipping_fedex_rates($aBasket);
			foreach ($aRates as $k => $value) {
				$value['carrier'] = 'fedex';
				$aMethods[$k] = $value;
			}
		}

		if ( SHIPPING_UPS_RATE )
		{
			$aRates = shipping_ups_rates($aBasket);
			
			$ChargedFiveBucks=FALSE;
			foreach ($aBasket['products'] as $k=>$v) 
			{ 
				//crawl the products subarray  Here, $k looks is our random dumb number
				if (is_array($v) && !$ChargedFiveBucks) 
				{ 
					//did we hit a random key sub array under products?
					if ($aBasket['products'][$k]['productid'] == '1001') 
					{  												
						foreach ($aRates as $k=>$v) 
						{
							if (is_array($v))
							{
								foreach ($v as $kk=>$vv) 
								{
									if ($kk == "RATE") 
									{
										$aRates[$k][$kk] += 5;
									}
								}
							}
						}
                        
                        $ChargedFiveBucks=TRUE; //keeps us from doing this again
					} 
				}
			}

			//  TASK : BD52198A0C : Alternate shipping calculation for Gift Card Only Purchases
			//  GiftCard only purchases to be $1 shipping for FullMoon BBQ….
			
			$giftCardOnly = true;

			foreach ($aBasket['products'] as $k=>$v) 
			{
				$v['productid'] != 1000 ? $giftCardOnly = false : false;				
			}

			if ($giftCardOnly) {			
				$aRates = array('Ground' => array( 'RATE' => 1.00));
			}

			// END TASK : BD52198A0C
						
			foreach ($aRates as $k => $value) {
				$value['carrier'] = 'ups';
				$aMethods[$k] = $value;
			}
		}

		if (SHIPPING_USPS_RATE) {
			$aRates = shipping_usps_rates( $aBasket , $aBasket['ship_address_postal_code'] , $nWeight, 'ALL' );
			foreach ($aRates as $k => $value) {
				$value['carrier'] = 'usps';
				$aMethods[$k] = $value;
			}
		}



	}
}

$cPageTitle = 'step 2 of 5 : select shipping method';

include '../_header.php';
?>
<script>
	table {
		background: none;
		border: none;
	}
	table td {
		color: white;
	}
</script>
<div class="row">
	<div class="large-12 columns">
		<h1>
			<?=$cPageTitle ?>
		</h1>
	</div>
</div>
<div class="row" id="errorMessage">
	<div class="large-12 columns errorMessage">
		<?= $errorMessage; ?>
	</div>
</div>

<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="tyfoonCart" id="tyfoonCart">
<input type="hidden" name="step" value="setshippingmethod">
<div class="row">
	<div class="large-3 columns">
		<h3>
			Shipping to:
		</h3>
		<br />
		<?=$aBasket['ship_name_salutation'] ?> <?=$aBasket['ship_name_first'] ?> <?=$aBasket['ship_name_last'] ?> <?=$aBasket['ship_name_suffix'] ?><br />
		<?=$aBasket['ship_address_1'] ?><br/>
		<?php
		echo ( !empty( $aBasket['ship_address_2'] ) ) ? $aBasket['ship_address_2'] . '<br />' : false ;
		echo ( !empty( $aBasket['ship_address_3'] ) ) ? $aBasket['ship_address_3'] . '<br />' : false ;
		?>
		<?=$aBasket['ship_address_city'] ?> <?=$aBasket['ship_address_state'] ?> <?=$aBasket['ship_address_postal_code'] ?> <?=$aBasket['ship_country_iso_3166'] ?><br />
		<b>Phone: </b><?=$aBasket['ship_address_phone'] ?><br />
		<b>Email: </b><?=$aBasket['ship_email'] ?><br />
		<?php
		echo strlen( $aBasket['ship_company'] ) > 2 ? $aBasket['ship_company'] . '<br />' : false;
		echo strlen( $aBasket['ship_msg'] ) > 2 ? 'Special Instructions: <br />' . nl2br($aBasket['ship_msg']) : false;
		?>
	

	</div>
	<div class="large-9 columns">
			<p> We have calculated the following shipping methods and costs to your location.</p>
			<table cellpadding="10" cellspacing="0">
			<?php
			foreach ($aMethods as $method => $service) {
				echo '<tr valign="top">'."\n";
				echo '<td width="1">'."\n";
				echo '<input type="radio" name="shipping" value="' . stripslashes( $method . '--' . $service['RATE'] ).'">'."\n";
				echo '</td>'."\n";
				echo '<td width="1"><nobr>'."\n";
				echo '$ ' . number_format( $service['RATE'] , 2 ).'</nobr></td><td width="*">' . html_entity_decode(stripslashes($method));
				echo '</nobr></td>'."\n";
				echo '</tr>'."\n";
			}
			?>
			</table>
	</div>
</div>
<div class="row">
	<div class="large-12 column">
		<input type="submit" value="proceed payment information " class="button right">
	</div>
</div>
</form>

<?php
include '../basket/contents.php';
include '../_footer.php';
