<?php

	$mQuery  = mysql_query("SELECT countryid, title FROM country WHERE active='1' ORDER BY deft DESC,title ASC");
	$cJavascript ='';
	while ( $aCountry = mysql_fetch_assoc( $mQuery )) {
		$mStatQry = mysql_query("SELECT * FROM states WHERE active='1' AND countryid='$aCountry[countryid]' ORDER BY title ASC");	
		if ( mysql_num_rows( $mStatQry ) == 0 ) {
			$cJavascript .= 'assocArray["state_'.$aCountry['countryid'].'"] = new Array ("0","Not Applicable",';
		} else {
			$cJavascript .= 'assocArray["state_'.$aCountry['countryid'].'"] = new Array ("0","Select State/Prov",';
		}
		while ( $aStates = mysql_fetch_assoc($mStatQry)) {
			$cJavascript .= "\"$aStates[stateid]\",\"".stripslashes($aStates['title']).'",';
		}
		$cJavascript = substr($cJavascript, 0, -1);
		$cJavascript .= ');';
	} 
      
	$cPageTitle = 'step 3 of 5 : payment information';

include '../_header.php';    
?>

<div class="row">
	<div class="large-12 columns">
		<h1>
			<?=$cPageTitle ?>
		</h1>
	</div>
</div>
<div class="row" id="errorMessage">
	<div class="large-12 columns errorMessage">
		<?= $errorMessage; ?>
	</div>
</div>
<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="tyfoonCart" id="tyfoonCart" class="validate" novalidate>
<input type="hidden" name="step" value="setbilling">
<input type="hidden" id="payment_method" name="payment_method" value="<?=$_POST['payment_method'] ?>">	
<input type="checkbox" value="1" name="sameas" onClick=" setBillTo( this.form );" style="display: inline;"> - same as the billing address

<div class="row">
	<div class="large-12 columns">
		<div class="row">
			<div class="large-4 columns">
				<input name="bill_name_salutation" type="hidden" value="<?=$_POST['bill_name_salutation'] ?>" placeholder="Mr., Mrs, Dr., etc.....">
				<div class="large-12 columns">				
					<label for="bill_name_first">First Name *</label>
					<input name="bill_name_first" id="bill_name_first" type="text" value="<?=$_POST['bill_name_first'] ?>" placeholder="Enter the first name (required)" required="required">					
				</div>

				<div class="large-12 columns">				
					<label for="bill_name_last">Last Name *</label>
					<input name="bill_name_last" id="bill_name_last" type="text" value="<?=$_POST['bill_name_last'] ?>" placeholder="Enter the last name (required)" required="required">					
				</div>

				<div class="large-12 columns">				
					<label for="bill_email">Email *&nbsp;<a href="javascript:;" class="large" data-dropdown="help-email">(why we need this)</a></label>
					<input name="bill_email" id="bill_email" class="isValidField" data-type="email" type="text" value="<?=$_POST['bill_email'] ?>" placeholder="Enter the email address (required) ex: disco@example.com">
				</div>

				<div class="large-12 columns">				
					<label for="bill_address_phone">Phone *&nbsp;<a href="javascript:;" class="large" data-dropdown="help-phone">(why we need this)</a></label>
					<input name="bill_address_phone" id="bill_address_phone" type="text" value="<?=$_POST['bill_address_phone'] ?>" placeholder="Enter the phone number (required) ex: (907) 555-1212" required="required">					
				</div>

				<div class="large-12 columns">				
					<label for="bill_company">Company</label>
					<input name="bill_company" id="bill_company" type="text" value="<?=$_POST['bill_company'] ?>">					
				</div>

				<div class="large-12 columns">				
					<label for="bill_msg">Special Instructions</label>
					<textarea class="tyfoonCartTextArea" placeholder="if you have any special instructions, please provide them."><?=$_POST['bill_msg'] ?></textarea>
				</div>

			</div>
			<div class="large-4 columns">
				<div class="large-12 columns">
					<label for="bill_address_1">Street Address *</label>
					<input name="bill_address_1" id="bill_address_1" value="<?=$_POST['bill_address_1'] ?>" placeholder="Street Address (required) ex: 1234 Main Street" type="text" data-errormessage="Enter a street address." required="required">
    			</div>
				<div class="large-12 columns">
					<label>&nbsp;</label>
					<input name="bill_address_2" value="<?=$_POST['bill_address_2'] ?>" type="text" placeholder="Apartment or Suite Number (optional)">
    			</div>
				<div class="large-12 columns">
					<label>&nbsp;</label>
					<input name="bill_address_3" value="<?=$_POST['bill_address_3'] ?>" type="text" placeholder="additional address information (optional)">      					
    			</div>
				<div class="large-12 columns">
					<label for="bill_address_city">City *</label>
					<input name="bill_address_city" id="bill_address_city" value="<?=$_POST['bill_address_city'] ?>" placeholder="City (required)" type="text" data-errormessage="Enter a city name. (required)" required="required">      					
    			</div>
				<div class="large-12 columns">
					<label for="bill_address_stateid">State/Prov *</label>
						<select name="bill_address_stateid" id="bill_address_stateid" required="required">
						<option value="">Select State/Prov (required)</option>
						<?php
							mysql_data_seek($mQuery, 0);
							$aCountry = mysql_fetch_assoc( $mQuery );
							$qState = mysql_query("SELECT * FROM states WHERE countryid='$aCountry[countryid]' ORDER BY deft DESC, title ASC");
							while ( $aState = mysql_fetch_assoc( $qState ) ) {
								echo "<option value=\"$aState[stateid]\"";
								if ( $aState['stateid'] == $_POST['bill_address_stateid'] ) { echo ' selected="selected"'; }
								echo ">".cleanstring( $aState['title'] ).'</option>';
							}
						?>  
						</select>
					
    			</div>
				<div class="large-12 columns">
					<label for="bill_address_country">Country *</label>
						<select name="bill_address_country" id="bill_address_country" onChange="listboxItemSelected(this,this.form.bill_address_stateid,'state_');" class="finput full" tabindex="12">
						<?php
						mysql_data_seek($mQuery, 0);
					
						while ( $aCountry = mysql_fetch_assoc( $mQuery ) ) {
							echo "<option value=\"$aCountry[countryid]\"";
							if ( $aCountry['countryid'] == $_POST['countryid'] ) { echo 'SELECTED'; }
							echo ">".cleanstring( $aCountry['title'] ).'</option>';
						}
						
						?>  
						</select>
					
    			</div>		
				<div class="large-12 columns">
					<label for="bill_address_postal_code">Postal Code *</label>
					<input name="bill_address_postal_code" id="bill_address_postal_code" value="<?=$_POST['bill_address_postal_code'] ?>" placeholder="Enter the zip code (required)" type="text" data-errormessage="Enter a postal code. (required)" required="required">      					
    			</div> 
    		</div>
			<div class="large-4 columns">    			
				<?php 
				if ( PAYMENT_CC ) {					
				?>
				<div class="large-12 columns">				
					<label for="cc_name_on_card">Name on your card *&nbsp;</label>
					<input name="cc_name_on_card" id="cc_name_on_card" type="text" value="<?=$_POST['cc_name_on_card'] ?>" placeholder="Enter the name of the authorized user as it appears on the card." required="required">					
				</div>

				<div class="large-12 columns">				
					<label for="cc_type">Card Type *&nbsp;</label>
						<select name="cc_type" required>
						<option value="">Select the card type</option>
						<?php 
						foreach ( ordersCardTypes() AS $cValue ) {
							echo '<option value="'.$cValue.'"';
							if ( $_POST['cc_type'] == $cValue ) { echo 'SELECTED'; }
							echo '>' . $cValue . '</option>';
						}
						?>
						</select>
				</div>

				<div class="large-12 columns">				
					<label for="cc_number">Card Number *&nbsp;</label>
					<input class="maskWholeNumber" name="cc_number" id="cc_number" type="text" value="" placeholder="Enter the number on the card." required="required">					
				</div>

				<div class="large-6 columns">
					<label for="cc_exp_month">Month</label>
					<select name="cc_exp_month" id="cc_exp_month">
						<?php
						foreach ( $aMonths AS $cKey => $cValue ) {		
							echo '<option value="' . $cKey . '"';
							echo ( $_POST['cc_exp_month'] == $cKey ) ? ' selected="selected"' : false;
							echo '>' . $cValue . '</option>';
						}
	
						?>
						</select>
				</div>
				<div class="large-6 columns">				
					<label for="cc_exp_month">Year</label>
					<select name="cc_exp_year" id="cc_exp_year">
						<?php
   
						if ( $_POST['cc_exp_year'] == NULL || !is_numeric( $_POST['cc_exp_year'] ) ) { $_POST['cc_exp_year'] = date( 'Y' ); }
							for ( $i = (date("Y")); $i <= date("Y")+8; $i++ ) {
								echo '<option value ="'.$i.'"';
								echo ( $_POST['cc_exp_year'] == $i ) ? ' selected="selected"' : false;
								echo '>'.$i.'</option>';
							}
							?>
						</select>
				</div>

				<div class="large-12 columns">				
					<label for="cc_cvv2">Security Code *&nbsp;<a href="javascript:;" class="large" data-dropdown="help-cvv2">what is this?</a></label>
					<input class="maskWholeNumber" name="cc_cvv2" id="cc_cvv2" type="text" value="" placeholder="Enter the number on the card." maxlength="4" required="required">					
				</div>				
				<input type="submit" value="review order" class="button right">
			<?php
			}			
			?>					
			<?php 
			echo (PAYPAL_EXPRESSCHECKOUT == 1) ? '<center><a href="/' . $rootDirectory .'/PP_ExpressCheckout.php" border="0"><img src="/' . $rootDirectory . '/images/btn_xpressCheckout.gif"></a></center>' : false;	
			?>
			</div>
		</div>
	</div>
</div>

</form>
<div id="help-cvv2" data-dropdown-content class="large f-dropdown content">
  <?php include '../help/help_cvv2.php'; ?>
</div>
<div id="help-address" data-dropdown-content class="large f-dropdown content">
  <?php include '../help/help_address.php'; ?>
</div>
<div id="help-email" data-dropdown-content class="large f-dropdown content">
  <?php include '../help/help_email.php'; ?>
</div>
<div id="help-phone" data-dropdown-content class="large f-dropdown content">
  <?php include '../help/help_phone.php'; ?>
</div>

<script language="javascript">
if (!assocArray) var assocArray = new Object();
<?=$cJavascript ?>

function setBillTo( oForm ) {
	if (oForm.sameas.checked == 1 ) {	
		oForm.bill_name_salutation.value = "<?=addslashes( $aBasket['ship_name_salutation']) ?>";
		oForm.bill_name_first.value = "<?=addslashes( $aBasket['ship_name_first']) ?>";
		oForm.bill_name_last.value = "<?=addslashes( $aBasket['ship_name_last']) ?>";
		oForm.bill_company.value = "<?=addslashes( $aBasket['ship_company']) ?>";
		oForm.bill_address_1.value = "<?=addslashes( $aBasket['ship_address_1']) ?>";
		oForm.bill_address_2.value = "<?=addslashes( $aBasket['ship_address_2']) ?>";
		oForm.bill_address_3.value = "<?=addslashes( $aBasket['ship_address_3']) ?>";
		oForm.bill_address_city.value = "<?=addslashes( $aBasket['ship_address_city']) ?>";
	    oForm.bill_address_country.value = "<?=addslashes( $aBasket['ship_address_country']) ?>";
    	listboxItemSelected( oForm.bill_address_country , oForm.bill_address_stateid , 'state_');
    	oForm.bill_address_stateid.value = "<?=addslashes( $aBasket['ship_address_stateid']) ?>";
    	oForm.bill_address_phone.value = "<?=addslashes( $aBasket['ship_address_phone']) ?>";
	    oForm.bill_address_postal_code.value = "<?=addslashes( $aBasket['ship_address_postal_code']) ?>";
    	oForm.bill_email.value = "<?=addslashes( $aBasket['ship_email']) ?>";    	
    }
}
</script>
<?php 

include '../basket/contents.php'; 
include '../_footer.php'; 
