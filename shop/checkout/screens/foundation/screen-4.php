<?php
	$cPageTitle = 'step 4 of 5: review order';
	include '../_header.php';    
?>

<div class="row">
	<div class="large-12 columns">
		<h1>
			<?=$cPageTitle ?>
		</h1>
	</div>
</div>

<div class="row" id="errorMessage">
	<div class="large-12 columns errorMessage">
		<?= $errorMessage; ?>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
	<ol>
		<li>Review the details of your order below, you can make changes to your order by using the "Change" button.</li>
		<li>Your order is <b>NOT</b> complete until you click <b>"send my order"</b>.</li>
		<li>** after you click on <b>"send my order"</b>, please allow 45 seconds to 1 minute for us to process your order.<br/>your receipt and order confirmation number will appear when processing is complete.</li>
	</ol>			
</div>

		<div class="row">
			<div class="large-4 columns">
				<div class="panel">
							<h4>
								Ship To:
							</h4>
							<?=$aBasket['ship_address_1'] ?><br/>
							<?php
							echo !empty($aBasket['ship_address_2']) ? $aBasket['ship_address_2'].'<br/>' : false;
							echo !empty($aBasket['ship_address_3']) ? $aBasket['ship_address_3'].'<br/>' : false;
							?>
							<?=$aBasket['ship_address_city'] ?> <?=$aBasket['ship_address_state'] ?> <?=$aBasket['ship_address_postal_code'] ?><br/>
							<?=$aBasket['ship_country_iso_3166'] ?><br />
							<?php echo !empty($aBasket['ship_company']) ? $aBasket['ship_company'].'<br/>' : false; ?>
							Phone: <?=$aBasket['ship_address_phone'] ?><br/>
							Email: <?=$aBasket['ship_email'] ?><br/>
							<?php echo !empty($aBasket['ship_msg']) ? $aBasket['ship_msg'].'<br/>' : false; ?>
							<?php 
							if ( $aBasket['gift'] ) { 
								echo 'Gift Reciept: ';
								echo !empty($aBasket['gift_receipt']) ? 'Yes' . '<br/>' : 'No' . '<br/>'; 
								echo 'Gift Wrapping: ';
								echo !empty($aBasket['gift_wrap']) ? 'Yes' . '<br/>' : 'No' . '<br/>'; 
								echo 'Gift Message: ';
								echo !empty($aBasket['gift_msg']) ? $aBasket['gift_msg'].'<br/>' : false;
							} 
							?>
							<br/>
							<a class="button tiny" href="/<?= $rootDirectory ?>/checkout/?step=setshipping">change</a>					
				</div>
				
			</div>
			<div class="large-4 columns">
				<div class="panel">
					<h4>
						Bill To:
					</h4>
					<?=$aBasket['bill_address_1'] ?><br/>
					<?php
					echo !empty($aBasket['bill_address_2']) ? $aBasket['bill_address_2'] . '<br/>' : false;
					echo !empty($aBasket['bill_address_3']) ? $aBasket['bill_address_3'] . '<br/>' : false;
					?>
					<?=$aBasket['bill_address_city'] ?> <?=$aBasket['bill_address_state'] ?> <?=$aBasket['bill_address_postal_code'] ?><br/>
					<?=$aBasket['bill_country_iso_3166'] ?><br />
					<?php echo !empty($aBasket['bill_company']) ? $aBasket['bill_company'].'<br/>' : false; ?>
					Phone: <?=$aBasket['bill_address_phone'] ?><br/>
					Email: <?=$aBasket['bill_email'] ?><br/>
					<?php echo !empty($aBasket['bill_msg']) ? $aBasket['bill_msg'].'<br/>' : false; ?>

					<?php echo !empty($aBasket['referral']) ? 'How did you hear about us: <br />' . $aBasket['bill_msg'].'<br/>' : false; ?>			
					<br />
					<a class="button tiny" href="/<?= $rootDirectory ?>/checkout/?step=setbilling">change</a>					
				</div>

			</div>
			<div class="large-4 columns">
				<div class="panel" style="display: inline-block;">
									<div class="rows">
										<div class="large-12 columns">
											<h4>
												Payment Information
											</h4>
										</div>
									</div>

									<div class="rows">
										<div class="large-6 columns">
										 	Type: 
										</div>
										<div class="large-6 columns">
											<?= $aBasket['cc_type'] ?>
										</div>
									</div>

									<div class="rows" style="display: inline-block">
										<div class="large-6 columns">
											Number:
										</div>
										<div class="large-6 columns" style="word-break:break-all;">
											<?= $aBasket['cc_number'] ?>
										</div>
									</div>

									<div class="rows">
										<div class="large-6 columns">
											Exp Date: 
										</div>
										<div class="large-6 columns">
											<?= $aBasket['cc_exp_month'] ?>/<?= $aBasket['cc_exp_year'] ?>
										</div>
									</div>

									<div class="rows">
										<div class="large-6 columns">
											Security Code: 
										</div>
										<div class="large-6 columns">
											<?= $aBasket['cc_cvv2'] ?>
										</div>
									</div>					
				</div>				
				
			</div>
		</div>


<div class="row">
	<div class="large-12 columns">
		<center>
		<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="tyfoonCart" id="tyfoonCart">
		<input type="hidden" name="step" value="processorder">
		<input type="hidden" name="amount" value="<?=$nSubTotal ?>">				
		<nobr><input type="checkbox" name="toc" style="display: inline;">&nbsp;I agree to the <?=html_link( '/terms-of-sale', 'Terms & Conditions of Sale' , '' , '_blank' ) ?></nobr>		
		<br />
			<input type="submit" value="process my order for <?= _CURRENCY_SYMBOL ?> <?=number_format( ( $aBasket['cart_handling'] + $aBasket['taxes'] + $aBasket['gift_charge'] + $aBasket['ship_charge'] + $aBasket['cart_total']), 2) ?>" class="button center">
		</form>
		</center>		
	</div>
</div>			

<?php

include '../basket/contents.php'; 	
include '../_footer.php';
