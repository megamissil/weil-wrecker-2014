<?php

	$mQuery  = mysql_query("SELECT countryid, title FROM country WHERE active='1' ORDER BY deft DESC,title ASC");
	$cJavascript ='';
	while ( $aCountry = mysql_fetch_assoc( $mQuery )) {
		$mStatQry = mysql_query("SELECT * FROM states WHERE active='1' AND countryid='$aCountry[countryid]' ORDER BY title ASC");
		if ( mysql_num_rows( $mStatQry ) == 0 ) {
			$cJavascript .= 'assocArray["state_'.$aCountry['countryid'].'"] = new Array ("0","Not Applicable",';
		} else {
			$cJavascript .= 'assocArray["state_'.$aCountry['countryid'].'"] = new Array ("0","Select State/Prov",';
		}
		while ( $aStates = mysql_fetch_assoc($mStatQry)) {
			$cJavascript .= "\"$aStates[stateid]\",\"".stripslashes($aStates['title']).'",';
		}
		$cJavascript = substr($cJavascript, 0, -1);
		$cJavascript .= ');';
	}

   	$cPageTitle = 'step 1 of 5: set shipping address';

   	include '../_header.php';

?>
<script language="javascript">if (!assocArray) var assocArray = new Object();
	<?=$cJavascript ?>
</script>
<div class="row">
	<div class="large-12 columns">
		<h1>
			<?=$cPageTitle ?>
		</h1>
	</div>
</div>
<div class="row" id="errorMessage">
	<div class="large-12 columns errorMessage">
		<?= $errorMessage; ?>
	</div>
</div>
<?php
if (!empty($_POST['error'])) {
	echo '';
} elseif ( $_GET['step'] != NULL ) {
	echo '<div class="row"><div class="large-12 columns"><div class="errorMessage">You have elected to change the shipping address, you will need to provide the complete shipping address, so we may select your shipping method and apply any applicable sales taxes to the new address.</div></div></div>';
} else {
	echo '<p>Make sure you get your stuff! If the address is not entered correctly, your package may be returned as undeliverable. You would then have to place a new order. Save time and avoid frustration by entering the address information in the appropriate boxes and double-checking for typos and other errors. ';
	echo '</p>';
}
?>

<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="tyfoonCart" id="tyfoonCart" class="validate" novalidate>
<input type="hidden" name="step" value="setshippingaddress">
<input type="hidden" name="ship_carrier" value="">
<input type="hidden" name="ship_charge" value="0">
<input type="hidden" name="gift_charge" value="<?=GIFT_WRAP_CHARGE ?>">
<input type="hidden" name="taxes" value="0">

<div id="help-address" data-dropdown-content class="large f-dropdown content">
  <?php include '../help/help_address.php'; ?>
</div>
<div id="help-email" data-dropdown-content class="large f-dropdown content">
  <?php include '../help/help_email.php'; ?>
</div>
<div id="help-phone" data-dropdown-content class="large f-dropdown content">
  <?php include '../help/help_phone.php'; ?>
</div>

<div class="row">
	<div class="large-12 columns">
		<div class="row">
			<div class="large-6 columns">
				<!-- <div class="large-12 columns">
					<label for="ship_name_salutation">Salutation</label>
					<input name="ship_name_salutation" type="text" value="<?=$_POST['ship_name_salutation'] ?>" placeholder="Mr., Mrs, Dr., etc.....">
				</div> -->
				<div class="large-12 columns">
					<label for="ship_name_first">First Name *</label>
					<input name="ship_name_first" id="ship_name_first" type="text" value="<?=$_POST['ship_name_first'] ?>" placeholder="Enter the first name (required)" required="required">
				</div>

				<div class="large-12 columns">
					<label for="ship_name_last">Last Name *</label>
					<input name="ship_name_last" id="ship_name_last" type="text" value="<?=$_POST['ship_name_last'] ?>" placeholder="Enter the last name (required)" required="required">
				</div>

				<div class="large-12 columns">
					<label for="ship_email">Email *&nbsp;</label>
					<input name="ship_email" id="ship_email" class="isValidField" data-type="email" type="text" value="<?=$_POST['ship_email'] ?>" placeholder="Enter the email address (required) ex: disco@example.com">
				</div>

				<div class="large-12 columns">
					<label for="ship_address_phone">Phone *&nbsp;</label>
					<input name="ship_address_phone" id="ship_address_phone" type="text" value="<?=$_POST['ship_address_phone'] ?>" placeholder="Enter the phone number (required) ex: (907) 555-1212" required="required">
				</div>

				<div class="large-12 columns">
					<label for="ship_company">Company</label>
					<input name="ship_company" id="ship_company" type="text" value="<?=$_POST['ship_company'] ?>">
				</div>

				<div class="large-12 columns">
					<label for="ship_msg">Special Instructions</label>
					<textarea class="tyfoonCartTextArea" placeholder="if you have any special instructions, please provide them."><?=$_POST['ship_msg'] ?></textarea>
				</div>

			</div>
			<div class="large-6 columns">
				<div class="large-12 columns">
					<label for="ship_address_1">Street Address *</label>
					<input name="ship_address_1" id="ship_address_1" value="<?=$_POST['ship_address_1'] ?>" placeholder="Street Address (required) ex: 1234 Main Street" type="text" data-errormessage="Enter a street address." required="required">
    			</div>
				<div class="large-12 columns">
					<label>&nbsp;</label>
					<input name="ship_address_2" value="<?=$_POST['ship_address_2'] ?>" type="text" placeholder="Apartment or Suite Number (optional)">
    			</div>
				<div class="large-12 columns">
					<label>&nbsp;</label>
					<input name="ship_address_3" value="<?=$_POST['ship_address_3'] ?>" type="text" placeholder="additional address information (optional)">
    			</div>
				<div class="large-12 columns">
					<label for="ship_address_city">City *</label>
					<input name="ship_address_city" id="ship_address_city" value="<?=$_POST['ship_address_city'] ?>" placeholder="City (required)" type="text" data-errormessage="Enter a city name. (required)" required="required">
    			</div>
				<div class="large-12 columns">
					<label for="ship_address_stateid">State/Prov *</label>
						<select name="ship_address_stateid" id="ship_address_stateid" required="required">
						<option value="">Select State/Prov (required)</option>
						<?php
							mysql_data_seek($mQuery, 0);
							$aCountry = mysql_fetch_assoc( $mQuery );
							$qState = mysql_query("SELECT * FROM states WHERE countryid='$aCountry[countryid]' ORDER BY deft DESC, title ASC");
							while ( $aState = mysql_fetch_assoc( $qState ) ) {
								echo "<option value=\"$aState[stateid]\"";
								if ( $aState['stateid'] == $_POST['ship_address_stateid'] ) { echo ' selected="selected"'; }
								echo ">".cleanstring( $aState['title'] ).'</option>';
							}
						?>
						</select>

    			</div>
				<div class="large-12 columns">
					<label for="ship_address_country">Country *</label>
						<select name="ship_address_country" id="ship_address_country" onChange="listboxItemSelected(this,this.form.ship_address_stateid,'state_');" class="finput full" tabindex="12">
						<?php
						mysql_data_seek($mQuery, 0);

						while ( $aCountry = mysql_fetch_assoc( $mQuery ) ) {
							echo "<option value=\"$aCountry[countryid]\"";
							if ( $aCountry['countryid'] == $_POST['countryid'] ) { echo 'SELECTED'; }
							echo ">".cleanstring( $aCountry['title'] ).'</option>';
						}

						?>
						</select>

    			</div>
				<div class="large-12 columns">
					<label for="ship_address_postal_code">Postal Code *</label>
					<input name="ship_address_postal_code" id="ship_address_postal_code" value="<?=$_POST['ship_address_postal_code'] ?>" placeholder="Enter the zip code (required)" type="text" data-errormessage="Enter a postal code. (required)" required="required">
    			</div>
				<div class="large-12 columns">
					<input type="submit" value="proceed to shipping" class="button right">
				</div>
			</div>
		</div>
	</div>
</div>
</form>


<?php
include '../_footer.php';
