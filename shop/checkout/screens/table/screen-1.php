<?php

	$mQuery  = mysql_query("SELECT countryid, title FROM country WHERE active='1' ORDER BY deft DESC,title ASC");

	$cJavascript ='';

	while ( $aCountry = mysql_fetch_assoc( $mQuery ) ) 
	{

		$mStatQry = mysql_query("SELECT * FROM states WHERE active='1' AND countryid='$aCountry[countryid]' ORDER BY title ASC");
		
		if ( mysql_num_rows( $mStatQry ) == 0 )
		{

			$cJavascript .= 'assocArray["state_'.$aCountry['countryid'].'"] = new Array ("0","Not Applicable",';
			
		}
		else {
		
			$cJavascript .= 'assocArray["state_'.$aCountry['countryid'].'"] = new Array ("0","Select State\Prov",';
		
		}
      
		while ( $aStates = mysql_fetch_assoc( $mStatQry ) ) 
		{

			$cJavascript .= "\"$aStates[stateid]\",\"".stripslashes($aStates['title']).'",';
		}

		$cJavascript = substr($cJavascript, 0, -1);

		$cJavascript .= ');';

	} 
      
   	$cPageTitle = 'step 1 of 5: set shipping address';

   	include '_header.php'; 
   
?>

<script type="text/javascript" src="shopping-cart.js"></script>
<script language="javascript">if (!assocArray) var assocArray = new Object();
<?=$cJavascript ?>
</script>

<h1>
	<?=$cPageTitle ?>
</h1>

<div class="fcontent">

	<?php
	
	if ( !empty( $_POST['error'] ) )
	{
	
		echo '<div class="bluebox"><div class="ferror">' . $_POST['error'] . '</div></div>';
	
	}
	elseif ( $_GET['step'] != NULL ) {
	
		echo 'You have elected to change the shipping address, you will need to provide the complete shipping address, so we may select your shipping method and apply any applicable sales taxes to the new address.';
	
	}
	else {
			
		echo 'Make sure you get your stuff! If the address is not entered correctly, your package may be returned as undeliverable. You would then have to place a new order. Save time and avoid frustration by entering the address information in the appropriate boxes and double-checking for typos and other errors. ';
	
		echo "<a href=\"help_address.php\" onclick=\"return hs.htmlExpand(this, { objectType: 'iframe' } )\">Need Help?</a> - items marked with \"*\" are required.";

	}
	?>
</div>
<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="foonster">
<input type="hidden" name="step" value="setshippingaddress">
<input type="hidden" name="ship_carrier" value="">
<input type="hidden" name="ship_charge" value="0">
<input type="hidden" name="gift_charge" value="<?=GIFT_WRAP_CHARGE ?>">
<input type="hidden" name="taxes" value="0">

<table width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td width="50%">
			<table width="100%" cellpadding="5" cellspacing="0">
				<tr>
					<td width="25%" class="<?=$aForm['ship_name_salutation']['style'] ?>">
						<?=$aForm['ship_name_salutation']['label'] ?>
					</td>
					<td width="75%">
						<input name="ship_name_salutation" type="text" value="<?=$_POST['ship_name_salutation'] ?>" class="finput full" tabindex="1">
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['ship_name_first']['style'] ?>">
						<?=$aForm['ship_name_first']['label'] ?>
					</td>
					<td>
						<input name="ship_name_first" type="text" value="<?=$_POST['ship_name_first'] ?>" class="finput full" placeholder="first name of person we are shipping to" tabindex="2" autofocus required>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['ship_name_last']['style'] ?>">
						<?=$aForm['ship_name_last']['label'] ?>
					</td>
					<td>
						<input name="ship_name_last" type="text" value="<?=$_POST['ship_name_last'] ?>" class="finput full" placeholder="last name of person we are shipping to" tabindex="3" required>
					</td>
				</tr>
				<tr>
					<td class="<?=$aForm['ship_company']['style'] ?>">
						<?=$aForm['ship_company']['label'] ?>
					</td>
					<td>
						<input name="ship_company" type="text" value="<?=$_POST['ship_company'] ?>" class="finput full" tabindex="4">
					</td>
				</tr>
				<tr>
					<td class="<?=$aForm['ship_email']['style'] ?>">
						<?=$aForm['ship_email']['label'] ?><br />
						<font size="-1">
							<a href="help_email.php"  onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )">(why required?)</a>
						</font>
					</td>
					<td>
						<input name="ship_email" type="text" value="<?=$_POST['ship_email'] ?>" class="finput full" placeholder="a valid email address" tabindex="5" required>
					</td>
				</tr>
				<tr>
					<td class="flabel align-right">
						Special Instructions
					</td>
					<td>
						<textarea name="ship_msg" class="finput" style="width: 100%;" rows="5" tabindex="6"><?=$_POST['ship_msg'] ?></textarea>
					</td>
				</tr>
			</table>
			
		</td>
		<td width="50%">
			<table width="100%" cellpadding="5" cellspacing="0">
				<tr>
					<td class="<?=$aForm['ship_address_1']['style'] ?>" width="25%">
						<?=$aForm['ship_address_1']['label'] ?><br />
						<!--<font size="-1">
							<a href="#?w=400" rel="popup4" class="poplight">(apo/fpo)</a>						
						</font>
						-->
					</td>
					<td width="75%">
						<input name="ship_address_1" type="text" value="<?=$_POST['ship_address_1'] ?>" class="finput full" placeholder="the shipping address" tabindex="7" required>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['ship_address_2']['style'] ?>">
						&nbsp;
					</td>
					<td>
						<input name="ship_address_2" type="text" value="<?=$_POST['ship_address_2'] ?>" class="finput full" tabindex="8">
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['ship_address_3']['style'] ?>">
						&nbsp;
					</td>
					<td>
						<input name="ship_address_3" type="text" value="<?=$_POST['ship_address_3'] ?>" class="finput full" tabindex="9">
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['ship_address_city']['style'] ?>">
						<?=$aForm['ship_address_city']['label'] ?>
					</td>
					<td>
						<input name="ship_address_city" type="text" value="<?=$_POST['ship_address_city'] ?>" class="finput full" placeholder="what city are we shipping to" tabindex="10" required>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['ship_address_state']['style'] ?>">
						<?=$aForm['ship_address_state']['label'] ?>
					</td>
					<td>
						<select name="ship_address_stateid" tabindex="11" class="select" required>
						<option value="0">Select State/Prov</option>
						<?php
							mysql_data_seek($mQuery, 0);
							$aCountry = mysql_fetch_assoc( $mQuery );
							$qState = mysql_query("SELECT * FROM states WHERE countryid='$aCountry[countryid]' ORDER BY deft DESC, title ASC");
							while ( $aState = mysql_fetch_assoc( $qState ) ) {
								echo "<option value=\"$aState[stateid]\"";
								if ( $aState['stateid'] == $_POST['ship_address_stateid'] ) { echo 'SELECTED'; }
								echo ">".cleanstring( $aState['title'] ).'</option>';
							}
						?>  
						</select>
					</td>
				</tr>
				<tr>
					<td class="<?=$aForm['ship_address_country']['style'] ?>">
						<?=$aForm['ship_address_country']['label'] ?>
					</td>
					<td>
						<select name="ship_address_country" onChange="listboxItemSelected(this,this.form.ship_address_stateid,'state_');" class="finput full" tabindex="12">
						<?php
						mysql_data_seek($mQuery, 0);
					
						while ( $aCountry = mysql_fetch_assoc( $mQuery ) ) {
							echo "<option value=\"$aCountry[countryid]\"";
							if ( $aCountry['countryid'] == $_POST['countryid'] ) { echo 'SELECTED'; }
							echo ">".cleanstring( $aCountry['title'] ).'</option>';
						}
						
						?>  
						</select>
					</td>
				</tr>
				<tr>
					<td class="<?=$aForm['ship_address_postal_code']['style'] ?>">
						<?=$aForm['ship_address_postal_code']['label'] ?>
					</td>
					<td>
						<input name="ship_address_postal_code" type="text" value="<?=$_POST['ship_address_postal_code'] ?>" class="finput full" placeholder="the postal code of the shipping address" tabindex="13" required>
					</td>
				</tr>
				<tr>
					<td class="<?=$aForm['ship_address_phone']['style'] ?>">
						<?=$aForm['ship_address_phone']['label'] ?><br />
						<font size="-1">
							<a href="help_phone.php"  onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )">(why required?)</a>
						</font>						
					</td>
					<td>
						<input name="ship_address_phone" type="text" value="<?=$_POST['ship_address_phone'] ?>" class="finput full" placeholder="what number to call" tabindex="14" required>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<br />			
<center>
	<input type="submit" value="proceed to shipping selection" class="fbtn" name="cSubmit">
</center>

<p/>

</form>

<?php 

	include( 'checkout-cart-contents.php' );

	include( '_footer.php' ); 
	
?>