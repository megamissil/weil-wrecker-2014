<table width="100%" cellpadding="5" cellspacing="0" border="0">
<tr valign="top">
<td class="fcartnav">Your Order</td>
</tr>
</table>

<table width="100%" cellpadding="5" cellspacing="0" border="0" class="bborder">
	<tr>
		<td width="1%" class="headline-md">
			&nbsp;
		</td>
		<td width="96%" class="headline-md">
			&nbsp;
		</td>
		<td width="1%" class="headline-md" align="center">
			qty
		</td>
		<?php
		if ( ENABLE_SETUP_FEE ) {
	
			echo '<td width="1%" class="headline-md" align="right">setup</td>';
		
		}
		?>
		<td width="1%" class="headline-md" align="right">
			price
		</td>
		<td width="1%" class="headline-md" align="right">
			total
		</td>
	</tr>
	<?php
   if ( sizeof( $aBasket['products'] ) > 0 ) {
  
      $nTabIndex = 0;
      
      $nSubTotal = 0;
      
      foreach ( $aBasket['products'] AS $cKey => $aItem ) {
                  
         echo '<tr valign="top" class="fcartrow"><td class="fcontent" align="center">';

         if ( $aItem['thumb'] != NULL ) {

			echo html_link("/shop/$aItem[productid]/".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-" , strtolower( trim( $aItem['title'] ) ) ) ).'.htm', '<img src="'.$aItem['thumb'].'" class="fimage" border="0">' ,'','');            
            
         }
         else {
            
			echo html_link("/shop/$aItem[productid]/".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-" , strtolower( trim( $aItem['title'] ) ) ) ).'.htm', '<img src="/shop/_images/missing.gif" hspace="5" vspace="5" width="50" height="75" class="fimage" border="0">' ,'','');
                        
         }                        
         // - .htaccess
         echo '</td><td class="fcontent">';                 

         echo html_link("/shop/$aItem[productid]/".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-" , strtolower( trim( $aItem['title'] ) ) ) ).'.htm',stripslashes( $aItem['title'] ),'','');    
         echo '<br/>item:'.stripslashes($aItem['sku']);
		if ( $aItem['attribute_text'] != NULL ) {

	         echo '<br/>'.stripslashes($aItem['attribute_text']);
	         
		}		
         
		if ( $aItem['note'] != NULL ) {
			
			echo '<br/>'.stripslashes( nl2br($aItem['note'] ));
	
		}          
         if ( $aItem['promo_code'] != NULL ) {
         
         	echo '<div class="error">Promotional Code<br/>';
         	echo $aItem['promo_code'].':'.$aItem['cart_display'];
         	
         	if ( $aBasket['free_shipping'] == 1 ) {
         	
         		echo '<br/>This item qualifies for FREE SHIPPING.';
         	
         	}
         	
         	echo '</div>';
         
         }

		// configure digital products
		        	            	    
		$mMedia = mysql_query( "SELECT p.mediaid , p.title, o.downloadid, p.pict_name FROM orders_items_media_downloads o LEFT JOIN products_media p USING ( mediaid ) WHERE o.itemid='$aItem[itemid]' AND p.product='1' ORDER BY p.title ASC" );
        	    
		if ( mysql_num_rows( $mMedia ) > 0 ) {
        	    
			echo '<h4>Digital Product Download</h4>';
			
			echo '<ul>';
        	    
			while ( $aMedia = mysql_fetch_assoc( $mMedia ) ) {
                  
				$cLink = md5( uniqid() );
    	      
          		$cLink = 'o' . substr( $cLink, 0 , 8 ) . $aMedia['downloadid'] . substr( $cLink, -8 );
						
				if ( FOONSTER_SEO_LINK ) {
        		    
					echo html_link( 'http://' . $_SERVER['HTTP_HOST'] . '/tyfoon/dnld/' . $cLink . '/' . $aMedia['pict_name'] , $aMedia['title'] , '' , '_blank') . '<br />';
							
				}
				else {
												
					echo html_link( 'http://' . $_SERVER['HTTP_HOST'] . '/tyfoon/connect.php?op=dnld&cn=' . $cLink , $aMedia['pict_name'] , '' , '_blank') . '<br />';
						
				}
						
			}
			
			echo '</ul>';
					
		}

         echo '</td>';
         
         echo "<td class=\"fcontent\" align=\"center\"><nobr>".round( $aItem['qty'] ).'</nobr></td>';

         if ( $aItem['promo_code'] != NULL ) {
         
         	if ( $aItem['promo_type'] == 'DOLR' ) {
         	
				 echo '<td class="fcontent" align="right"><nobr>$'.number_format($aItem['price'],2).'<br/><div class="error"> - $'.number_format( $aItem['promo_amount'] , 2 ).'</div></nobr></td>';
				 
				if ( ENABLE_SETUP_FEE ) {

					echo '<td class="fcontent" align="right"><nobr>$'.number_format($aItem['setupfee'],2).'</nobr></td>';
			
				} 				 
				 
				echo '<td class="fcontent" align="right"><nobr>$ '.number_format( ( ($aItem['qty']*( $aItem['price'] - $aItem['promo_amount'] ) ) + $aItem['setupfee'] ),2).'</nobr></td>';
    
				$nSubTotal += ( ( $aItem['qty'] * ( $aItem['price'] - $aItem['promo_amount'] ) ) + $aItem['setupfee'] );

         	}
         	else {
         	
				 echo '<td class="fcontent" align="right"><nobr>$'.number_format($aItem['price'],2).'<br/><div class="error">'.number_format( $aItem['promo_amount'] , 2 ).' % off<br/>$ '.number_format( ( ( $aItem['price'] - ( $aItem['price'] * ( $aItem['promo_amount']/100 ) ) ) ) ,2).'</div></nobr></td>';
				 
				if ( ENABLE_SETUP_FEE ) {

					echo '<td class="fcontent" align="right"><nobr>$'.number_format($aItem['setupfee'],2).'</nobr></td>';
			
				} 				 
				echo '<td class="fcontent" align="right"><nobr>$ '.number_format(($aItem['qty']*( $aItem['price'] - ( $aItem['price'] * ( $aItem['promo_amount']/100 ) ) ) ) , 2).'</nobr></td>';

				$nSubTotal += ( ( $aItem['qty'] * ( $aItem['price'] - ( $aItem['price']*($aItem['promo_amount']/100) ) ) ) + $aItem['setupfee'] );
         	
         	}
    	     
    	 }
    	 else {
    	 
	         echo '<td class="fcontent" align="right"><nobr>$'.number_format($aItem['price'],2).'</nobr></td>';
				if ( ENABLE_SETUP_FEE ) {

					echo '<td class="fcontent" align="right"><nobr>$'.number_format($aItem['setupfee'],2).'</nobr></td>';
			
				} 				 
			echo '<td class="fcontent" align="right"><nobr>$ '.number_format( ( ($aItem['qty'] * $aItem['price'] ) + $aItem['setupfee'] ) , 2 ).'</nobr></td>';

			$nSubTotal += ( ( $aItem['qty'] * $aItem['price'] ) + $aItem['setupfee'] );
    	 
    	 }
    	     
         echo '</tr>';
         
      } 

   }

?>  
</table>

<?

echo '<table width="100%" cellpadding="5" cellspacing="0" border="0">';

      
		if ( sizeof( $aBasket['basket_promos'] ) > 0 ) {
		
			foreach ( $aBasket['basket_promos'] AS $nPromoID => $aItem ) {

				echo '<tr>';
				echo '<td width="99%" class="error align-right">';
       			echo '['.$aItem['code'].'] : ';
				echo $aItem['cart_display'];
				if ( $aItem['free_shipping'] == 1 ) { echo ' : FREE SHIPPING'; }
				echo '</td>';
				echo '<td width="1%" class="fcontent"><nobr>&nbsp;';
					
				if ( $aItem['amount'] > 0 ) {
				
					if ( $aItem['promo_type'] == 'DOLR' ) {
					
						echo '(-$&nbsp;'.number_format( $aItem['amount']  , 2 ).')';
						
						$nSubTotal = ( $nSubTotal - $aItem['amount'] );
					
					}
					else {
					
						echo $aItem['amount'].'% off';
						
						$nSubTotal = ( $nSubTotal - ( $nSubTotal *( $aItem['amount']/100 ) ) );
						
					
					}
					
				}
				
				echo '</nobr></td></tr>';
			
			}
      
      }
            
	echo '</table>';
      

?>

<table width="100%" cellpadding="5" cellspacing="0" border="0">

      <tr>
      	<td class="flabel align-right" width="99%">merchandise subtotal:</td>
      	<td class="fcontent align-right" width="1%">$<?=number_format( $nSubTotal , 2 ) ?></td>
      </tr>

	
	<?php if ( $aBasket['ship_carrier'] != NULL ) { ?>

		<tr>
      		<td class="flabel align-right">
      			<nobr><?=strtolower( $aBasket['ship_carrier'] ) ?>:</nobr>
      		</td>
			<td class="fcontent align-right">
				$<?=number_format( $aBasket['ship_charge'] , 2 ) ?>
			</td>
		</tr>
      
     <?php 
     	} 
     	else { ?>
      
	<tr>
		<td class="flabel align-right">
			<nobr>shipping:</nobr>
		</td>
      	<td class="fcontent align-right">
      		$<?=number_format( 0 , 2 ) ?>
      	</td>
	</tr>

	<?php } 

	?>      
	<tr>
		<td class="flabel align-right">
			<nobr>handling:</nobr>
		</td>
      	<td class="fcontent align-right">
      		$<?=number_format( $aBasket['cart_handling'] , 2 ) ?>
      	</td>
	</tr>

	<?php
	if ( $aBasket['gift_charge'] > 0 ) { ?>

	<tr>
		<td class="flabel align-right">
			<nobr>wrapping charge:</nobr>
		</td>
      	<td class="fcontent align-right">
      		$<?=number_format( $aBasket['gift_charge'] , 2 ) ?>
      	</td>
	</tr>
      
	<?php } ?>

	<tr>
		<td class="flabel align-right">
			<nobr>taxes:</nobr>
		</td>
      	<td class="fcontent align-right">
      		$<?=number_format( $aBasket['taxes'] , 2 ) ?>
      	</td>
	</tr>
	<tr>
      	<td class="flabel align-right">
      		<nobr>total:</nobr>
      	</td>
		<td class="fcontent align-right">
			$<?=number_format( ( $aBasket['taxes'] + $aBasket['cart_handling'] + $aBasket['gift_charge'] + $aBasket['ship_charge'] + $nSubTotal ) , 2 ) ?>
		</td>
	</tr>
</table>