<?php

	$cPageTitle = 'step 4 of 5: review order';

	include '_header.php'; 
   
?>

<h1>
	<?=$cPageTitle ?>
</h1>


<div class="txtbox">
		<?php
		
			if ( $_POST['error'] != NULL ) {
	
				echo '<div class="ferror">' . $_POST['error'] . '</div>';	
	
			}
			else {
			
				echo '1.) Review the details of your order below, you can make changes to your order by using the "Change" button.<p/>';
				
				echo '2.) Your order is <b>NOT</b> complete until you click <b>"send my order"</b>.<p/>';
				
				echo '** after you click on <b>"send my order"</b>, please allow 45 seconds to 1 minute for us to process your order.<br/>your receipt and order confirmation number will appear when processing is complete.';
			
			}
			
		?>		
</div>

<?php include( 'checkout-cart-contents.php' ); ?>	

	<table align="center" width="100%" cellpadding="5" cellspacing="5">
		<tr valign="top">
			<td class="headline-md dashed" width="33%">
				<button type="button" class="fbtn float-right" name="sShip" onClick="location.href='checkout.php?step=setshipping'">change</button>
				Shipping Information			
					<table cellpadding="5" cellspacing="0" width="100%">
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							to:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['ship_name_salutation'] ?> <?=$aBasket['ship_name_first'] ?> <?=$aBasket['ship_name_last'] ?> <?=$aBasket['ship_name_suffix'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							address:
						</td>
						<td class="fcontent">
							<?=$aBasket['ship_address_1'] ?><br/>
							<?php
							if ( $aBasket['ship_address_2'] != NULL ) {
				
								echo $aBasket['ship_address_2'].'<br/>';
				
							}
				
							if ( $aBasket['ship_address_3'] != NULL ) {
				
								echo $aBasket['ship_address_3'].'<br/>';
				
							}
							?>
							<?=$aBasket['ship_address_city'] ?> <?=$aBasket['ship_address_state'] ?> <?=$aBasket['ship_address_postal_code'] ?><br/>
							<?=$aBasket['ship_country_iso_3166'] ?>
						</td>
					</tr>
					<?php
					
					if ( $aBasket['ship_company'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right nobr" width="25%">
							<?=$aForm['ship_company']['label'] ?>:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['ship_company'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>
					<tr valign="top">
						<td class="flabel align-right">
							phone:
						</td>
						<td class="fcontent">
							<?=$aBasket['ship_address_phone'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							email:
						</td>
						<td class="fcontent">
							<?=$aBasket['ship_email'] ?><br />
						</td>
					</tr>
					
					<?php
					
					if ( $aBasket['ship_msg'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right">
							special&nbsp;instructions:
						</td>
						<td class="fcontent">
							<?=nl2br( trim( $aBasket['ship_msg'] )) ?>
						</td>
					</tr>
					<?php
					
					}
					
					if ( $aBasket['gift'] ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel dashed align-right">
							Gift Options
						</td>
						<td class="flabel dashed">
							&nbsp;
						</td>
					</tr>	
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							receipt:
						</td>
						<td class="fcontent" width="75%">
							<?php
							if ( $aBasket['gift_receipt'] ) {
					
								echo 'Yes<br />';
						
							}
							else {
						
								echo 'No<br />';
					
							}
							?>
						</td>
					</tr>					
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							wrapping:
						</td>
						<td class="fcontent" width="75%">
							<?php
							if ( $aBasket['gift_wrap'] ) {
					
								echo 'Yes<br />';
						
							}
							else {
						
								echo 'No<br />';
					
							}
							?>
						</td>
					</tr>					
					
					<tr valign="top">
						<td class="flabel align-right">
							message:
						</td>
						<td class="fcontent">
							<?=nl2br( trim( $aBasket['gift_msg'] ) ) ?>
						</td>
					</tr>
					<?php
										
				}					

					?>								
				</table>				
			</td>
			<td class="headline-md dashed" width="33%">
				<button type="button" class="fbtn float-right" name="sBill" onClick="location.href='checkout.php?step=setbilling'">change</button>			
				Billing Information						
				
				<table cellpadding="5" cellspacing="0" width="100%">
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							to:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['bill_name_salutation'] ?> <?=$aBasket['bill_name_first'] ?> <?=$aBasket['bill_name_last'] ?> <?=$aBasket['bill_name_suffix'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							address:
						</td>
						<td class="fcontent">
							<?=$aBasket['bill_address_1'] ?><br/>
							<?php
							if ( $aBasket['bill_address_2'] != NULL ) {
				
								echo $aBasket['bill_address_2'].'<br/>';
				
							}
				
							if ( $aBasket['bill_address_3'] != NULL ) {
				
								echo $aBasket['bill_address_3'].'<br/>';
				
							}
							?>
							<?=$aBasket['bill_address_city'] ?> <?=$aBasket['bill_address_state'] ?> <?=$aBasket['bill_address_postal_code'] ?><br/>
							<?=$aBasket['bill_country_iso_3166'] ?>
						</td>
					</tr>
					<?php
					
					if ( $aBasket['bill_company'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right nobr" width="25%">
							<?=$aForm['bill_company']['label'] ?>:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['bill_company'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>
					
					<?php
					
					if ( $aBasket['referral'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right nobr" width="25%">
							<?=$aForm['referral']['label'] ?>:
						</td>
						<td class="fcontent" width="75%">
							<?=$aBasket['referral'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>					
									
					<tr valign="top">
						<td class="flabel align-right">
							phone:
						</td>
						<td class="fcontent">
							<?=$aBasket['bill_address_phone'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							email:
						</td>
						<td class="fcontent">
							<?=$aBasket['bill_email'] ?><br />
							order confirmation and order status will be sent to this address.
						</td>
					</tr>
				</table>					
			</td>
			<td width="34%">
				<table cellpadding="5" cellspacing="0" width="100%">
			<?php
					
					if ( $aBasket['payment_method'] == 'PAYMENT_MO' ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel dashed" colspan="2">
							You are paying by Check
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							<?=$aForm['check_route_num']['label'] ?>
						</td>
						<td class="fcontent">
							<?=$aBasket['check_route_num'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							<?=$aForm['check_account_num']['label'] ?>
						</td>
						<td class="fcontent">
							<?=$aBasket['check_account_num'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							<?=$aForm['check_num']['label'] ?>
						</td>
						<td class="fcontent">
							<?=$aBasket['check_num'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							&nbsp;
						</td>
						<td class="fcontent">
							<p>
							Although we do accept Personal Checks, we must wait seven "7" business days after the day of 
							deposit for all	checks to clear, and we will only accept checks that
							are imprinted with your name and address. There is a fifty "$50.00" fee for
							bounced checks. <p />
							<b>DO NOT SEND US CASH! WE DO NOT ACCEPT CASH.</b> 
							</p>	
						</td>
					</tr>

					<?php
					
					}
					
					?>
					
					
					<?php
					
					if ( $aBasket['payment_method'] == 'PAYMENT_PO' ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel dashed" colspan="2">
							You are submitting a purchase order
						</td>
					</tr>
					<?php
					
					if ( $aBasket['ext_ref_number'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right">
							reference:
						</td>
						<td class="fcontent">
							<?=$aBasket['ext_ref_number'] ?>
						</td>
					</tr>
					<?php
					
					}
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right">
							&nbsp;
						</td>
						<td class="fcontent">
							<b><i><?=$aMember['name_first'] ?> <?=$aMember['name_last'] ?></i></b> you are authorized to order products using a purchase order.  
							please note that a purchase order is non-binding until we have notified you that it has been accepted.
	
						</td>
					</tr>
										
					<?php
					
					}
					
					?>
					
					<?php
					
					if ( $aBasket['payment_method'] == 'PAYMENT_CC' ) {
					
					?>
					
					
					<tr valign="top">
						<td class="flabel dashed" colspan="2">
							You are paying by credit card
						</td>
					</tr>	
					
					<tr valign="top">
						<td class="flabel align-right">
							type:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_type'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							number:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_number'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							exp date:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_exp_month'] ?>/<?=$aBasket['cc_exp_year'] ?>
						</td>
					</tr>
					<tr valign="top">
						<td class="flabel align-right">
							cvv2 code:
						</td>
						<td class="fcontent">
							<?=$aBasket['cc_cvv2'] ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>
					
					<?php
					
					if ( $aBasket['bill_msg'] != NULL ) {
					
					?>
					
					<tr valign="top">
						<td class="flabel align-right" width="25%">
							special&nbsp;instructions:
						</td>
						<td class="fcontent" width="75%">
							<?=nl2br( trim( $aBasket['bill_msg'] )) ?>
						</td>
					</tr>
					
					<?php
					
					}
					
					?>					
					
				</table>
			</td>
			
		</tr>

		<tr>
			<td colspan="3" align="center" class="fcontent">
				<br/>
				<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="foonster">
					<input type="hidden" name="step" value="processorder">
					<input type="hidden" name="amount" value="<?=$nSubTotal ?>">				
					<nobr><input type="checkbox" name="toc" style="display: inline;">&nbsp;I agree to the <?=html_link( '/terms-of-sale.htm', 'Terms & Conditions of Sale' , '' , '_blank' ) ?></nobr>		
					<p />
					<center>
						<input type="submit" value="process my order for $ <?=number_format( ( $aBasket['cart_handling'] + $aBasket['taxes'] + $aBasket['gift_charge'] + $aBasket['ship_charge'] + $nSubTotal ) , 2 ) ?>" class="fbtn" name="cSubmit">		
					</center>				
				</form>
			</td>
		</tr>
	</table>
	
<?php
	
	include( '_footer.php' ); 
?>