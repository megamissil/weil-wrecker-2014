<?php

	require_once( '_connect.php' );
	
	if ( empty( $aMember['memberid'] ) ) { 
	
		redirect( 'orders.php' ); 
		
	}
	
	$aOrder = memberOrder( $_GET['cn'] );
	
	if ( $aOrder['orderid'] == 0 ) { 
	
		redirect( 'orders.php' ); 
		
	}	
					
	$cPageTitle = html_link( '/members/' . $aMember['handle'] , stripslashes( $aMember['handle'] ) ) . ' : Order : ' . sprintf("%07d",$aOrder['orderid'] );
	   	
	include( '_header.php' ); 

	include( 'navigation-user.php' ); 

?>	
&nbsp;
<hr />

	<?=$aOrder['html'] ?>

<?php
	
	include '../_footer.php'; 
	
?>