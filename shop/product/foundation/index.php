<?php // ADD BIG HEADER IMGS TO COOKIES AND ULTIMATE BOX PRODUCT PAGES
	if ( $product->productid == '1027' || $product->productid == '1001' )
	{ ?>
		<div class="has-big-header-img">
			<div class="row">
				<div class="small-12 columns">
					<?php if ( $product->productid == '1001' )
					{ ?>
						<img src="/img/rotator/Half-Moon-Cookies.JPG" alt="Ultimate BBQ Box full 2" width="100%" />
					<?php } else
					{ ?>
						<img src="/img/box-full-3.jpg" alt="Ultimate BBQ Box full 2" width="100%" />
					<?php } ?>
				</div>
			</div>
		</div>
	<?php  } ?>
<div class="row">
	<div class="small-12 columns">
		<h3><a href="/merchandise.php" style="color:white;">&#10094; Back to Moon Swag</a></h3>
	</div>
	<div class="small-12 columns">
		<div class="row">
			<div class="medium-4 columns panel">
				<?php if ( $product->productid == '1027' || $product->productid == '1015' || $product->productid == '1016' || $product->productid == '1017' || $product->productid == '1018' || $product->productid == '1019' || $product->productid == '1020' || $product->productid == '1021' || $product->productid == '1021' || $product->productid == '1022' )
				{ ?>
					<ul class="clearing-thumbs clearing-feature" data-clearing>
						<li class="clearing-featured-img"><a href="<?= $product->images[1]['image']; ?>"><img src="<?= $product->images[1]['image']; ?>" alt="" /></a></li>
						<?php if (!empty( $product->images[2] ) )
						{ ?>
							<li><a href="<?= $product->images[2]['image']; ?>"><img src="<?= $product->images[2]['image']; ?>" alt="" /></a></li>
						<?php } else
						{ ?>
							<li><a href="<?= $product->images[1]['thumb']; ?>"><img src="<?= $product->images[1]['thumb']; ?>" alt="" /></a></li>
						<?php } ?>

					</ul>
				<?php } else
				{ ?>
					<img src="<?= $product->images[1]['image']; ?>" alt="" />
				<?php } ?>

			</div>
			<div class="medium-8 columns">
				<article class="product-description">
					<?php
						include "$framework/order-form.php";
						//include "../share-this.php";
					?>

					<hr>

					<div class="row">
						<div class="medium-12 columns product-desc-detail">
							<?= $product->msg ?>
						</div>
					</div>
				</article>
			</div>
		</div>
		<?php
			#include "$framework/also-purchased.php";
		?>
	</div>
</div>
