<form action="/<?= $webroot ?>/basket/additem.php" method="POST" name="foonster" id="foonster"> <!-- PRODUCT FORM -->
<input type="hidden" name="type" value="PLP">
<input type="hidden" name="cn" value="<?=$product->productid ?>">
	<?php
	if (!empty($errorMessage) && $_SESSION['FERROR_PROD'] === $product->productid) {
		echo '<div class="row"><div class="small-12 columns"><div class="errorMessage">Error: ' . $errorMessage . '</div></div></div>';
	}
	?>

	<div class="row">

		<div class="medium-12 small-12 columns">
			<h1><?php echo $product->title ?></h1>
			<p>
				<?php echo $product->msg_short ?>
			</p>
			<br />
			<h2 class="price">
			<?php
				if ( $product->acctprice > 0 ) 
				{
					echo _CURRENCY_SYMBOL . number_format($product->acctprice, 2 );
				} 
				elseif ( $aOutput['allprice'] > 0 ) 
				{
					echo _CURRENCY_SYMBOL . number_format($product->allprice, 2 );
				} 
				elseif ( $aOutput['price_retail']== 0 ) 
				{
					echo '';
				} 
				else 
				{
					echo _CURRENCY_SYMBOL . number_format($product->price_retail, 2 );
				}
			?>
			</h2>
		</div>


	    <div class="small-6 columns">
<?php
					if (sizeof( $product->attributes ) > 0 ) {
						foreach ( $product->attributes AS $nId => $aAttribute ) {
							if ( strpos($aAttribute['code'],'|') !== false) {

								$initialValue = '';

								// Build array for multiple select boxes
								$splitAttrTitles = explode('|',$aAttribute['code']);
								$i=0;
								foreach ($aAttribute['options'] as $nKey => $aOption) {
									if($i==0) {$initialValue = $aOption['code']; }
									$splitCodes = explode('|',$aOption['code']);
									$splitTitles = explode('|',$aOption['title']);

									// Inventory array for javascript
									if(count($splitCodes) == 2 ) {
										$aInventory[$splitAttrTitles[0]][$splitCodes[0]]['qty'] += $aOption['qty'];
										$aInventory[$splitAttrTitles[0]][$splitCodes[0]][$splitAttrTitles[1]][$splitCodes[1]]['qty'] = $aOption['qty'];
									} else if(count($splitCodes) == 3) {
										$aInventory[$splitAttrTitles[0]][$splitCodes[0]]['qty'] += $aOption['qty'];
										$aInventory[$splitAttrTitles[0]][$splitCodes[0]][$splitAttrTitles[1]][$splitCodes[1]]['qty'] += $aOption['qty'];
										$aInventory[$splitAttrTitles[0]][$splitCodes[0]][$splitAttrTitles[1]][$splitCodes[1]][$splitAttrTitles[2]][$splitCodes[2]] = $aOption['qty'];
									}

									foreach($splitCodes as $key => $aCode) {
										$separatedSelect[$splitAttrTitles[$key]][$aCode] = $splitTitles[$key];
									}

									$i++;

								}
							?>
								<script type="text/javascript">
									var inventory = <?php echo json_encode($aInventory); ?>;
									var attr_id = '<?php echo $nId; ?>';
									$(document).ready(function() {
										disableEmptyQuantities();
										$('select.build').change(function() {
											var newSubmitValue = $( $('select.build').get(0) ).val() + '|' + $( $('select.build').get(1) ).val();
											console.log(newSubmitValue);
											$('input[name="attr_'+attr_id+'"]').val(newSubmitValue);
											disableEmptyQuantities();
										});
									});

									function disableEmptyQuantities() {
										$('select.build option').prop('disabled',false);
										for (var attr in inventory) {
											for (var attr2 in inventory[attr]) {
												var $currentOption = $('select[name="build_attr_'+attr+'"]').children('option[value="'+attr2+'"]');
												if(inventory[attr][attr2].qty == 0) {
													$currentOption.prop('disabled',true);

												}
												if( $currentOption.prop('selected') ) {
													var $firstSelected = $currentOption;
													for(var attr3 in inventory[attr][$firstSelected.val()]) {
														for(var size in inventory[attr][$firstSelected.val()][attr3]) {
															if(inventory[attr][$firstSelected.val()][attr3][size].qty == 0) {
																$currentSecondOption = $('select[name="build_attr_'+attr3+'"]').children('option[value="'+size+'"]');
																console.log('size disable')
																console.log($currentSecondOption);
																$currentSecondOption.prop('disabled',true);
															}
														}
													}
												}

											}
										};
									}


								</script>
								<input type="hidden" name="attr_<?=$nId ?>" value="<?=$initialValue ?>">
								<div class="row">
									<?php
										foreach( $separatedSelect as $sAttributeID => $aSelect) {
									?>
										<div class="small-6 columns">
											<label class="quantity text-left">
												<?php echo $sAttributeID ?>
											</label>
											<select name="build_attr_<?=$sAttributeID ?>" id="" class="build">
												<?php
													$i = 0;
													foreach ($aSelect as $key => $value):
												?>
													<option value="<?=$key ?>" <?php if ($i==0) { echo 'selected'; } ?>><?=$value ?></option>
													<?php $i++; ?>
												<?php
													endforeach
												?>
											</select>
										</div>

									<?php
										}
									?>
								</div>

					<?php

							} else if ( $aAttribute['type'] == 'RQRD' ) {
					?>
							<label class="quantity text-left">

							</label>
							<select name="attr_<?=$nId ?>" style="margin-top:1.3rem;">
								<option disabled="Sizes" selected="">SELECT ONE</option>
								<?php
								foreach ( $aAttribute['options'] AS $cKey => $aValue ) {
									if ( $aValue['qty'] > 0 ) {
										echo '<option value="' . $aValue['optionid'] . '">' . stripslashes( $aValue['title'] );
										if ( $aValue['price'] != 0 ) {
											if ( $aValue['price'] < 0 ) {
												echo ' (minus: ' . _CURRENCY_SYMBOL . number_format( $aValue['price'] , 2 ) . ')';

											} else {
												echo ' (add: ' . _CURRENCY_SYMBOL . number_format( $aValue['price'] , 2 ) . ')';

											}
										}
										echo '</option>';
									}
								}
								?>
							</select><br/>
					<?php
							}
						}
					}
					?>

	    </div>


		<div class="medium-2 small-6 left columns">
			<label for="right-label" class="quantity">
				Quantity
			</label>
			<select name="qty">
			<?php if ($product->manufacturer_sku == '83704413FF') { ?>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				
				<?php } elseif ($product->manufacturer_sku == '29617B442B') { ?>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>					
				<?php } else { ?>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				<?php } ?>

			</select>
			<!-- <input type="text" name="qty" value="1" size="4" maxlength="<?php echo strlen($product->limit_per_sale); ?>" class="maskWholeNumber textCenter"> -->
	    </div>
	</div>

	<?php /*
	<div class="row">
		<div class="small-12 columns">
			<p class="stock"><?= number_format($product->qty, 0) ?> ITEMS IN STOCK</p>
		</div>
	</div>
	*/ ?>

	<hr>
	<div class="row"> <!-- PRICING AND PURCHASE BUTTON -->
		<div class="small-4 columns add-cart">
	    	<input type="submit" class="button" value="Add To Cart">
		</div>

	</div>
</form> <!-- PRODUCT FORM END -->
