<form action="/<?= $webroot ?>/basket/additem.php" method="POST" name="foonster" id="foonster"> <!-- PRODUCT FORM -->
<input type="hidden" name="type" value="PLP">
<input type="hidden" name="cn" value="<?=$product->productid ?>">
	<?php		
	if (!empty($errorMessage) && $_SESSION['FERROR_PROD'] === $product->productid) {
		echo '<div class="row"><div class="small-12 columns"><div class="errorMessage">Error: ' . $errorMessage . '</div></div></div>';
	} 
	?>

	<div class="row">
	    <div class="small-6 columns">
				<?php		
					if (sizeof( $product->attributes ) > 0 ) {		  
						foreach ( $product->attributes AS $nId => $aAttribute ) {				
							if ( $aAttribute['type'] == 'RQRD' ) {
					?>
					<select name="attr_<?=$nId ?>">
						<option disabled="Sizes" selected="">SIZES</option>
						<?php
						foreach ( $aAttribute['options'] AS $cKey => $aValue ) {		
							if ( $aValue['qty'] > 0 ) {					
								echo '<option value="' . $aValue['optionid'] . '">' . stripslashes( $aValue['title'] );								
								if ( $aValue['price'] != 0 ) {
									if ( $aValue['price'] < 0 ) {									
										echo ' (minus: ' . _CURRENCY_SYMBOL . number_format( $aValue['price'] , 2 ) . ')'; 

									} else {									
										echo ' (add: ' . _CURRENCY_SYMBOL . number_format( $aValue['price'] , 2 ) . ')'; 	
									
									}
								}
								echo '</option>';
							}
						}
						?>
					</select>
					<?php
							}
						}
					}  
					?>
	    </div>
	    <div class="small-6 columns">
	    	<div class="row">
				<div class="small-5 columns">
					<label for="right-label" class="right quantity">
						Quantity
					</label>
				</div>
				<div class="small-7 columns">
					<input type="text" name="qty" value="1" size="4" maxlength="<?php echo strlen($product->limit_per_sale); ?>" class="maskWholeNumber textCenter">
				</div>
	    	</div>
	    </div>
	</div>
	<div class="row">
	<div class="small-12 columns">
		<p class="stock"><?= number_format($product->qty, 0) ?> ITEMS IN STOCK</p>
	</div>
	</div>
	<hr>
	<div class="row"> <!-- PRICING AND PURCHASE BUTTON -->
		<div class="small-6 columns">
			<p class="price">
			<?php
				if ( $product->acctprice > 0 ) {				
					echo _CURRENCY_SYMBOL . number_format($product->acctprice, 2 ); 				
				} elseif ( $aOutput['allprice'] > 0 ) {
					echo _CURRENCY_SYMBOL . number_format($product->allprice, 2 ); 
				} else {
					echo _CURRENCY_SYMBOL . number_format($product->price_retail, 2 ); 
				}
			?>		
			</p>
		</div>
		<div class="small-6 columns">
	    	<input type="submit" class="fbtn button" value="Add To Cart">	
		</div>
	</div>
</form> <!-- PRODUCT FORM END -->
