<?php

include __DIR__ . '/../connect.php';

$product = (object) productGet(strtoupper(trim(html_entity_decode($_GET['cn']))));

$cSEOTitle = $product->addl_seo_title;
$cMetaDesc = str_replace('"','\"',$product->meta_description);
$cMetaKW = $product->meta_keywords;
$cMetaAbst = $product->meta_abstract;
$cRobotIndex = $product->robot_index;

include __DIR__ . '/../_header.php';

include "$framework/index.php";

include __DIR__ . '/../_footer.php';