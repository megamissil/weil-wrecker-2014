<?php

// LOAD TYFOON POSTING CONFIGURATION

require_once( $_SERVER['DOCUMENT_ROOT'] . '/tyfoon/site/configs/pages-configuration.php' );

if ( PG_COMMENT_REQUIRE_CAPTCHA ) {

	require_once( $_SERVER['DOCUMENT_ROOT'] . '/tyfoon/_classes/recaptchalib.php' );

	$publickey = FOONSTER_CAPTCHA_PUBLICKEY;
	
	$privatekey = FOONSTER_CAPTCHA_PRIVATEKEY;

	$reCAPTCHA = NULL;
	
	$reError = NULL;
	
}

?>
	<a name="fcomment" id="fcomment"></a>
	<h3 class="section-title">Add a Comment</h3>	
<?php

	if ( $_GET['ferror'] != NULL ) {
	
		echo '<div class="ferror">' . $_GET['ferror'] . '</div>';
	
	}
	
	if ( $aMember['memberid'] == 0 ) {
	
	?>

	We encourage users to analyze, comment on and even challenge <?=strtolower( FOONSTER_SITE_TITLE ) ?>'s articles, blogs, reviews 
	and multimedia features.<p />

	Member reviews and comments that include profanity or personal attacks or other inappropriate 
	comments or material will be removed from the site. Additionally, entries 
	that are unsigned or contain "signatures" by someone other than the actual 
	author will be removed. Finally, we will take steps to block users who 
	violate any of our posting standards, <a href="/terms-and-conditions-of-use.php">terms of use</a> or <a href="/privacy-policy.php">privacy policies</a>
	or any other policies governing this site. 
	<p />

	You will need to <a href="/members/logon.php">Log On</a>&nbsp;or&nbsp;<a href="/members/join.php">Create&nbsp;an&nbsp;Account</a> 
	to join the conversation.
	
	<p />
	
	<?php
	
	}
	else {


?>

<style type="text/css">

	#cComment {
	  display: block;
	  width: 175;
	  height: 35;
	  background: url( "/_images/add-comment.png" ) no-repeat 0 0;
	}

	#cComment:hover { 
	  background-position: 0 -35px;
	}

	#cComment span
	{
	  display: none;
	}
	
</style>

<form action="/tyfoon/index.php" method="POST" name="fcomment" id="fcomment">
<input type="hidden" name="op" value="postComment">
<input type="hidden" name="module" value="products">
<input type="hidden" name="cn" value="<?=$aProduct['productid'] ?>">

<table width="100%" cellpadding="5" cellspacing="0">
	<tr>
		<td class="flabel align-right" width="25%">
			Subject:
		</td>
		<td width="75%">
			<input name="subject" id="subject" type="text" class="finput full" value="<?=$aOutput['subject'] ?>">
		</td>
	</tr>

	<tr valign="top">
		<td class="flabel align-right">
			Your Comment:
		</td>
		<td>
			<textarea name="comment" id="comment" class="finput" style=" width: 100%;" rows="8"><?=$aOutput['comment'] ?></textarea>
		</td>
	</tr>

	<tr valign="top">
		<td>
			&nbsp;
		</td>
		<td>
		<?php
				
			if ( PG_COMMENT_REQUIRE_CAPTCHA ) {
			
				echo recaptcha_get_html($publickey, $reError);			
			
			}

			?>
			<p />
			<a id="cComment" href="javascript:;" onclick="document.fcomment.submit();" title="Add Rating"><span>Add Rating</span></a>
			<input type="image" name="sbtbtn" src="/_images/widget.gif" width="1" height="1" /> 
		</td>
	</tr>
</table>

</form>

<?php }


?>