<?php
			
if ( 
	$_COOKIE['FERROR'] != NULL 
	&& $_COOKIE['FERROR_PROD'] == $aOutput['productid'] ) 
{
				
	echo '<div class="ferror">ERROR: ' . $_COOKIE['FERROR'] . '</div>';
				
}
?>
<div class="obox">
	<form action="/shop/additem.php" method="POST" name="fordernow" onsubmit="return validate(this);">
	<input type="hidden" name="cn" value="<?=$aOutput['productid'] ?>">
	<table  cellpadding="10" cellspacing="0" width="100%">
		<tr>
			<td colspan="2" class="bluebar">
				<div class="pull-right" style="margin-top: -6px;"><?=$aOutput['sku'] ?></div>
			</td>
		</tr>
		<tr>
			<td width="50%" class="flabel">
				Price:
			</td>
			<td width="50%">
				<nobr>
				<?php
				if ( $aOutput['acctprice'] > 0 ) {
			
					echo '$' . number_format( $aOutput['acctprice'] , 2 ); 
			
				}
				elseif ( $aOutput['allprice'] > 0 ) {
			
					echo '$' . number_format( $aOutput['allprice'] , 2 ); 
			
				}
				else {
			
					echo '$' . number_format( $aOutput['price_retail'] , 2 ); 
			
				}
					?>			
				</nobr>
			</td>
		</tr>
		
<?php
	
	if ( $aOutput['setupfee'] > 0 && ENABLE_SETUP_FEE ) {
	
	?>

	<tr valign="top">
		<td class="flabel pull-right">
			Setup&nbsp;Fee:
		</td>
		<td class="fcontent" align="left">$&nbsp;<?=number_format( $aOutput['setupfee'] , 2 ) ?></td>
	</tr>

	<?php
	
	}
	
	?>

	<tr valign="top">
		<td class="flabel pull-right">
			Quantity:
		</td>
		<td class="fcontent">
			<input type="text" name="qty" value="1" size="4" maxlength="4" onKeyUp="intgerOnly(this);" onKeyPress="intgerOnly(this);" style="text-align: center;">
		</td>
	</tr>
<?php

	if ( sizeof( $aOutput['attributes'] ) > 0 ) {
   
		foreach ( $aOutput['attributes'] AS $nId => $aAttribute ) {
		
			if ( $aAttribute['type'] == 'RQRD' ) {
			
			?>
				<tr valign="top">
					<td class="flabel pull-right"><?=$aAttribute['title'] ?>:</td>
					<td class="fcontent">
						<select name="attr_<?=$nId ?>">
						<option value=""> -- SELECT -- </option>
						<?php
						
						foreach ( $aAttribute['options'] AS $cKey => $aValue ) {
												
							if ( $aValue['qty'] > 0 ) {
					
								echo '<option value="' . $aValue['optionid'] . '">' . stripslashes( $aValue['title'] );
								
								if ( $aValue['price'] != 0 ) {
								
									if ( $aValue['price'] < 0 ) {
									
										echo ' (minus: $' . number_format( $aValue['price'] , 2 ) . ')'; 
									
									}
									else {
									
										echo ' (add: $' . number_format( $aValue['price'] , 2 ) . ')'; 	
									
									}
																
								}
								
								echo '</option>';
								
							}
					
						}
						?>
					</select>
					</td>
				</tr>	
			<?php
			}
			else {
			
			?>

				<tr valign="top">
					<td class="flabel pull-right"><?=$aAttribute['title'] ?>:</td>
					<td class="fcontent">
						<?php
						foreach ( $aAttribute['options'] AS $cKey => $aValue ) {
						
							if ( $aValue['qty'] > 0 ) {
					
								echo '<input type="checkbox" name="' . 'attr_' . $nId . '[]" value="' . $aValue['optionid'] . '">' . stripslashes( $aValue['title'] ) . '<br />';
								
							}
					
						}
						?>
					</td>
				</tr>				
			
			<?php
			}
		
		}
	
	}  

		?>
		<tr>
			<td colspan="2">
				<center>
					<input type="submit" class="fbtn" value="Add To Order">
				</center>
			</td>
		</tr>		
		
		<tr>
			<td colspan="2">
				<h3 style="color: black;">
					Price Schedule
				</h3>
				<table width="100%">
					<?php 
					foreach ( $aOutput['price_schedule'] AS $n => $a )
					{
					
						echo '<tr>';
						echo "<td width=\"*\">$a[sku]</td>";
						echo "<td width=\"1\"><center>$a[qty]</center></td>";
						echo "<td width=\"1\"><center>\$&nbsp;$a[price]</center></td>";
						echo '</tr>';
				
					}
				?>
				</table>
			</td>
		</tr>		

		<tr>
			<td colspan="2">
				<center>
					<?php include 'share-this.php';?>			
				</center>
			</td>
		</tr>		
	</table>
	</form>
</div>