<?php

if ( sizeof( $aOutput['related'] ) > 0 ) 
{

?>

<h2 class="orange">
	Related Items
</h2>	
	<table width="100%" cellpadding="10" cellspacing="10">
		<?php

		$i = 0;

		foreach ( $aOutput['related'] AS $n => $product )
		{
			$i++;

			echo $i == 1 ? '<tr>' : false;
	
			echo '<td width="25%">';

			$cURL = '/' . $product['sku'] . '~'. urlencode( preg_replace( "/\//", '' , $product['title'] ) ).'.htm';

			echo '<center>';			

			echo '<img src="/tyfoon/site/products/thumbs/' . $product['thumb_path'] . '">';

			echo '<center>';

			echo '<a href="' . $cURL .'">['. $product['sku'] . '] ' . $product['title'] . '</a><hr />';
				
			echo '<p style="font-size: 11px;">' . $product['msg_short'] . '</p>';

			echo '<center><strong>Price: $' . number_format( $product['price_retail'] , 2 ) . '</strong></center>'; 

			echo '</td>';

			echo $i == 4 ? '</tr>' : false;

			echo $i == 4 ? $i = 0 : false;
		}

		if ( $i < 4 ) 
		{

			while ( $i < 4 )
			{

				echo '<td width="25%">&nbsp;</td>';

				$i++;

			}
		
		}

		?>
	</table>	
<?php
}
?>	