<h1>
    <span class="headline-icon"><img src="/img/tow-icon.png" alt="towing icon" /></span>
    <?= $product->title ?>
</h1>
    <div class="row">
        <!-- START SUB CONTENT -->
        <div class="col-md-7 text-center">
            <a href="<?=$product->images[1]['image']; ?>"><img src="<?=$product->images[1]['image']; ?>" alt="<?=$product->title ?>"></a>                            
            <?php 
            if ($member['memberid'] > 0) {
                include "$framework/order-form.php";
            }
            ?>
        </div>
        <div class="col-md-5">
            <div class="info-container">                        
                <div class="vehicle-info">
                <?php 
                echo !empty($product->title) ? '<p><span>Name:</span>' . $product->title . '</p>' : false;
                echo !empty($product->addl_part) ? '<p><span>Part:</span>' . $product->addl_part . '</p>' : false;
                echo !empty($product->addl_size) ? '<p><span>Size:</span>' . $product->addl_size . '</p>' : false;
                echo !empty($product->addl_wll) ? '<p><span>Working Load Limit:</span>' . $product->addl_wll . '</p>' : false;
                echo !empty($product->addl_mbb) ? '<p><span>Minimum Break Strength:</span>' . $product->addl_mbb . '</p>' : false;
                echo !empty($product->addl_length) ? '<p><span>Length:</span>' . $product->addl_length . '</p>' : false;
                echo !empty($product->addl_width) ? '<p><span>Width:</span>' . $product->addl_width . '</p>' : false;
                echo !empty($product->addl_height) ? '<p><span>Height:</span>' . $product->addl_height . '</p>' : false;
                echo !empty($product->addl_color) ? '<p><span>Color:</span>' . $product->addl_color . '</p>' : false;
                echo !empty($product->addl_vertical) ? '<p><span>Vertical WLL:</span>' . $product->addl_vertical . '</p>' : false;
                echo !empty($product->addl_choker) ? '<p><span>Choker: WLL:</span>' . $product->addl_choker . '</p>' : false;
                echo !empty($product->addl_basket) ? '<p><span>Basket WLL:</span>' . $product->addl_basket . '</p>' : false;
                echo !empty($product->addl_unit) ? '<p><span>Unit:</span>' . $product->addl_unit . '</p>' : false;
                echo !empty($product->addl_addl_grade) ? '<p><span>Grade:</span>' . $product->addl_addl_grade . '</p>' : false;
                ?>
                </div>
            </div>
        </div>
    </div><!-- /.row -->
