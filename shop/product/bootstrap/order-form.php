<?php
			
if ($_COOKIE['FERROR'] != NULL && $_COOKIE['FERROR_PROD'] == $product->productid) {
	echo '<div class="row col-sm-12 ferror">ERROR: ' . $_COOKIE['FERROR'] . '</div>';				
}
?>

<form action="/shop/basket/additem.php" method="POST" name="fordernow" onsubmit="return validate(this);">
<input type="hidden" name="cn" value="<?= $product->productid ?>">
<div class="row">
	<div class="col-sm-12">
		<?= $product->sku ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-10 text-right">			
		<strong>Price:</strong>
	</div>
	<div class="col-sm-2">						
	<?php
	if ( $product->acctprice > 0 ) {			
		echo '$' . number_format( $product->acctprice , 2 ); 
	} elseif ($product->allprice > 0 ) {
		echo '$' . number_format( $product->allprice , 2 ); 
	} else {
		echo '$' . number_format( $product->price_retail , 2 ); 
	}
	?>			
	</div>
</div>
<?php
if ( $product->setupfee > 0 && ENABLE_SETUP_FEE ) {
?>
<div class="row">
	<div class="col-sm-10 text-right">			
		<strong>Setup&nbsp;Fee:</strong>
	</div>
	<div class="col-sm-2">			
		$&nbsp;<?=number_format( $product->setupfee , 2 ) ?>
	</div>
</div>
<?php	
}
?>
<div class="row">
	<div class="col-sm-10 text-right">			
		<strong>Qty:</strong>
	</div>
	<div class="col-sm-2">					
		<input type="text" name="qty" value="1" size="4" maxlength="4" onKeyUp="intgerOnly(this);" onKeyPress="intgerOnly(this);" class="form-control text-center">
	</div>
</div>	
<?php
if ( sizeof( $product->attributes ) > 0 ) {   
	foreach ( $product->attributes AS $nId => $aAttribute ) {		
		if ( $aAttribute['type'] == 'RQRD' ) {			
		?>
		<div class="row">
			<div class="col-sm-6 text-right">			
				<strong><?=$aAttribute['title'] ?>:</strong>
			</div>
			<div class="col-sm-6">			
				<select name="attr_<?=$nId ?>">
				<option value=""> -- SELECT -- </option>
				<?php
				foreach ( $aAttribute['options'] AS $cKey => $aValue ) {
					if ( $aValue['qty'] > 0 ) {
						echo '<option value="' . $aValue['optionid'] . '">' . stripslashes( $aValue['title'] );
						if ( $aValue['price'] != 0 ) {
							if ( $aValue['price'] < 0 ) {
								echo ' (minus: $' . number_format( $aValue['price'] , 2 ) . ')'; 
							} else {
								echo ' (add: $' . number_format( $aValue['price'] , 2 ) . ')'; 	
							}
						}
						echo '</option>';
						}
					}
				?>
				</select>
			</div>
		</div>
		<?php
		} else {
		?>
		<div class="row">
			<div class="col-sm-6 text-right">			
				<strong>
					<?=$aAttribute['title'] ?>:
				</strong>
			</div>
			<div class="col-sm-6">			
			<?php
			foreach ( $aAttribute['options'] AS $cKey => $aValue ) {
				if ( $aValue['qty'] > 0 ) {
					echo '<input type="checkbox" name="' . 'attr_' . $nId . '[]" value="' . $aValue['optionid'] . '">' . stripslashes( $aValue['title'] ) . '<br />';
				}
			}
			?>
			</div>
		</div>			
		<?php
		}
	}
}  
?>
<br />
<div class="row">
	<div class="col-sm-12 text-right">			
		<input type="submit" class="button" value="Add To Order">
	</div>
</div>
</form>
