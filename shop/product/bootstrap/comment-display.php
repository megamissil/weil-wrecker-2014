<p>&nbsp;</p>
<a name="fcomments" id="fcomments"></a>
<h3 class="section-title">What other's are saying</h3>
<?php

	if ( $_GET['fcmt'] != NULL ) {
	
		echo '<div class="ferror">' . $_GET['fcmt'] . '</div>';
	
	}

	$aComments = productComments( $aProduct['productid'] );

	foreach ( $aComments AS $nComment => $aComment ) {

?>

<table width="100%" cellpadding="5" cellspacing="0" class="dashed">
	<tr>
		<td class="f12">
			<?=$aComment['subject'] ?>
			<p />
			<?=nl2br( $aComment['msg'] ) ?>
			<p />
			By: <?=html_link( '/members/' . strtolower( $aComment['createdby'] ) , $aComment['createdby'] ) ?>
			<?=$aComment['city'] . ', ' . $aComment['state'] ?> 
			<div class="float-right">
				<?=html_link( '/shop/report-abuse.php?&cn=' . $aComment['productid'] . '&cmt=' . $aComment['commentid'] , '[report abuse]' ); ?>
			</div>
			 on 
			<?=date('F j, Y, g:i a' , strtotime( $aComment['createdon'] ) );?>
		</td>
</table>

<?php } ?>