<?php
				
if ( $aOutput['ratings'] ) { 

	echo '<h4 class="section-title">Rate this Product</h4>';
	
	echo slurp( 'product-rating-display.php' , $aOutput );
	
	echo '<p />';
	
	if ( $aMember['memberid'] > 0 ) {
	
		if ( isset( $_COOKIE[FOONSTER_COOKIE_NAME.'_PRDRATING'] ) ) { 
	
			$aRating = explode( "," , $_COOKIE[FOONSTER_COOKIE_NAME.'_PRDRATING'] ); 

			if ( !in_array( $aOutput['productid'] , $aRating ) ) {
					
				echo slurp( 'product-rating-add.php' , $aOutput );
		
			}
		
		}   
		else {
		
			echo slurp( 'product-rating-add.php' , $aOutput );
		
		}

	}
	else {
	
		echo '<p />You will need to <a href="/members/logon.php">Log On</a>';
		echo ' or <a href="/members/join.php">Create an Account</a>';
		echo ' to rate this product.<p />';
	
	}
	
}
?>