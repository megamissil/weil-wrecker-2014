<?php

require_once 'connect.php';

$return = array('success' => 1, 'message' => '', 'field' => $_GET['field']);

if ($_GET['type'] == 'email') {
	if (!is_valid_email_address($_GET['value'])) {
		$return['success'] = 0;
		$return['message'] = 'Invalid email address';
	}
}

if ($_GET['type'] == 'cc_number') {
	if (!is_valid_email_address($_GET['value'])) {
		$return['success'] = 0;
		$return['message'] = 'Invalid email address';
	}
}

echo json_encode($return);
