
<div class="row">
	<div class="large-12 columns">
		<h2>
			<?php
			echo preg_match("/checkout/i", $_SERVER['PHP_SELF']) ? 'Your Order' : 'Your basket';
			?>
		</h2>
	</div>
</div>
<div class="row store">
	<div class="large-12 columns">
		<div class="panel">
			<?php
			if (sizeof( $aBasket['products'] ) > 0) {
				$nTabIndex = 0;
				foreach ( $aBasket['products'] AS $cKey => $aItem ) {
					$nTabIndex++;
		         	$nAdjusted = 0;
		         	$url = "/shop/$aItem[sku]~".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-", strtolower(trim($aItem['title'])))) . '.htm';
		         ?>

				<div class="row tyfoonRow tyfoonBottomBorder">
					<div class="large-2 columns">
					<?php
					if ( $aItem['thumb'] != NULL ) {
						echo html_link($url, '<img src="' . $aItem['thumb'] . '">');
			         } else {
						echo html_link($url, '<img src="/' . $rootDirectory . '/images/missing.gif">');
					}
					?>
					</div>
					<div class="large-4 columns">
					<?php
						echo html_link($url, stripslashes( $aItem['title'] ));
		         		#echo '<br/>item : '.stripslashes($aItem['sku']);
		         		echo !empty($aItem['attribute_text']) ? '<br/>'.stripslashes($aItem['attribute_text']) : false;
		         		echo !empty($aItem['note']) ? '<br/>'.stripslashes($aItem['note']) : false;
		         	?>
					</div>
					<div class="large-2 columns text-center">
						<?= number_format($aItem['qty'], 0) ?>
					</div>
					<div class="large-2 columns text-right">
						<?= _CURRENCY_SYMBOL ?>&nbsp;<?= number_format($aItem['price'], 2) ?>
						<div id="price_'<?= $aItem['itemid'] ?>"></div>
					</div>
					<div class="large-2 columns text-right" id="subtotal_'<?= $aItem['itemid'] ?>">
						<?= _CURRENCY_SYMBOL ?> <?= number_format((($aItem['qty']*$aItem['price']) + $aItem['setupfee']), 2) ?>
					</div>
				</div>
		         <?php
				}
			} else {
				echo '<div class="row"><div class="large-12 columns">There are no items configured for your cart.</div></div>';
			}
			?>
		</div>
	</div>
</div>
<?php
	if (preg_match("/checkout/i", $_SERVER['PHP_SELF'])) {
?>
<div class="row tyfoonRow">
	<div class="large-10 columns text-right">
		sub-total:
	</div>
	<div class="large-2 columns text-right">
	<?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['cart_total'], 2 ) ?>
	</div>
</div>
<div class="row tyfoonRow">
	<div class="large-10 columns text-right">
		Shipping <?php if($aBasket['ship_carrier']) { echo '('.$aBasket['ship_carrier'].')'; } ?>:
	</div>
	<div class="large-2 columns text-right">
		<?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['ship_charge'], 2 ) ?>
	</div>
</div>
<div class="row tyfoonRow">
	<div class="large-10 columns text-right">
		Taxes:
	</div>
	<div class="large-2 columns text-right">
		<?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['taxes'], 2 ) ?>
	</div>
</div>
<div class="row tyfoonRow">
	<div class="large-10 columns text-right">
		Total
	</div>
	<div class="large-2 columns text-right">
		<?= _CURRENCY_SYMBOL ?> <?=number_format(($aBasket['ship_charge'] + $aBasket['cart_total'] + $aBasket['taxes'] + $aBasket['cart_handling']), 2 ) ?>
	</div>
</div>
<?php
	}
?>
