<?php
require_once '../connect.php';	
if ( $_REQUEST['cn'] > 0 ) {
	$aProduct = productGet($_REQUEST['cn']);				
	if ( sizeof( $aProduct['attributes'] ) > 0 ) {
		foreach ( $aProduct['attributes'] AS $nKey => $aValue ) {
			if ( $aValue['type'] == 'RQRD' && $_POST['attr_'.$nKey] == NULL ) {
				$_SESSION['FERROR'] = 'Missing ' . $aProduct['attributes'][$nKey]['title'];
				$_SESSION['FERROR_PROD'] = $_POST['cn'];
				redirect( $_SERVER['HTTP_REFERER'] );					
				exit;
			}
		}
	}

	if (!ordersAddItem($_REQUEST)) {
		$_SESSION['FERROR'] = 'We were not able to add the product to your order.';
		$_SESSION['FERROR_PROD'] = $_POST['cn'];
		redirect( $_SERVER['HTTP_REFERER'] );					
		exit;
	}
	redirect( '/' . $webroot . '/basket');
} else {
	redirect( $_SERVER['HTTP_REFERER'] );
}
