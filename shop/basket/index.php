<?php
    require_once '../connect.php';

	$aBasket = ordersCartContents();



	include '../_header.php';
?>
<h1>
    <span class="headline-icon"><img src="/img/tow-icon.png" alt="towing icon" /></span>
    Your Basket
</h1>
<div class="row" id="errorMessage">
  <div class="col-sm-12 errorMessage">
    <?php echo !empty($errorMessage) ? $errorMessage : false; ?>
  </div>
</div>
<br/>
<div class="row">
  <div class="col-sm-12">
        <div class="panel">
          <?php
            if (sizeof( $aBasket['products'] ) > 0) {
            $nTabIndex = 0;
            ?>
            <form action="<?php if ( FOONSTER_ORDERS_SECURE == 1 ) { echo 'https://' . $_SERVER['HTTP_HOST'] . '/' . $webroot . '/checkout/index.php'; } else { echo 'http://' . $_SERVER['HTTP_HOST'] . '/' . $webroot . '/checkout/index.php'; } ?>" method="POST" name="tyfoonCart" id="tyfoonCart">
            <input type="hidden" name="step" value="gotocheckout">
            <?php
            foreach ( $aBasket['products'] AS $cKey => $aItem ) {
              $nTabIndex++;
                  $nAdjusted = 0;
                  $url = "/shop/$aItem[sku]~".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-", strtolower(trim($aItem['title'])))) . '.htm';
                 ?>

            <div class="row">
              <div class="col-md-2 col-sm-12">
                  <?php
                      if ( $aItem['thumb'] != NULL ) {
                        echo html_link($url, '<img src="' . $aItem['thumb'] . '">');
                           } else {
                        echo html_link($url, '<img src="/' . $webroot . '/images/missing.gif">');
                      }
                  ?>
              </div>
              <div class="col-md-4 col-sm-12">
              <?php
                echo html_link($url, stripslashes( $aItem['title'] ));
                    #echo '<br/>item : '.stripslashes($aItem['sku']);
                    echo !empty($aItem['attribute_text']) ? '<br/>'.stripslashes($aItem['attribute_text']) : false;
                    echo !empty($aItem['note']) ? '<br/>'.stripslashes($aItem['note']) : false;
                  ?>
              </div>
                    <div class="col-md-4 text-center col-sm-6">
                        <div class="row collapse">
                            <div class="col-lg-4 col-md-8">
                                <input type="text" class="text-center maskWholeNumber" name="qty_<?= $aItem['itemid'] ?>" value="<?= number_format($aItem['qty'], 0) ?>" maxlength="4">
                            </div>
                            <div class="col-md-4 col-lg-pull-4">
                                <span class="postfix"><?= html_link('remove.php?cn=' . $aItem[itemid], '&times;', 'delitem');?></span>
                            </div>
                        </div>
                    </div>
              <div class="col-md-2 text-right hide-for-small">
                <?= _CURRENCY_SYMBOL ?>&nbsp;<?= number_format($aItem['price'], 2) ?>
                <div id="price_'<?= $aItem['itemid'] ?>"></div>
              </div>
              <div class="col-md-2 col-sm-6 text-right show-for-small" id="subtotal_'<?= $aItem['itemid'] ?>">
                <?= _CURRENCY_SYMBOL ?> <?= number_format((($aItem['qty']*$aItem['price']) + $aItem['setupfee']), 2) ?>
              </div>
            </div>
                 <?php
                 }
                 ?>
                  <div class="row">
                    <div class="col-lg-10 text-right">
                      sub-total:
                    </div>
                    <div class="col-lg-2 text-right">
                    <?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['cart_total'], 2 ) ?>
                    </div>
                  </div>
                 </form>

                 <?php
          } else {
            echo '<div class="row"><div class="col-sm-12">There are no items configured for your cart.</div></div>';
          }
          ?>
        </div><!-- /.panel -->
  </div>        
</div>        
    
<?php

	include '../_footer.php';
