<?php

	require_once '../tyfoon/connect.php';
	
	$aOrder = ordersCartContents();
						
	if ( PP_SetExpressCheckout( $aOrder , $cError ) ) {
	
		PP_SendPayPalToken( $cError ); // this function does a REDIRECT TO PAYPAL	
			
	}
	else {
	
		redirect( $_SERVER['HTTP_REFERER'] . '?error=' . urlencode( $cError ) );
	
	}