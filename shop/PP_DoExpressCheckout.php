<?php

	require_once( '../tyfoon/connect.php' );
	
	include( FOONSTER_PATH . '/site/configs/orders-configuration.php' );
	
	$aReturn = PP_DoExpressCheckoutPayment( $_REQUEST['token'] , $_REQUEST['PayerID'] );
	
	if ( $aReturn['ACK'] == 'Success' ) 
	{
		
		$aReturn['cc_auth_code'] = $aReturn['PAYMENTINFO_0_TRANSACTIONID'];

		$aReturn['PP_CORRELATIONID'] = $aReturn['CORRELATIONID'];
		
		$aReturn['PP_TRANSACTIONID'] = $aReturn['PAYMENTINFO_0_TRANSACTIONID'];
		
		$aReturn['payment_gateway'] = 'PayPal';
		
		$aTransaction = PP_GetExpressCheckoutDetails( $_GET['token'] );
		
		foreach ( $aTransaction AS $cKey => $cValue ) 
		{
		
			$aTransaction[$cKey] = urldecode( $cValue );
		
		}	
				
		$aReturn['bill_name_first'] = $aTransaction['FIRSTNAME'];

		$aReturn['bill_name_last'] = $aTransaction['LASTNAME'];
		
		list( $aReturn['ship_name_first'] , $aReturn['ship_name_last'] ) = preg_split( "/ /" , $aTransaction['PAYMENTREQUEST_0_SHIPTONAME'] );

		$aReturn['ship_address_1'] = $aTransaction['PAYMENTREQUEST_0_SHIPTOSTREET'];

		$aReturn['ship_address_city'] = $aTransaction['PAYMENTREQUEST_0_SHIPTOCITY'];

		$aReturn['ship_address_state'] = $aTransaction['PAYMENTREQUEST_0_SHIPTOSTATE'];

		$aReturn['ship_address_postal_code'] = $aTransaction['PAYMENTREQUEST_0_SHIPTOZIP'];
		
		$aCountry = sql_query( "SELECT countryid FROM country WHERE iso_3166='$aTransaction[PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE]' LIMIT 1" );

		$aReturn['ship_address_country'] = $aCountry['countryid'];

		$aReturn['ship_email'] = $aTransaction['EMAIL'];
		
		// commented out due to issue with PayPal not returning valid information.
				
		/*$aReturn['amount'] = $aTransaction['AMT'];

		$aReturn['taxes'] = $aTransaction['TAXAMT'];

		$aReturn['handle_charge'] = $aTransaction['HANDLINGAMT'];

		$aReturn['ship_charge'] = $aTransaction['SHIPPINGAMT'];
		*/
		
		$aPost = ordersPostTransaction( $aReturn );
				
		$aReciept = ordersReceipt( $aPost['response'] );
		
		$cPageTitle = $aPost['response'] . ' : Order Complete';
		
		include '_header.php';
		
		?>
		
	<h2>
		<?=$cPageTitle ?>
	</h2>
						
		<p><?=$aReciept['customer']['html'] ?></p>
		
		<?php
				
		include '_footer.php';
	
	}
	else {
	
		$cPageTitle = 'Order Error';

		include '_header.php';
	
		echo $aReturn['L_SHORTMESSAGE0'];
	
		include '_footer.php';
	
	}