<?php 
include $_SERVER['DOCUMENT_ROOT'] . '/header.php' ;    
?>
<style>
.content-box h1 a,
.content-box h1 a:active,
.content-box h1 a:link,
.content-box h1 a:visited,
.content-box h1 a:hover {
    color: #232323;
}
.panel { 
    border: none;
}
.panel-footer { 
    color: black;
    background: none;
}

.panel-footer a { 
    color: black;
    background: none;
    text-transform: uppercase;
}

</style>
<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="/img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    