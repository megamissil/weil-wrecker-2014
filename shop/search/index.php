<?php

    require_once( 'functions.php' );
                
    // use smartphone shopping cart.
    
	if ( is_mobile_device() ) {
    
		include( '/shop/mobile/search.php' );
    	
    	exit;
    
    }    

	if ( $aCategory['title'] != NULL ) {
		
		$cCrumbTrail .= ' : ' . html_link( '/shop' , 'Shopping' );
					
	}
									
	$cHeadLine = 'Seach Results for ' . $_GET['terms'];
			
	// start rendering page.
    
    include( $_SERVER['DOCUMENT_ROOT'] . '/shop/_header.php' ); 
    
    // add categories at this level.
	
	
	
	include( $_SERVER['DOCUMENT_ROOT'] . '/shop/_footer.php' ); 
	
?>	
