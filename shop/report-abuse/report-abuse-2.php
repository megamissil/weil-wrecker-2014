<?php    
  
  	$cCrumbTrail .= ' : ' . html_link( '/shop' , 'Shopping' );
  			
	$cHeadLine = 'Report Abuse';

	include( $_SERVER['DOCUMENT_ROOT'] .'/shop/_header.php' );
	
	if ( $_POST['error'] != NULL ) {
	
		echo '<div class="ferror">' . $_GET['error'] . '</div>';
	
	}
	
	?>

<style type="text/css">

	#cAbuse {
	  display: block;
	  width: 175;
	  height: 35;
	  background: url( "/_images/submit-abuse-report.png" ) no-repeat 0 0;
	}

	#cAbuse:hover { 
	  background-position: 0 -35px;
	}

	#cAbuse span
	{
	  display: none;
	}
	
</style>

<table width="100%" cellpadding="5" cellspacing="0">

	<tr>
		<td class="flabel align-right" width="5%">
			&nbsp;
		</td>
		<td width="95%" class="fcontent">
		
			Your abuse report has been submitted to our 
			<p />

			<b>Offensive Comment:</b><p />
			
			<?=$aComment['subject'] ?>
			<p />
			<?=nl2br( $aComment['msg'] ) ?>
			<p />
			By: <?=html_link( '/members/' . strtolower( $aComment['createdby'] ) , $aComment['createdby'] ) ?>
			<?=$aComment['city'] . ', ' . $aComment['state'] ?> on 
			<?=date('F j, Y, g:i a' , strtotime( $aComment['createdon'] ) );?>

		</td>
	</tr>

	<tr>
		<td class="flabel align-right">
			Name:
		</td>
		<td>
			<input name="name" id="name" type="text" class="finput full" value="<?=$_POST['name'] ?>">
		</td>
	</tr>

	<tr>
		<td class="flabel align-right">
			Email:
		</td>
		<td>
			<input name="email" id="email" type="text" class="finput full" value="<?=$_POST['email'] ?>">
		</td>
	</tr>

	<tr>
		<td class="flabel align-right">
			Subject:
		</td>
		<td>
			<input name="subject" id="subject" type="text" class="finput full" value="<?=$_POST['subject'] ?>">
		</td>
	</tr>

	<tr valign="top">
		<td class="flabel align-right">
			Your&nbsp;Issue:
		</td>
		<td>
			<textarea name="msg" id="msg" class="finput" style=" width: 100%;" rows="8"><?=$_POST['msg'] ?></textarea>
		</td>
	</tr>

	<tr valign="top">
		<td>
			&nbsp;
		</td>
		<td>
		<?php
				
			if ( PG_COMMENT_REQUIRE_CAPTCHA ) {
			
				echo recaptcha_get_html($publickey, $reError);			
			
			}

			?>
			<p />
			<a id="cAbuse" href="javascript:;" onclick="document.fcomment.submit();" title="Submit Abuse Report"><span>Submit Abuse Report</span></a>
			<input type="image" name="sbtbtn" src="/_images/widget.gif" width="1" height="1" /> 
		</td>
	</tr>
</table>

</form>

<?php	
	
	include( $_SERVER['DOCUMENT_ROOT'] .'/_footer.php' );
	
?>	