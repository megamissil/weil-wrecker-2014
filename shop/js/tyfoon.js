 $(document).ready( function() {

    $('html,body').on('submit', '.submitForm', function() {            
        $('#' + $(this).data("form")).submit();
    });    
     
    $('[required]').on('blur', function() {        
        if ($(this).val().length === 0) 
        {
            $(this).addClass('error').attr('placeholder', $(this).data("errormsg") );            
            $('label[for="' + $(this).attr('id') + '"]').addClass('error'); 
        }
        else {
            $('label[for="' + $(this).attr('id') + '"]').removeClass('error');
            $(this).removeClass('error');            
        }
    });

    // -------------------------------
                        
    $('.maskFloatNumber').ForceNumericOnly();
    
    $('.maskWholeNumber').ForceWholeNumber();

    $('html,body').on('blur', '.isValidEmail', function() {        
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(!emailReg.test($(this).val())) {    
            $(this).val('');
            $(this).addClass('red').attr('placeholder', $(this).data('You entered an invalid email address.') );
            $('label[for="' + $(this).attr('id') + '"]').addClass('error'); 
        }
    }); 

    $('html,body').on('click', '.href', function() {        
        //if ($(this).data('target') == 'parent' || $(this).data('target') == '_blank') {
            window.location.href = $(this).data('href');
        //} else {        
        //    var target = '#' + $(this).data('target');
        //    $.get($(this).data('href'), {k: '1'},
        //        function(data) {                    
        //            $(target).hide().html(data).fadeIn('slow');
        //        }            
        //    );         
        //}
    });

    // logic to hide error state
    // 
    // - this will hide if nothing loaded.
    //
    if (!$('#tyfoonError').val()) {
        $('#tyfoonError').hide();
    }
    
});

jQuery.fn.convertToUrlSafe =
function convertToUrlSafe(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

jQuery.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 188 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

jQuery.fn.ForceWholeNumber =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 188 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||                
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};