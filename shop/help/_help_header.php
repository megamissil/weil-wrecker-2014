<html>
	<head>
		<title><?php  if ( $cPageTitle != NULL ) { echo $cPageTitle.' : '; } ?><?=FOONSTER_SITE_TITLE ?></title>
		<meta name="author" content="">
		<meta name="copyright" content="<?=date('Y') ?>">
		<meta name="description" content="<?=$aOutput['meta_description'] ?>">
		<meta name="keywords" content="<?=$aOutput['meta_keywords'] ?>">
		<meta name="robots" content="index,follow">
	</head>
	<body>

	<style type="text/css">
		.right { text-align: right; }
		.error { color: red; font-size: 14px; white-space: nowrap; }
		.fimage{ border: 1px solid black; }
		.flabel { color: #2D5788; font-size: 14px; }
		.fcart { border-bottom: 1px solid #2D5788; }
		.fcartrow { border-bottom: 1px solid #2D5788; background: white; }
		.fcartnav { color: #2D5788; }		
		* a:active  { color: #2D5788; text-decoration: none; }
		* a:link    { color: #2D5788; text-decoration: none; }
		* a:visited { color: #2D5788; text-decoration: none; }
		* a:hover   { color: #ff9900; text-decoration: none; }
		.fcartheader { background: #2D5788; color: white; }
		.fcartheader a:active  { color: white; text-decoration: none; }
		.fcartheader a:link    { color: white; text-decoration: none; }
		.fcartheader a:visited { color: white; text-decoration: none; }
		.fcartheader a:hover   { color: #ff9900; text-decoration: none; }
		.fcontent { padding: 5px; font-size: 14px; }
		.fcontent a:active  { color: #2D5788; }
		.fcontent a:link    { color: #2D5788; }
		.fcontent a:visited { color: #2D5788; }
		.fcontent a:hover   { color: #950015; text-decoration: underline; }
		.fqty { border: 1px solid #2D5788; font-size: 14px; background: #e3e3e3; text-align: center; padding: 2px;}
		.finput { border: 1px solid #2D5788; font-size: 14px; background: #e3e3e3; padding: 2px; }
		.finput_hidden { border: 0px solid white; text-align: right; font-size: 14px; }
		
		.sidenav { border: 1px solid #2D5788; }
		
		.sidenav th { background: #2D5788; color: white; } 
		
		.ftable { border: 1px solid #2D5788; }
				
		tr.fcartrow td { border-top: 1px solid #2D5788; }
		
		.submitbutton { border: 2px solid #2D5788; background: #2D5788; color: white; padding: 5px;  }

		.submitbutton_over { border: 2px solid #2D5788; background: #ff9900; color: #2D5788; padding: 5px;  }
		
	</style>