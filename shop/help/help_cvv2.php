<p>The Card Security Code (CSC), sometimes called Card Verification Value (CVV or CV2), Card 
Verification Value Code (CVVC), Card Verification Code (CVC), Verification Code (V-Code or 
V Code), or Card Code Verification (CCV)[1] is a security feature for credit or debit card 
transactions, giving increased protection against credit card fraud.
</p>

<p>The CVV2 or CVC2, is asked for by merchants for them to secure "card not present" 
transactions occurring over the Internet, by mail, fax or over the phone. In many 
countries in Western Europe, due to increased attempts at card fraud, it is now 
mandatory to provide this code when the cardholder is not present in person.
</p>

<p>For Discover, MasterCard and Visa, enter the last three digits on the signature
strip. For American Express, enter the four digits in small print on the front of the card.
</p>

<h4>More Information</h4>

<p>The CVV2 or CVC2 should not be confused with the standard card account number appearing in 
embossed or printed digits. (The standard card number undergoes a separate validation 
algorithm called the Luhn algorithm which serves to determine whether a given card's 
number is appropriate.)</p>

<p>The CVV2 or CVC2 should not be confused with a card's PIN value or PINs associated with 
MasterCard SecureCode or Visa's Verified by Visa. These codes are not printed or 
embedded in the card but are manually entered at the time of transaction.</p>