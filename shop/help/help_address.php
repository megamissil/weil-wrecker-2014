<p>1. Your billing address must match the address that appears on your monthly credit card or bank statement. Be sure to check your statement for accuracy.
If you have recently moved, you may need to contact your credit card company to ensure you are using the
correct information.
</p>

<p>2. Leave fields blank if they don't apply to your shipping or billing address. Do not enter "N/A," "Not applicable," "None," "Same," etc.</p>

<p>3. Enter information in the appropriate fields. For example, do not enter city and state information in the "Address Line 2" field; use the provided city and state fields instead.</p>

<p>4. Avoid using accent marks or non-English characters. Our systems cannot process Chinese characters, Cyrillic letters, etc., and entering these into any of the address fields may delay fulfillment of your order.</p>

<p>5. Do not enter shipping or delivery instructions in any of the address fields. In order to ship your items in a timely fashion.dd</p>	
