<?php 

$cPageTitle = 'Help';

include( '_help_header.php' ); 

?>

Shopping Basket<p/>

When you are shopping and add an item, it is saved in your Shopping
Basket. The Shopping Basket holds products you wish to purchase while
you shop, the same way you use a shopping basket in a retail store.
Items you place in your Shopping Basket will remain there until they are
purchased, removed, or moved to your Wish List.<p/>

Options in the Shopping Basket: 

<ul>

Remove an item from your Shopping Basket. To remove an
item, click on "Remove from Basket," or change the quantity to zero and
click "Update Basket."<p/>

Return to shopping. If you wish to continue shopping on
, you can use the Shopping Basket to store items you
wish to purchase. Click on "Return to Shopping" to search the site for
additional items. At any time during your shopping experience, you can
return to your Shopping Basket by clicking on "Shopping Basket."<p/>

Proceed to checkout. When you are ready to purchase your
item(s), click on "Checkout Now." Our checkout process is fast, easy and
secure. For more information on our secure shopping guarantee.<p/>

Move an item to your Wish List. If there are items in your
Shopping Basket that you want to save but do not wish to purchase
immediately, you can move them to your Wish List by clicking on "Move to
Wish List."<p/>

</ul>

<?php include( '_help_header.php' ); ?>
