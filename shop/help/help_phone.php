<b>Billing Address Phone Number</b>
<ul>
	Your billing address phone number must match the telephone number on your credit card account.
</ul>
<b>Shipping Address Phone Number</b>
<ul>
	The phone number at the shipping address is important in the event the order courier needs to arrange a delivery time with you.
</ul>