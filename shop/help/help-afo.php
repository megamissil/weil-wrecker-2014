<p>Using An APO/FPO Address</p>
<p>We are pleased to support the Billing and Shipping of your order to an Armed Forces APO "Army Post Office" or FPO
"Fleet Post Office"
address. To take advantage of this service, please enter either APO or FPO in the 
&quot;City&quot; field. All APO and FPO orders will be shipped via the U.S. Postal 
Service. &quot;Express&quot; and &quot;Next-Day&quot; service is not available.
</p>

<p><b>Go Navy , Beat Army</b></p>

	