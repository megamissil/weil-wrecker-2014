<?php
  include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
  $aPage = pageGet( 74 );
    $cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'FAQs';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php include('header.php'); ?>

<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    <h1><span class="headline-icon"><img src="img/faq-icon.png" alt="towing icon" /></span>Frequently Asked Questions</h1>
                    <div class="row">
                        <!-- START SUB CONTENT -->
                        <div class="col-md-12">
                          <?php echo $aPage['msg']; ?>
                        </div>
                    </div><!-- /.row -->
                </div>
                <div class="content-box-append hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-md-7 col-lg-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-md-5 col-lg-4">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append-touch visible-xs"><!--MOBILE SURVEY SECTION -->
                    <a href="survey.php">
                        <h2>Happy with your experience?</h2>
                    </a>
                </div>
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include('sidebar-sub.php'); ?>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include('footer.php'); ?>
