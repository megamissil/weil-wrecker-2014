<?php
	include('postman/_variables.php');
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet( 68 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Contact Us';
	$cSEOTitle = '';
	$layout = 'sub';
?>

<?php
	include("header.php");
?>
<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    <h1><span class="headline-icon"><img src="img/contact-icon.png" alt="towing icon" /></span>Contact Us</h1>
                    	<div class="row">
	                    	<div class="col-md-7 col-sm-11 col-xs-11">
                            <p align="left" style="color: #FF0000">Please do not use this page to request a service or a tow. To get immediate assistance, please call 205.261.4060.</p>
			                	<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
			                	<div class="row">
			                		<div class="col-md-12">
					                	<div class="form-group">
					                		<label for="service-type">Type of Service</label>
						                      	<select name="service-type" class="form-control">
						                          	<option value="Towing">Towing</option>
						                          	<option value="Recovery">Recovery</option>
						                          	<option value="Auction">Auction</option>
						                          	<option value="Legal">Legal</option>
						                          	<option value="Storage">Storage</option>
						                          	<option value="Equipment Sales">Equipment Sales</option>
						                          	<option value="Customer Service">Customer Service</option>
						                          	<option value="Other">Other</option>
						                      	</select>
					                    </div>
					                </div>
			                	</div>
			                	<div class="row">
			            			<div class="col-md-12">
								  		<div class="form-group">
								    		<label for="name">Name:</label>
								    		<input type="text" class="form-control" name="name" value="<?=$_POST['name'] ?>"/>
								  		</div>
								  	</div>
								 </div>
			                	<div class="row">
			            			<div class="col-md-12">
							  			<div class="form-group">
							   				<label for="email">Email:</label>
							   				<input type="text" class="form-control" name="email" value="<?=$_POST['email'] ?>"/>
							  			</div>
							  		</div>
								 </div>
								 <div class="row">
			            			<div class="col-md-12">
								  		<div class="form-group">
								    		<label for="phone">Phone:</label>
								    		<input type="text" class="form-control" name="phone" value="<?=$_POST['phone'] ?>"/>
								  		</div>
								  	</div>
								  </div>
								  <div class="row">
			            			<div class="col-md-12">
								  		<div class="form-group">
								    		<label for="msg">Message:</label> 
								    		<textarea id="msg" class="form-control" value="<?=$_POST['msg'] ?>" name="msg"></textarea>
								  		</div>
								  	</div>
								  </div>
								  <div class="row">
								  	<div class="col-md-6 col-sm-6 col-xs-5">
								  		<button type="submit" name="sbmtbtn" id="sbmtbtn" class="button radius form-btn">Submit</button>
								  	</div>
								  	<div class="col-md-6 col-sm-6 col-xs-7">
								  		<a class="about-btn radius" href="about.php">About Us</a>
								  	</div>
								  </div>
								</form>
								<br />
								
							</div>
							<div class="col-md-5 hidden-sm hidden-xs contact-img">
		                        <?=$aPage['msg']; ?>
		                    </div>
		                </div>
			  </div>
				<div class="content-box-append hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-lg-8 col-md-7">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-lg-4 col-md-5">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append-touch visible-xs"><!--MOBILE SURVEY SECTION -->
                    <a href="survey.php">
                        <h2>Happy with your experience?</h2>
                    </a>
                </div>
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include('sidebar-sub.php'); ?>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include('footer.php'); ?>