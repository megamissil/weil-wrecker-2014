
<div class="row">
	<div class="col-md-12">
		<h2>
			<?php 
			echo preg_match("/checkout/i", $_SERVER['PHP_SELF']) ? 'Your Order' : 'Your basket';
			?>
		</h2>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
	<?php		
	if (sizeof( $aBasket['products'] ) > 0) { 
		$nTabIndex = 0;
		foreach ( $aBasket['products'] AS $cKey => $aItem ) {
			$nTabIndex++;
         	$nAdjusted = 0;
         	$url = "/parts/$aItem[sku]~".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-", strtolower(trim($aItem['title'])))) . '.htm';
         ?>

		<div class="row tyfoonRow tyfoonBottomBorder">
			<div class="col-md-2">
			<?php
			if ( $aItem['thumb'] != NULL ) {
				echo html_link($url, '<img src="' . $aItem['thumb'] . '">');            
	         } else {
				echo html_link($url, '<img src="/' . $rootDirectory . '/images/missing.gif">');
			}   
			?>
			</div>
			<div class="col-md-4">
			<?php 
				echo html_link($url, stripslashes( $aItem['title'] ));    
         		echo '<br/>item : '.stripslashes($aItem['sku']);
         		echo !empty($aItem['attribute_text']) ? '<br/>'.stripslashes($aItem['attribute_text']) : false;
         		echo !empty($aItem['note']) ? '<br/>'.stripslashes($aItem['note']) : false;
         	?>         
			</div>
			<div class="col-md-2 text-center">
				<?= number_format($aItem['qty'], 0) ?>				
			</div>
			<div class="col-md-2 text-right">
				<?= _CURRENCY_SYMBOL ?>&nbsp;<?= number_format($aItem['price'], 2) ?>
				<div id="price_'<?= $aItem['itemid'] ?>"></div>
			</div>
			<div class="col-md-2 text-right" id="subtotal_'<?= $aItem['itemid'] ?>">
				<?= _CURRENCY_SYMBOL ?> <?= number_format((($aItem['qty']*$aItem['price']) + $aItem['setupfee']), 2) ?>
			</div>
		</div>
         <?php
		}
	} else {
		echo '<div class="row"><div class="col-md-12">There are no items configured for your cart.</div></div>';
	}
	?>
	</div>
</div>
<?php 
	if (preg_match("/checkout/i", $_SERVER['PHP_SELF'])) {
?>
<div class="row tyfoonRow">
	<div class="col-md-10 text-right">
		sub-total:
	</div>
	<div class="col-md-2 text-right">
	<?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['cart_total'], 2 ) ?>
	</div>
</div>
<div class="row tyfoonRow">
	<div class="col-md-10 text-right">
		Shipping (<?= $aBasket['ship_carrier'] ?>):
	</div>
	<div class="col-md-2 text-right">
		<?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['ship_charge'], 2 ) ?>
	</div>
</div>
<div class="row tyfoonRow">
	<div class="col-md-10 text-right">
		Handling:
	</div>
	<div class="col-md-2 text-right">
		<?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['cart_handling'], 2 ) ?>
	</div>
</div>
<div class="row tyfoonRow">
	<div class="col-md-10 text-right">
		Taxes:
	</div>
	<div class="col-md-2 text-right">
		<?= _CURRENCY_SYMBOL ?> <?=number_format($aBasket['taxes'], 2 ) ?>
	</div>
</div>
<div class="row tyfoonRow">
	<div class="col-md-10 text-right">
		Total
	</div>
	<div class="col-md-2 text-right">
		<?= _CURRENCY_SYMBOL ?> <?=number_format(($aBasket['ship_charge'] + $aBasket['cart_total'] + $aBasket['taxes'] + $aBasket['cart_handling']), 2 ) ?>
	</div>
</div>
<?php
	}
?>


