<?php


	require_once '../connect.php';

	echo '<pre>';
	print_r($_POST);
	echo '</pre>';
	
	if ( $_REQUEST['cn'] > 0 ) {

		session_start();

		$product = (object) productGet($_REQUEST['cn']);				

		$primaryOption = array_shift($product->attributes);

		
		// this is a simple look-up and the other options are controlled
		// by this script.

		foreach ($_POST as $k => $field) {
			if (substr($k, 0, 4) == 'qty_') {
				
				if ($field >= 1) { // the '0' test was failiing.
					$option1 = substr($k, 4);
					$attribute1 = $primaryOption['options'][$option1]['attributeid'];
					$temp = array(
						'cn' => $product->productid,
						'qty' => $field,
						'attr_' . $attribute1 => $option1,
						);
					echo "<pre>";
					print_r($temp);
					echo "</pre>";			

					if (!ordersAddItem($temp)) {
						echo 'something broke';
						$_SESSION['FERROR'] = 'We were not able to add the product to your order.';
						$_SESSION['FERROR_PROD'] = $_REQUEST['cn'];
					}  

				}
			}
		}


		if (!empty($_SESSION['FERROR'])) {
			redirect( $_SERVER['HTTP_REFERER'] );
		} else {
			redirect( '/' . $rootDirectory . '/basket');
		}

	} else {
		redirect( '/' . $rootDirectory . '/basket');
	}
