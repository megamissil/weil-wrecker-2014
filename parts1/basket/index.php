<?php
	$cMetaKW = $aOutput['keywords'];
	$aOutput['title'] = 'View Your Basket';
	$cPageTitle = $aOutput['title'];
	$aOutput['crumbtrail'] = $aOutput['title'];
    require_once '../connect.php';
	include '../_header.php'; 
?> 
<div class="row" id="errorMessage">
	<div class="col-md-12 errorMessage">
		<?php echo !empty($errorMessage) ? $errorMessage : false; ?>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-12 columns">
	<?php

		if (sizeof( $aBasket['products'] ) > 0) { 
		$nTabIndex = 0;
		?>
		<form action="<?php if ( FOONSTER_ORDERS_SECURE == 1 ) { echo 'https://' . $_SERVER['HTTP_HOST'] . '/' . $rootDirectory . '/checkout/index.php'; } else { echo 'http://' . $_SERVER['HTTP_HOST'] . '/' . $rootDirectory . '/checkout/index.php'; } ?>" method="POST" name="tyfoonCart" id="tyfoonCart">
		<input type="hidden" name="step" value="gotocheckout">
		<?php
		foreach ( $aBasket['products'] AS $cKey => $aItem ) {
			$nTabIndex++;
         	$nAdjusted = 0;
         	$url = "/parts/$aItem[sku]~".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-", strtolower(trim($aItem['title'])))) . '.htm';
         ?>

		<div class="row tyfoonRow">
			<div class="col-md-12">
			<?php
			if ( $aItem['thumb'] != NULL ) {
				echo html_link($url, '<img src="' . $aItem['thumb'] . '">');            
	         } else {
				echo html_link($url, '<img src="/' . $rootDirectory . '/images/missing.gif">');
			}   
			?>
			</div>
			<div class="col-md-4">
			<?php 
				echo html_link($url, stripslashes( $aItem['title'] )); 
				$attribute_text = str_replace( array('<strong>', '</strong>'),'',$aItem['attribute_text']);
				$attribute_text = preg_replace('/\[(.)*\]/','',$attribute_text);
				$splitAttr = explode(':',$attribute_text );
				$attr_titles = explode('|',$splitAttr[0]);
				$attr_values = explode('|',$splitAttr[1]);
				$attr_text_new = '<div class="values">'.$attr_titles[0].': '.$attr_values[0].'</div>';
				$attr_text_new .= '<div class="values">'.$attr_titles[1].': '.$attr_values[1].'</div>';

         		echo !empty($aItem['attribute_text']) ? '<br/>'.$attr_text_new : false;
         	?>         
			</div>
			<div class="col-md-2 text-center">
				<input type="text" class="text-center maskWholeNumber" name="qty_<?= $aItem['itemid'] ?>" value="<?= number_format($aItem['qty'], 0) ?>" maxlength="4">
				<?= html_link('remove.php?cn=' . $aItem[itemid], 'remove', 'button tiny delitem');?>
			</div>
			<div class="col-md-2 text-right">
				<?= _CURRENCY_SYMBOL ?>&nbsp;<?= number_format($aItem['price'], 2) ?>
				<div id="price_'<?= $aItem['itemid'] ?>"></div>
			</div>
			<div class="col-md-2 text-right" id="subtotal_'<?= $aItem['itemid'] ?>">
				<?= _CURRENCY_SYMBOL ?> <?= number_format((($aItem['qty']*$aItem['price']) + $aItem['setupfee']), 2) ?>
			</div>
		</div>
        <?php
        } 
        ?>
        <hr>
	        <div class="row">
	        	<div class="col-md-8 text-right">Subtotal:</div>
	        	<div class="col-md-2 text-right">
			        <?php 
			        	echo '$'.number_format($aBasket['cart_total'],2);
			        ?>        		
	        	</div>
	        </div>
        <hr>
		<div class="row">
			<div class="col-md-12 columns">
				<?php strpos($_SERVER['HTTP_REFERER'], '/parts') >= 0 && strpos($_SERVER['http_referer'], '/parts/basket.php' == -1) ? $shop_url = $_SERVER['HTTP_REFERER'] : $shop_url = '/parts'; ?>
				<a href="<?=$shop_url; ?>" class="button left">continue shopping</a>        
	         <?php  
	         echo (!FOONSTER_ORDERS_DISABLE) ? '<input type="submit" value="proceed to checkout" class="button right">' : false;

	         ?>  
         	</div>
         </div>       
         </form>
         <?php
	} else {
		echo '<div class="row"><div class="col-md-12">There are no items configured for your cart.</div></div>';
	}
	?>
		<hr>
		<div class="row">
			<div class="col-md-6">
				<h3>
					Wholesale Customers
				</h3>
				<?php
				if ($member['memberid'] > 0) {
					echo 'You are currently logged in as: <br />';
					echo $member['name_first'].' '.$member['name_last'] . '<br />';
					echo 'Company: ' . $member['account']['title'].'<br/>';
					echo '<a href="/wholesale/logout.php">Logout</a>';
				} else {
				?>
				<p>If you have a wholesale account, please enter your username and password
						to experience a faster checkout and access your other membership options.</p>
				<br/>
				<form action="/wholesale/logon/index.php" method="POST" name="flogon">
					<input type="hidden" name="refer" value="<?=$_SERVER['REQUEST_URI']; ?>">

  					<div class="row">
    					<div class="col-md-12">      			
        					<input type="text" class="required" data-errorMessage="You are missing a username" placeholder="Username" name="handle" id="handle" />
    					</div>
  					</div>

  					<div class="row">
    					<div class="col-md-12">      			
        					<input type="password" class="required" data-errorMessage="You are missing a username" placeholder="Password" name="passwd" id="passwd" />
    					</div>
  					</div>

  					<div class="row">
    					<div class="col-md-12">      			
        					<input type="submit" value="Login" class="button center">
    					</div>
  					</div>
								
				</form>				
				<?php
				} 
				?>
			</div>
		</div>
	</div>
</div>

<?php 

	include '../_footer.php'; 
