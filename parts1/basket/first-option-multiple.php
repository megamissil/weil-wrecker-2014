<?php
	require_once '../connect.php';	
	
	if ( $_REQUEST['cn'] > 0 ) {



		$product = (object) productGet($_REQUEST['cn']);				

		$primaryOption = array_shift($product->attributes);

		$secondaryOption = array_shift($product->attributes);	
		
		// this is a simple look-up and the other options are controlled
		// by this script.

		foreach ($_POST as $k => $field) {
			if (substr($k, 0, 4) == 'qty_') {
				
				if ($field >= 1) { // the '0' test was failiing.
					list($option1, $option2) = preg_split('/-/', substr($k, 4));
					$attribute1 = $primaryOption['options'][$option1]['attributeid'];
					$attribute2 = $secondaryOption['options'][$option2]['attributeid'];
					$temp = array(
						'cn' => $product->productid,
						'qty' => $field,
						'attr_' . $attribute1 => $option1,
						'attr_' . $attribute2 => $option2
						);					

					if (!ordersAddItem($temp)) {
						$_SESSION['FERROR'] = 'We were not able to add the product to your order.';
						$_SESSION['FERROR_PROD'] = $_REQUEST['cn'];
					}
				}
			}
		}

		if (!empty($_SESSION['FERROR'])) {
			redirect( $_SERVER['HTTP_REFERER'] );
		} else {
			redirect( '/' . $rootDirectory . '/basket');
		}

	} else {
		redirect( '/' . $rootDirectory . '/basket');
	}
