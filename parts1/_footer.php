                        



                        </div>
                    </div><!-- /.row -->
                </div>
                <div class="content-box-append hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-md-7 col-lg-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-md-5 col-lg-4">
                            <a href="/survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append-touch visible-xs"><!--MOBILE SURVEY SECTION -->
                    <a href="/survey.php">
                        <h2>Happy with your experience?</h2>
                    </a>
                </div>
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include('../sidebar-sub.php'); ?>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="/survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php 
include $_SERVER['DOCUMENT_ROOT'] . '/footer.php';
