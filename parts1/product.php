<?php
include $_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php';  

$product = productGet($_GET['cn']);

$cMetaDesc = '';
$cMetaKW = '';
$cPageTitle = 'Parts';
$cSEOTitle = '';
$layout = 'home';

// -- MySQL commands

include '../header.php'; ?>

<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="/img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    <h1><span class="headline-icon"><img src="/img/tow-icon.png" alt="towing icon" /></span><?php echo $product['title']; ?>  <?php echo $product['price_retail']; ?></h1>
                    <div class="row">
                        <!-- START SUB CONTENT -->
                        <div class="col-md-7">
                            <a href="<?=$product['images'][1]['image']; ?>"><img src="<?=$product['images'][1]['image']; ?>" alt="<?=$product['title'] ?>"></a>                            
                        </div>
                        <div class="col-md-5">
                            <div class="info-container">                        
                                <div class="vehicle-info">
                                    <?php /* dumpVariable($product); */ ?>
                                    <p><span>Name:</span> <?php echo $product['required_title']; ?></p>
                                    <p><span>Part:</span> <?php echo $product['addl_part']; ?></p>
                                    <p><span>Size:</span> <?php echo $product['addl_size']; ?></p>
                                    <p><span>Working Load Limit:</span> <?php echo $product['addl_wll']; ?></p>
                                    <p><span>Minimum Break Strength:</span> <?php echo $product['addl_mbb']; ?></p> 
                                    <p><span>Length:</span> <?php echo $product['addl_length']; ?></p>
                                    <p><span>Width:</span> <?php echo $product['addl_width']; ?></p>
                                    <p><span>Height:</span> <?php echo $product['addl_height']; ?></p>
                                    <p><span>Color:</span> <?php echo $product['addl_color']; ?></p>
                                    <p><span>Vertical WLL:</span> <?php echo $product['addl_vertical']; ?></p>
                                    <p><span>Choker: WLL</span> <?php echo $product['addl_choker']; ?></p>
                                    <p><span>Basket WLL:</span> <?php echo $product['addl_basket']; ?></p>
                                    <p><span>Unit:</span> <?php echo $product['addl_unit']; ?></p>
                                    <p><span>Grade:</span> <?php echo $product['addl_addl_grade']; ?></p>
                                    <p><span>QTY on Hand:</span> <?php echo $product['qty']; ?></p>
                                    <p><span>Retail:</span> <?php echo $product['price_retail']; ?></p>
                                    
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div>
                <div class="content-box-append hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-md-7 col-lg-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-md-5 col-lg-4">
                            <a href="/survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append-touch visible-xs"><!--MOBILE SURVEY SECTION -->
                    <a href="/survey.php">
                        <h2>Happy with your experience?</h2>
                    </a>
                </div>               
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include '../sidebar-sub.php'; ?>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="/survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '../footer.php';
