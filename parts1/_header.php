<?php 
include $_SERVER['DOCUMENT_ROOT'] . '/header.php' ;    
?>
<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="/img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    <h1><span class="headline-icon"><img src="/img/tow-icon.png" alt="towing icon" /></span>
                       <?= str_replace('/', ' : ', strip_tags($category['path'])) ?>
                    </h1>
                    <div class="row">
                        <!-- START SUB CONTENT -->
                        <div class="col-md-12">
                        