<?php

	$mQuery  = mysql_query("SELECT countryid, title FROM country WHERE active='1' ORDER BY deft DESC,title ASC");
	$cJavascript ='';
	while ( $aCountry = mysql_fetch_assoc( $mQuery ) ) {
		$cJavascript .= 'assocArray["state_'.$aCountry['countryid'].'"] = new Array ("0","SELECT",';
		$mStatQry = mysql_query("SELECT * FROM states WHERE active='1' AND countryid='$aCountry[countryid]' ORDER BY title ASC");
		while ( $aStates = mysql_fetch_assoc( $mStatQry ) ) {
			$cJavascript .= "\"$aStates[stateid]\",\"".stripslashes($aStates['title']).'",';
		}
		$cJavascript = substr($cJavascript, 0, -1);
		$cJavascript .= ');'."\n";
	} 

	$cPageTitle = 'step 3 of 5 : payment information';

   	include '_header.php'; 
   
?>
<h1>
	<?=$cPageTitle ?>
</h1>
<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="foonster">
<input type="hidden" name="step" value="setbilling">
<input type="hidden" id="payment_method" name="payment_method" value="<?=$_POST['payment_method'] ?>">

<?php
		
	if ( !empty( $_POST['error'] ) ) {
	
		echo '<div class="ferror">' . $_POST['error'] . '</div>';	
				
	}

?>		
<input type="checkbox" value="1" name="sameas" onClick=" setBillTo( this.form );" style="display: inline;"> - same as the billing address

<table width="100%" cellpadding="5" cellspacing="0">
	<tr valign="top">
		<td width="33%">
			<table width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td class="<?=$aForm['bill_name_salutation']['style'] ?>" width="25%">
						<?=$aForm['bill_name_salutation']['label'] ?>
					</td>
					<td width="75%">	
						<input name="bill_name_salutation" type="text" value="<?=$_POST['bill_name_salutation'] ?>" tabindex="1" class="finput full"><div class="ferror"><?=$aForm['bill_name_salutation']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_name_first']['style'] ?>">
						<?=$aForm['bill_name_first']['label'] ?>
					</td>
					<td>
						<input name="bill_name_first" type="text" value="<?=$_POST['bill_name_first'] ?>" tabindex="2" class="finput full" placeholder="Your first name" autofocus required><div class="ferror"><?=$aForm['bill_name_first']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_name_last']['style'] ?>">
						<?=$aForm['bill_name_last']['label'] ?>
					</td>
					<td>
						<input name="bill_name_last" type="text" value="<?=$_POST['bill_name_last'] ?>" tabindex="3" class="finput full" placeholder="Your last name" required><div class="ferror"><?=$aForm['bill_name_last']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_company']['style'] ?>">
						<?=$aForm['bill_company']['label'] ?>
						</td>
					<td>
						<input name="bill_company" type="text" value="<?=$_POST['bill_company'] ?>" tabindex="4" class="finput full"><div class="ferror"><?=$aForm['bill_company']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_email']['style'] ?>">
						<?=$aForm['bill_email']['label'] ?>
						<div class="fcontent">
							<a href="help_email.php"  onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )" style="font-size: 10px;">(why required?)</a>
						</div>
					</td>
					<td>
						<input name="bill_email" type="text" value="<?=$_POST['bill_email'] ?>" class="finput full" tabindex="5" placeholder="billing email address" required><div class="ferror"><?=$aForm['bill_email']['msg'] ?></div>
					</td>
				</tr>
				<tr>
					<td class="<?=$aForm['referral']['style'] ?>">
						<?=$aForm['referral']['label'] ?>
					</td>
					<td>
						<input name="referral" type="text" value="<?=$_POST['referral'] ?>" tabindex="6" class="finput full"><div class="ferror"><?=$aForm['referral']['msg'] ?></div>
					</td>
				</tr>
								
				<tr>
					<td class="flabel align-right">
						Special Instructions:
					</td>
					<td>
						<textarea name="bill_msg" class="finput full" style="width: 100%;" rows="4" tabindex="7"><?=$_POST['bill_msg'] ?></textarea>
					</td>
				</tr>				
			</table>
		</td>
		<td width="33%">

			<table width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td class="<?=$aForm['bill_address_1']['style'] ?>">
						<?=$aForm['bill_address_1']['label'] ?>
					</td>
					<td>
						<input name="bill_address_1" type="text" value="<?=$_POST['bill_address_1'] ?>" tabindex="8" class="finput full" placeholder="billing address" required><div class="ferror"><?=$aForm['bill_address_1']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_address_2']['style'] ?>">
						<?=$aForm['bill_address_2']['label'] ?>
					</td>
					<td>
						<input name="bill_address_2" type="text" value="<?=$_POST['bill_address_2'] ?>" tabindex="9" class="finput full"><div class="ferror"><?=$aForm['bill_address_2']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_address_3']['style'] ?>">
						<?=$aForm['bill_address_3']['label'] ?>
					</td>
					<td>
						<input name="bill_address_3" type="text" value="<?=$_POST['bill_address_3'] ?>" tabindex="10" class="finput full"><div class="ferror"><?=$aForm['bill_address_3']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_address_city']['style'] ?>">
						<?=$aForm['bill_address_city']['label'] ?>
					</td>
					<td>
						<input name="bill_address_city" type="text" value="<?=$_POST['bill_address_city'] ?>" tabindex="11" class="finput full" placeholder="billing city" required><div class="ferror"><?=$aForm['bill_address_city']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_address_state']['style'] ?>">
						<?=$aForm['bill_address_state']['label'] ?>
					</td>
					<td>
						<select name="bill_address_stateid" class="finput full" tabindex="12" placeholder="select a state/prov">
						<option value="0">Select a State/Prov</option>
						<?php
						mysql_data_seek($mQuery, 0);
						$aCountry = mysql_fetch_assoc( $mQuery );
						$qState = mysql_query("SELECT * FROM states WHERE countryid='$aCountry[countryid]' ORDER BY deft DESC, title ASC");
						while ( $aState = mysql_fetch_assoc( $qState ) ) {
							echo "<option value=\"$aState[stateid]\"";
							if ( $aState['stateid'] == $_POST['bill_address_stateid'] ) { echo 'SELECTED'; }
							echo ">".cleanstring( $aState['title'] ).'</option>';
						}
						?>  
						</select>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_address_country']['style'] ?>">
						<?=$aForm['bill_address_country']['label'] ?>
					</td>
					<td>
						<select name="bill_address_country" tabindex="3" onChange="listboxItemSelected(this,this.form.bill_address_stateid,'state_');" class="finput full">
						<?php
						mysql_data_seek($mQuery, 0);
						while ( $aCountry = mysql_fetch_assoc( $mQuery ) ) {
							echo "<option value=\"$aCountry[countryid]\"";
							if ( $aCountry['countryid'] == $_POST['countryid'] ) { echo 'SELECTED'; }
							echo ">".cleanstring( $aCountry['title'] ).'</option>';
						}
						?>  
						</select>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_address_postal_code']['style'] ?>">
						<?=$aForm['bill_address_postal_code']['label'] ?>
					</td>
					<td>
						<input name="bill_address_postal_code" type="text" value="<?=$_POST['bill_address_postal_code'] ?>" class="finput full" tabindex="13" placeholder="your postal code" required><div class="ferror"><?=$aForm['bill_address_postal_code']['msg'] ?></div>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['bill_address_phone']['style'] ?>">
						<?=$aForm['bill_address_phone']['label'] ?>
						
						<br />
						<div class="fcontent">
							<a href="help_phone.php"  onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )" style="font-size: 10px;">(why required?)</a>
						</div>
					</td>
					<td>
						<input name="bill_address_phone" type="text" value="<?=$_POST['bill_address_phone'] ?>" tabindex="14" class="finput full" placeholder="the number we should call" required><div class="ferror"><?=$aForm['bill_address_phone']['msg'] ?></div>
					</td>
				</tr>
			</table>						
		</td>
		<td width="34%">
		<?php

			if ( PAYPAL_EXPRESSCHECKOUT)
			{		
				echo '<center><a href="PP_ExpressCheckout.php" border="0"><img src="./_images/btn_xpressCheckout.gif"></a></center>';		
			}					

			
			if ( PAYMENT_CC ) 
			{
					
			?>
			<table width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td class="<?=$aForm['cc_name_on_card']['style'] ?> align-right"><?=$aForm['cc_name_on_card']['label'] ?></td>
					<td colspan="2"><input name="cc_name_on_card" type="text" value="<?=$_POST['cc_name_on_card'] ?>" tabindex="15" class="finput full" placeholder="Name on Card"></td>
				</tr>

				<tr>
					<td class="<?=$aForm['cc_type']['style'] ?> align-right"><?=$aForm['cc_type']['label'] ?></td>
					<td colspan="2">
						<select name="cc_type" class="finput full" tabindex="16">
						<option value="">SELECT</option>
						<?php 
						foreach ( $aCreditCards AS $cValue ) {
							echo '<option value="'.$cValue.'"';
							if ( $_POST['cc_type'] == $cValue ) { echo 'SELECTED'; }
							echo '>' . $cValue . '</option>';
						}
						?>
						</select>
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['cc_number']['style'] ?> align-right">
						<?=$aForm['cc_number']['label'] ?>
					</td>
					<td colspan="2">
						<input name="cc_number" type="text" value="<?=$_POST['cc_number'] ?>" tabindex="17" class="finput full" onKeyPress="return wholeNumber(this, event);">
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['cc_exp_month']['style'] ?> align-right">
						<?=$aForm['cc_exp_month']['label'] ?>
					</td>
					<td valigm="top"><select name="cc_exp_month" tabindex="18">
						<?php
   
						foreach ( $aMonths AS $cKey => $cValue ) {
		
							echo '<option value="' . $cKey . '"';
							echo ( $_POST['cc_exp_month'] == $cKey ) ? ' SELECTED' : false;
							echo '>' . $cValue . '</option>';
						}
	
						?>
						</select>
					</td>
					<td valign="top"><select name="cc_exp_year" tabindex="19">
						<?php
   
						if ( $_POST['cc_exp_year'] == NULL || !is_numeric( $_POST['cc_exp_year'] ) ) { $_POST['cc_exp_year'] = date( 'Y' ); }
   
							for ( $i = (date("Y")); $i <= date("Y")+8; $i++ ) {
   	
								echo '<option value ="'.$i.'"';
      	
								echo ( $_POST['cc_exp_year'] == $i ) ? ' SELECTED' : false;
      
								echo '>'.$i.'</option>';
   
							}
   
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="<?=$aForm['cc_cvv2']['style'] ?> align-right">
						<?=$aForm['cc_cvv2']['label'] ?><br />
						<div class="fcontent">
							<a href="help_cvv2.php"  onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )" style="font-size: 10px;">(why required?)</a>
						</div>
					</td>
					<td colspan="2">
						<input name="cc_cvv2" type="text" value="<?=$_POST['cc_cvv2'] ?>" class="finput" size="5" tabindex="20" maxlength="4" onKeyPress="return wholeNumber(this, event);">
					</td>
				</tr>
			</table>
			
			<?php
			
			}
			
			?>		
		</td>
		
		
		
	</tr>
	<tr valign="top">
		<td>
		<?php 
		
		if ( PAYMENT_CC && ( PAYMENT_CHECK_MO || PAYMENT_PO ) ) 
		{
		
			echo "<button class=\"fbtn full\" onClick=\"document.getElementById('payment_method').value = 'PAYMENT_CC'; toggle( 'PAYMENT_CC' , 'block' ); toggle( 'PAYMENT_PO' , 'none' ); toggle( 'PAYMENT_MO' , 'none' ); return false;\">Pay by Credit Card</button><br />";
			
		}
		
		if ( PAYMENT_CHECK_MO && ( PAYMENT_CC || PAYMENT_PO ) ) 
		{
		
			echo "<button class=\"fbtn full\" onClick=\"document.getElementById('payment_method').value = 'PAYMENT_MO'; toggle( 'PAYMENT_CC' , 'none' ); toggle( 'PAYMENT_PO' , 'none' ); toggle( 'PAYMENT_MO' , 'block' ); return false;\">Pay by Check</button><br />";
			
		}
		
		if ( ( PAYMENT_PO && ( $aMember['account']['enable_purchase_orders'] || $aMember['enable_purchase_orders'] ) ) && ( PAYMENT_CC || PAYMENT_CHECK_MO ) ) 
		{
		
			echo "<button class=\"fbtn full\" onClick=\"document.getElementById('payment_method').value = 'PAYMENT_PO'; toggle( 'PAYMENT_CC' , 'none' ); toggle( 'PAYMENT_PO' , 'block' ); toggle( 'PAYMENT_MO' , 'none' ); return false;\">Pay by Purchase Order</button><br />";
			
		}	
		

		?>

		</td>
		<td class="fcontent">
		<div name="PAYMENT_PO" id="PAYMENT_PO" style="<?php if ( $_POST['payment_method'] == 'PAYMENT_PO' ) { echo 'display: block;'; } else { echo 'display: none;'; } ?>">
		<?php
						
		if( PAYMENT_PO && ( $aMember['account']['enable_purchase_orders'] || $aMember['enable_purchase_orders'] ) ) { 
		?>
			<h3 class="dashed">
				Pay by Purchase Order
			</h3>

			<table width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td class="flabel align-right">
						&nbsp;
					</td>
					<td class="fcontent">
						<b><i><?=$aMember['name_first'] ?> <?=$aMember['name_last'] ?></i></b> you are authorized to order products using a purchase order.  
						please note that a purchase order is non-binding until we have notified you that it has been accepted.<p />  please select the 
						purchase order option below and provide any internal reference number you may need.
					</td>
				</tr>
				<tr>
					<td class="flabel align-right">
						<input type="checkbox" value="1" name="ptype">
					</td>
					<td class="fcontent">
						Yes - I will be using a purchase order
					</td>
				</tr>
				<tr>
					<td class="flabel nowrap align-right">
						Reference:
					</td>
					<td>
						<input name="ext_ref_number" type="text" value="<?=$_POST['ext_ref_number'] ?>" tabindex="15" class="finput full">
					</td>
				</tr>
			</table>

		<?php
		
		}
		
		?>		
		</div>
		
		<div name="PAYMENT_CC" id="PAYMENT_CC" style="<?php if ( PAYMENT_CC ) { echo 'display: block;'; } else { echo 'display: none;'; } ?>">
		
			
			
			</div>
			
			<div name="PAYMENT_MO" id="PAYMENT_MO" style="<?php if ( $_POST['payment_method'] == 'PAYMENT_MO' ) { echo 'display: block;'; } else { echo 'display: none;'; } ?>">
			
			<?php
			
			if ( PAYMENT_MO ) {
			
			?>
			
			<h3 class="dashed">
				Pay by Check
			</h3>
			
			<table width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td class="<?=$aForm['check_route_num']['style'] ?> align-right">
						<?=$aForm['check_route_num']['label'] ?>
					</td>
					<td>
						<input name="check_route_num" type="text" value="<?=$_POST['check_route_num'] ?>" class="finput full">
					</td>
				</tr>

				<tr>
					<td class="<?=$aForm['check_account_num']['style'] ?> align-right">
						<?=$aForm['check_account_num']['label'] ?>
					</td>
					<td>
						<input name="check_account_num" type="text" value="<?=$_POST['check_account_num'] ?>" class="finput full" onKeyPress="return wholeNumber(this, event);">
					</td>
				</tr>			
			
				<tr>
					<td class="<?=$aForm['check_num']['style'] ?> align-right">
						<?=$aForm['check_num']['label'] ?>
					</td>
					<td>
						<input name="check_num" type="text" value="<?=$_POST['check_num'] ?>" class="finput full" onKeyPress="return wholeNumber(this, event);">
					</td>
				</tr>
			</table>
			<p>
			Although we do accept Personal Checks, we must wait seven "7" business days after the day of 
			deposit for all	checks to clear, and we will only accept checks that
			are imprinted with your name and address. There is a fifty "$50.00" fee for
			bounced checks. <p />
			<b>DO NOT SEND US CASH! WE DO NOT ACCEPT CASH.</b> 
			</p>	
			<?php
			
			}
			
			?>
			
			</div>
			
		</td>
	</tr>
</table>
<br />
<center>	
	<input type="submit" value="review your order" class="fbtn" name="cSubmit">
</center>

</form>


<script type="text/javascript" src="shopping-cart.js"></script>
<script language="javascript">
if (!assocArray) var assocArray = new Object();
<?=$cJavascript ?>
</script>
<script language="JavaScript">

function setBillTo( oForm ) {

	if (oForm.sameas.checked == 1 ) {
	
		oForm.bill_name_salutation.value = "<?=addslashes( $aBasket['ship_name_salutation']) ?>";
		oForm.bill_name_first.value = "<?=addslashes( $aBasket['ship_name_first']) ?>";
		oForm.bill_name_last.value = "<?=addslashes( $aBasket['ship_name_last']) ?>";
		oForm.bill_company.value = "<?=addslashes( $aBasket['ship_company']) ?>";
		oForm.bill_address_1.value = "<?=addslashes( $aBasket['ship_address_1']) ?>";
		oForm.bill_address_2.value = "<?=addslashes( $aBasket['ship_address_2']) ?>";
		oForm.bill_address_3.value = "<?=addslashes( $aBasket['ship_address_3']) ?>";
		oForm.bill_address_city.value = "<?=addslashes( $aBasket['ship_address_city']) ?>";
	    oForm.bill_address_country.value = "<?=addslashes( $aBasket['ship_address_country']) ?>";
    	listboxItemSelected( oForm.bill_address_country , oForm.bill_address_stateid , 'state_');
    	oForm.bill_address_stateid.value = "<?=addslashes( $aBasket['ship_address_stateid']) ?>";
    	oForm.bill_address_phone.value = "<?=addslashes( $aBasket['ship_address_phone']) ?>";
	    oForm.bill_address_postal_code.value = "<?=addslashes( $aBasket['ship_address_postal_code']) ?>";
    	oForm.bill_email.value = "<?=addslashes( $aBasket['ship_email']) ?>";
    	
    }
}
</script>



<?php 

	include( 'checkout-cart-contents.php' );  
	
	include( '_footer.php' ); 

?>