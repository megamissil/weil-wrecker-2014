<?php

	require_once( 'functions.php' );
	
	if ( $aMember['memberid'] == 0 ) {
	
		redirect( '/parts' );
	
	}
	
	if ( is_mobile_device() ) {
    
    	include( 'basket-smartphone.php' );
    	
    	exit;
    
    }	

	$cPageTitle = $aMember['handle']. "'s : Favorite List";

    include( '_header.php' ); 
    
?> 

<form action="<?php if ( FOONSTER_ORDERS_SECURE == 1 ) { echo 'https://' . $_SERVER['HTTP_HOST'] . '/parts/checkout.php'; } else { echo 'http://' . $_SERVER['HTTP_HOST'] . '/parts/checkout.php'; } ?>" method="POST" name="mycart">
<input type="hidden" name="step" value="gotocheckout">

<table width="100%" cellpadding="5" cellspacing="0" border="0" class="bborder">
<tr>
<td width="1%" class="fcartheader">&nbsp;</td>
<td width="96%" class="fcartheader">
&nbsp;
</td>
<td width="1%" class="fcartheader" align="center">price</td>
</tr>

<?php
   if ( sizeof( $aBasket['products'] ) > 0 ) {
  
      $nTabIndex = 0;
      
      foreach ( $aBasket['products'] AS $cKey => $aItem ) {
               
         $nTabIndex++;
         
         $nAdjusted = 0;
         
         echo '<tr valign="top" class="fcartrow">';
         	echo '<td class="fcontent" align="center">';

         if ( $aItem['thumb'] != NULL ) {

			echo html_link("/parts/$aItem[productid]/".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-" , strtolower( trim( $aItem['title'] ) ) ) ).'.htm', '<img src="'.$aItem['thumb'].'" class="fimage" border="0">' ,'','');            
            
         }
         else {
            
			echo html_link("/parts/$aItem[productid]/".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-" , strtolower( trim( $aItem['title'] ) ) ) ).'.htm', '<img src="/parts/_images/missing.gif" hspace="5" vspace="5" width="50" height="75" class="fimage" border="0">' ,'','');
                        
         }                        
         // - .htaccess
                  
         echo '</td><td>';
         
         echo html_link("/parts/$aItem[productid]/".preg_replace("/[^0-9a-z-]/", "" , preg_replace("/\ /", "-" , strtolower( trim( $aItem['title'] ) ) ) ).'.htm',stripslashes( $aItem['title'] ),'','');    

         echo '<br/>item : '.stripslashes($aItem['sku']);
                  
		if ( $aItem['attribute_text'] != NULL ) {

	         echo '<br/>'.stripslashes($aItem['attribute_text']);
	         
		}		
         
		if ( $aItem['note'] != NULL ) {
			
			echo '<br/>'.stripslashes( nl2br($aItem['note'] ));
	
		}           
         
		if ( $aItem['promo_code'] != NULL ) {
		         
         
         }         
         	     
         echo '</td>';         

		
	         echo '<td class="fcontent" align="right">';
	         
	         if ( $aItem['baseprice'] != $aItem['price'] ) {
	         
	         	echo '<div style="text-decoration: line-through;">$' . number_format( $aItem['baseprice'] , 2 ) . '</div>';
	         
	         }
	         
	         echo '$' . number_format( $aItem['price'] , 2 );
	         
	         echo '<input type="hidden" name="price_'.$aItem['itemid'].'" value="$&nbsp;'.number_format($aItem['price'],2).'" class="finput_hidden" size="10" tabindex="200" readonly>';
	         
	         
	         echo '</td>';
    	 
         
         echo '</tr>';


      }
      
	echo '</table>';

                  
   }
   else {

      echo '<tr class="fcartrow"><td colspan="5">There are no items configured for your basket</td></tr>';

   }

?>  

</table>

</form>

<br/>&nbsp;


<script type="text/javascript" src="shopping-cart.js"></script>

<?php    include( '_footer.php' ); ?>
