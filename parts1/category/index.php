<?php
$nLimit = 8;
!is_numeric($_GET['cnt']) ? $_GET['cnt'] = 0 : false;
if ( substr( $_GET['cat'] , -1 ) == '/' ) { $_GET['cat'] = substr( $_GET['cat'], 0, -1 ); }            
$aCategory =  productCategory( str_replace('-', '_', $_GET['cat']), 'CATEGORY_ASC', $_GET['search'], $_GET['cnt'], $nLimit);

if ( $aCategory['title'] != NULL ) {	
	$cDir = '/parts';		
	$aOutput['title'] = html_link( $cDir , 'parts' );	
	foreach ( $aCategory[ 'directory' ] AS $nKey => $cValue ) {
			$cDir .= '/' . str_replace('_', '-', strtolower($cValue));
			$aOutput['title'] .= ' - ' . html_link ( $cDir , $aCategory[ 'directory_titles' ][ $nKey ] );		

	}						
} else {
	$aOutput['title'] = 'Parts';
}
$cPageTitle = $aOutput['title'];

$cMetaDesc = $aCategory['meta_description'];
$cMetaKW = $aCategory['meta_keywords'];
$cSEOTitle = $aCategory['meta_abstract'];

include "category/bootstrap/index.php";