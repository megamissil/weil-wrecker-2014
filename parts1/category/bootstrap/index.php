<?php
if ( sizeof($category['products']['results']) > 0 ) {
	$loopCount = 0;
	foreach ( $category['products']['results'] AS $nKey => $product) { 
		$product = (object) $product;	
		$productUrl = $product->sku . '~' . urlencode(strtolower($product->title)) . '.htm';
		$loopCount++;
		$url = $productUrl;
		echo ($loopCount === 1) ? '<div class="row">' : false;
		echo '<div class="col-sm-3">';
		echo '<a href="' . $url .'">';
		echo '<img src="' . $product->image . '" alt="" class="img img-responsive">';
		echo html_link($url, $product->title);
		echo '</a>';
		echo '</div>';
		echo ($loopCount === 4) ? '</div>' : false;
		($loopCount === 4) ? $loopCount = 0 : false;		
	}
	echo ($loopCount !== 4) ? '</div>' : false;		
} else {
	echo '<div class="row col-sm-12">There are no products associated with this category.</div>';
}
?>