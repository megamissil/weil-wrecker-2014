<footer>
    <div class="container">
        <section>
            <div class="row">
                <div class="col-xs-10 col-center visible-xs">
                    <ul class="social">
                        <li><a href="https://www.facebook.com/pages/Weil-Wrecker-Service-Inc/173689346520" target="_blank"><img src="/img/social/facebook-icon.jpg" alt="facebook icon" /></a></li>
                        <li><a href="https://twitter.com/WeilWrecker" target="_blank"><img src="/img/social/twitter-icon.jpg" alt="twitter icon" /></a></li>
                        <li><a href="#" target="_blank"><img src="/img/social/youtube-icon.jpg" alt="youtube icon" /></a></li>
                    </ul>
                </div>
            
                <div class="col-md-4 col-sm-4">
                    <p class="copyright">
                        Weil Wrecker, Inc. Copyright <?php echo date('Y'); ?>
                    </p>
                </div>
            
                <div class="col-md-4 col-sm-4">
                    <p class="text-center">
                        3400 Second Ave South<br>
                        Birmingham, AL 35222
                    </p>
                </div>
            
                <div class="col-md-4 col-sm-4 hidden-xs">
                    <ul class="social">
                        <li><a href="https://www.facebook.com/pages/Weil-Wrecker-Service-Inc/173689346520" target="_blank"><img src="/img/social/facebook-icon.jpg" alt="facebook icon" /></a></li>
                        <li><a href="https://twitter.com/WeilWrecker" target="_blank"><img src="/img/social/twitter-icon.jpg" alt="twitter icon" /></a></li>
                        <li><a href="https://www.youtube.com/channel/UChwQKxQXR8GaXawkhwJmFCA" target="_blank"><img src="/img/social/youtube-icon.jpg" alt="youtube icon" /></a></li>
                    </ul>
                    <div class="clear-fix"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-12 hidden-xs">
                    <a href="http://zeekeeinteractive.com/" target="_blank" class="zeekee-slug"><img src="/img/zeekee-slug-white-2011.png" alt="Powered by Zeekee Interactive" /></a>
                </div>
                
                <div class="col-xs-5 col-center visible-xs">
                    <a href="http://zeekeeinteractive.com/" target="_blank" class="zeekee-slug"><img class="text-center" src="/img/zeekee-slug-white-2011.png" alt="Powered by Zeekee Interactive" /></a>
                </div>
            </div>
        </section>
    </div>
</footer>

<script src="/bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js"></script>
<script src="/js/slick.min.js"></script>
<script src="/js/jquery.colorbox-min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>