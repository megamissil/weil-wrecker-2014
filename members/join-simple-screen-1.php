<?php 

if ( $_SERVER['PHP_SELF'] == 'join-simple-screen-1.php' ) { header( 'Location: join.php' ); }

$cPageTitle = 'Create an Account ' ;

include '_header.php'; 

?>
	<h1><?=$cPageTitle ?></h1>
	<br />
	<br />
<script language="javascript" type="text/javascript" src="foonster.js"></script>

	<?php

		echo !empty( $_POST['error'] ) ? '<div class="ferror">' . $_POST['error'] . '</div>' : false;
		
	?>
	<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="fjoin">
	<center>
	<table cellpadding="5" cellspacing="0" width="50%">
		<tr>
			<td class="<?=$aForm['name_first']['style'] ?>" width="35%">
				<?=$aForm['name_first']['label'] ?>
			</td>
			<td width="45%">
				<input type="text" name="name_first" value="<?=$_POST['name_first'] ?>" class="finput full" onBlur="fcheck( this , 'empty' , 'ferr_name_first' , 'required field' )" onKeyPress="return verifyAlpha(this, event);" placeholder="Your first name" required>
			</td>
			<td width="30%">
				<div class="fcontent" id="ferr_name_first"><?=$aForm['name_first']['msg'] ?></div>
			</td>
		</tr>

		<tr>
			<td class="<?=$aForm['name_last']['style'] ?>">
				<?=$aForm['name_last']['label'] ?>
			</td>
			<td>
				<input type="text" name="name_last" value="<?=$_POST['name_last'] ?>" class="finput full" onBlur="fcheck( this , 'empty' , 'ferr_name_last', 'required field' )" onKeyPress="return verifyAlpha(this, event);" placeholder="Your last name" required>
			</td>
			<td>
				<div class="fcontent" id="ferr_name_last"><?=$aForm['name_last']['msg'] ?></div>
			</td>
		</tr>

		<tr>
			<td class="<?=$aForm['email']['style'] ?>">
				<?=$aForm['email']['label'] ?>
			</td>
			<td>
				<input type="text" name="email" value="<?=$_POST['email'] ?>" class="finput full" onBlur="fcheck( this , 'email' , 'ferr_email', 'invalid email address' );" placeholder="Enter a valid email address" required>
			</td>
			<td>
				<div class="fcontent" id="ferr_email"><?=$aForm['email']['msg'] ?></div>
			</td>
		</tr>

		<tr>
			<td class="<?=$aForm['handle']['style'] ?>">
				<?=$aForm['handle']['label'] ?>
			</td>
			<td class="fcontent">
				<input type="text" name="handle" value="<?=$_POST['handle'] ?>" class="finput full" onBlur="fcheck( this , 'availuname' , 'ferr_handle', '' );" onKeyPress="return verifyAlphaNum(this, event);" placeholder="A username you will remember" required>
			</td>
			<td>
				<div class="fcontent" id="ferr_handle"><?=$aForm['handle']['msg'] ?></div>
			</td>
		</tr>
		
		<tr>
			<td class="<?=$aForm['passwd']['style'] ?>">
				<?=$aForm['passwd']['label'] ?>
			</td>
			<td class="fcontent">
				<input type="password" name="passwd" value="<?=$_POST['passwd'] ?>" class="finput full" placeholder="Enter a password" required>
			</td>
			<td>
				<div class="fcontent"><?=$aForm['passwd']['msg'] ?></div>
			</td>
		</tr>		

		<tr>
			<td class="<?=$aForm['opasswd']['style'] ?>">
				<?=$aForm['opasswd']['label'] ?>
			</td>
			<td class="fcontent">
				<input type="password" name="opasswd" value="<?=$_POST['opasswd'] ?>" class="finput full" placeholder="Enter a password again." required><br />
			</td>
			<td>
				<div class="fcontent"><?=$aForm['opasswd']['msg'] ?></div>
			</td>
		</tr>		
		
		<tr>
			<td colspan="3">
				<h2 class="headline">Security Information</h2>
				<hr />
			</td>
		</tr>		
		<tr>
			<td colspan="3" class="fcontent">
				This is a question/answer combonation that we will use that to verify your account information.  If you forget your
				account access credentials.<b>WE DO NOT HAVE ACCESS TO YOUR PASSWORD AND CAN ONLY RESET IT.</b><p />
						
				<ul> 
    				<li>Choose a question only you know the answer to - make sure the question isn't associated with your password.<p />
					<li>Pick a question that can't be answered through research (for example, avoid your mother's maiden name, your birth date, your first or last name, your social security number, your phone number, your pet's name, etc.).<p />
					<li>Make sure your answer is memorable, but not easy to guess. Use an answer that is a complete sentence for even more security.<p /> 						
				</ul>
			</td>
		</tr>		

		<tr>
			<td class="<?=$aForm['pw_question']['style'] ?>">
				<?=$aForm['pw_question']['label'] ?>
			</td>
			<td class="fcontent" colspan="2">
				<input type="text" name="pw_question" value="<?=$_POST['pw_question'] ?>" class="finput full" placeholder="Enter a question" required>
			</td>
		</tr>		

		<tr>
			<td class="<?=$aForm['pw_answer']['style'] ?>">
				<?=$aForm['pw_answer']['label'] ?>
			</td>
			<td class="fcontent" colspan="2">
				<input type="text" name="pw_answer" value="<?=$_POST['pw_answer'] ?>" class="finput full" placeholder="Enter an answer" required><br />
			</td>
		</tr>		
		
		<tr>
			<td class="fcontent" colspan="3">
				By clicking on 'I accept' below you are agreeing to the <a href="/terms-of-serivice.php">Terms of Service</a> and the <a href="/privacy-policy.php">Privacy Policy</a>.  
				<p />
				You will recieve an email from us with your account activation link, so ensure that all your spam filters, email
				clients and ISP settings are properly configures.
				
			</td>
		</tr>
		
		<tr>
			<td class="fcontent">
			&nbsp;
			</td>
			<td class="fcontent">
				<input type="submit" name="sbtbtn" class="fbtn" value="I want to JOIN">
			</td>
			<td>
			&nbsp;
			</td>
		</tr>
	</table>	
	</center>
</form>	

<p>&nbsp;</p>

<?php include '_footer.php';