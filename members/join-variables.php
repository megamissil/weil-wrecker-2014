<?php

	require_once( FOONSTER_PATH.'/site/configs/orders-configuration.php' );	

	$lSSL = 0; // 1 = require SSL connection - 0 = no SSL required

	// settings for staff notification email.
   
	$aStaffEmail = array();
	$aStaffEmail['to'] = '';  // single email address
	$aStaffEmail['cc'] = '';  // email address - multiples seperated by commas
	$aStaffEmail['bcc'] = ''; // email address - multiples seperated by commas
	$aStaffEmail['subject'] = ''; // subject line of email
	// -- html version of email is located at staff-email-html.php	
	// -- plaintext version of email is located at staff-email-plaintext.php	


	$aCustomerEmail = array();
	$aCustomerEmail['from'] = ''; // single email address
	$aCustomerEmail['subject'] = ''; // subject line of email.
	// -- html version of email is located at user-email-html.php	
	// -- plaintext version of email is located at user-email-plaintext.php	
         
	$aSalutations = array( 'Mr.' , 'Mrs.' , 'Ms.' , 'Dr.' , 'Prof.' );
   
	$aMonths = array( '01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October','11' => 'November','12' => 'December' );    
   
	$aForm = array(
		'handle' => array( 'label' => 'Desired User Name*' , 'style' => 'flabel align-right'  , 'msg' => 'Examples: JSmith, John.Smith'),
		'bill_name_first' => array( 'label' => 'Name&nbsp;First*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_name_last' => array( 'label' => 'Name&nbsp;Last*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_address_1' => array( 'label' => 'Address*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_address_2' => array( 'label' => '&nbsp;' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_address_city' => array( 'label' => 'City*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_address_state' => array( 'label' => 'State*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_address_postal_code' => array( 'label' => 'Zip Code*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_email' => array( 'label' => 'Email*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_address_country' => array( 'label' => 'Country' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'amount' => array( 'label' => 'Amount*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'cc_type' => array( 'label' => 'Card&nbsp;Type*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'cc_number' => array( 'label' => 'Number*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'cc_cvv2' => array( 'label' => 'Security&nbsp;Code*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'cc_exp_month' => array( 'label' => 'Expiration&nbsp;Date*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'cc_name_on_card' => array( 'label' => 'Name&nbsp;on&nbsp;Credit&nbsp;Card*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'msg' => array( 'label' => 'Note' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'bill_address_phone' => array( 'label' => 'Phone*' , 'style' => 'flabel align-right'  , 'msg' => ''),   
		'name_first' => array( 'label' => 'First&nbsp;Name*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'name_last' => array( 'label' => 'Last&nbsp;Name*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'name_middle' => array( 'label' => 'Middle&nbsp;Name' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'phone_home' => array( 'label' => 'Phone*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'spouse_name_first' => array( 'label' => 'First&nbsp;Name*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'spouse_name_last' => array( 'label' => 'Last&nbsp;Name*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'spouse_name_middle' => array( 'label' => 'Middle&nbsp;Name' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'spouse_email' => array( 'label' => 'Email*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'spouse_phone_home' => array( 'label' => 'Phone*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'email' => array( 'label' => 'Email*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'line_1' => array( 'label' => 'Address*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'line_2' => array( 'label' => '&nbsp;' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'city' => array( 'label' => 'City*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'state' => array( 'label' => 'State*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'phone' => array( 'label' => 'Phone*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'postalcode' => array( 'label' => 'Zip Code*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'pw_question' => array( 'label' => 'Security&nbsp;Question*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'pw_answer' => array( 'label' => 'Security&nbsp;Password*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'spouse_opasswd' => array( 'label' => 'Confirm&nbsp;Password*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'spouse_passwd' => array( 'label' => 'Password*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'opasswd' => array( 'label' => 'Re-enter Password*' , 'style' => 'flabel align-right'  , 'msg' => ''),
		'passwd' => array( 'label' => 'Choose a Password*' , 'style' => 'flabel align-right'  , 'msg' => 'Minimum of 8 characters in length. ')
	);	
	
	$aCreditCards = ordersCardTypes(); // TYFOON FUNCTION
   		
// -- that's all folks	

?>