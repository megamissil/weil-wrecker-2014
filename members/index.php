<?php
require_once __DIR__ . '/authenticate.php'; // connect to tyfoon
include '_header.php';
?>	
<h1>
	Auction Wishlist
</h1>
<br />
<?php
$auctions = mysql_query(
"SELECT pav.*, p.*, pp.* FROM 
members_wishlist mw
LEFT JOIN products p ON (mw.productid = p.productid)
LEFT JOIN products_additional_values pav  ON (pav.productid = p.productid)
LEFT JOIN products_photos pp ON (pp.productid = pav.productid AND pp.sortorder = 1)
WHERE p.active = 1 AND mw.memberid = '$member[memberid]'
ORDER BY pav.addl_addl_year2"
); 

if (!mysql_error()) { 
while ($auction = mysql_fetch_object($auctions)) { ?>
	<div class="col-md-6">
		<div class="auction-list">
			<ul class="small-blocks">
				<li>
					<figure>
					<div class="image">
						<div class="capwrapper">
							<span id="sku">
							<?php 
							echo html_link('/auctions/auction.php?cn=' . $auction->sku, $auction->sku . ' ') ;
							?>
							</span>
						</div>
						<?php 
						$img = '/tyfoon/site/products/images/' . $auction->photo_path;
						$thumb = '/tyfoon/site/products/thumbs/' . $auction->thumb_path;
						echo html_link('/auctions/auction.php?cn=' . $auction->sku, '<img class="auctions-img" src=' . $thumb . '>') ;
						?>
					</div>
	
					<figcaption class="hidden-xs hidden-sm">
					<?php
					echo html_link('auction.php?cn=' . $auction->sku, $auction->addl_addl_year2 . ' ' . $auction->addl_add_make . ' ' . $auction->addl_addl_model . ' ') ;
					?>
					</figcaption>
					</figure>	
				</li>
			</ul>
		</div>
	</div>
<?php } 
} else {
	echo 'You have no wishlist auctions configured.';
}



include '_footer.php';
