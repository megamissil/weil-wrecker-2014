<?php

	require_once( '_connect.php' ); // connect to tyfoon
		
	$cPageTitle = $aProfile['handle'];

	include( '_header.php' );
			
	include( 'navigation.php' );
	
?>	
	
	<hr />

	<div class="float-right align-center">

	<?php 

	if ( $aProfile['photo'] != NULL ) {
	
		echo '<img src="' . $aProfile['photo'] . '"><p />';
			
	}
	else {
	
		echo '<img src="/_images/missing-image.jpg"><p />';
	
	}
	
	if ( $aProfile['memberid'] == $aMember['memberid'] ) {
	
		echo '<a href="/members/delete-photo.php" onclick="return confirmLink( '."'Are you sure you want to remove the photo assocaited with your account.".'\n'."'".');" class="crumbtrail"><br />[delete]</a>&nbsp;';

		echo html_link( '/members/change-photo.php' , '[change]' , 'crumbtrail' );
		
	}
	
	echo '</div>';
	
	?>

	<?php if ( $aProfile['memberid'] == $aMember['memberid'] ) { ?>

	<table>
		<tr>
			<td width="25%" class="flabel align-right">
				Title: 	
			</td>
			<td width="75%" class="fcontent">
				<?=$aMember['name_title'] ?>
			</td>
		</tr>
		<tr>
			<td class="flabel align-right">
				First Name: 	
			</td>
			<td class="fcontent">
				<?=$aMember['name_first'] ?>
			</td>
		</tr>
		<tr>
			<td class="flabel align-right">
				Last Name: 	
			</td>
			<td class="fcontent">
				<?=$aMember['name_last'] ?>
			</td>
		</tr>
		<tr>
			<td class="flabel align-right">
				Email: 	
			</td>
			<td class="fcontent">
				<?=$aMember['email'] ?>
			</td>
		</tr>
		<tr>
			<td class="flabel align-right">
				Home&nbsp;Phone: 	
			</td>
			<td class="fcontent">
				<?=$aMember['phone_home'] ?>
			</td>
		</tr>
		<tr>
			<td class="flabel align-right">
				Work&nbsp;Phone: 	
			</td>
			<td class="fcontent">
				<?=$aMember['phone_work'] ?>
			</td>
		</tr>
		<tr>
			<td class="flabel align-right">
				Cell&nbsp;Phone: 	
			</td>
			<td class="fcontent">
				<?=$aMember['phone_cell'] ?>
			</td>
		</tr>
		<tr valign="top">
			<td class="flabel align-right">
				Address:
			</td>
			<td class="fcontent">
				<?php
				if ( $aMember['line1'] != NULL ) {
			
					echo $aMember['line1'] . '<br />';
					echo $aMember['city'] . ' ' . $aMember['state'] . ' ' . $aMember['postalcode'] . '<br />';
					echo '<span class="flabel">P:' . '</span>' . $aMember['phone'] . '<br />';
					echo '<span class="flabel">F:' . '</span>' . $aMember['fax'] . '<br />';
					
				}
				?>
			</td>
		</tr>
	</table>
	
	<p>&nbsp;</p>
	
	<?php } 
	
	if ( $aProfile['msg'] != NULL ) {

		echo '<div class="headline">Biography</div><hr />';

		echo '<div class="fcontent">' . $aProfile['msg'] . '</div>';	
		
		echo '<span style="clear: both;"> </span>';
	
	}
	
	?>
	
	
	<table widht="100%" cellpadding="5" cellspacing="0">
		<tr>
			<td colspan="2">
				<h2 class="headline">
					Social Media Networks
				</h2>
				<hr />
			</td>
		</tr>				
		
		<tr>
			<td valign="top">
			
				<table cellpadding="5" cellspacing="0" width="100%">	
					<tr>
						<td class="flabel align-right" width="150">
							AIM:
						</td>
						<td width="*" class="fcontent">
							<?=$aProfile['aim'] ?>
						</td>
					</tr>				

					<tr>
						<td class="flabel align-right">
							Facebook:
						</td>
						<td class="fcontent">
							<?=$aProfile['facebook'] ?>
						</td>
					</tr>				

					<tr>
						<td class="flabel align-right">
							Flickr:
						</td>
						<td class="fcontent">
							<?=$aProfile['flickr'] ?>
						</td>
					</tr>				

					<tr>
						<td class="flabel align-right">
							Google&nbsp;Talk:
						</td>
						<td class="fcontent">
							<?=$aProfile['googletalk'] ?>
						</td>
					</tr>				

					<tr>
						<td class="flabel align-right">
							ICQ:
						</td>
						<td class="fcontent">
							<?=$aProfile['icq'] ?>
						</td>
					</tr>				

					<tr>
						<td class="flabel align-right">
							Jabber:
						</td>
						<td class="fcontent">
							<?=$aProfile['jabber'] ?>
						</td>
					</tr>				
				
				</table>
			
			</td>
			<td valign="top">
			
				<table cellpadding="5" cellspacing="0" width="100%">	

					<tr>
						<td class="flabel align-right" width="150">
							MSN:
						</td>
						<td width="*" class="fcontent">
							<?=$aProfile['msn'] ?>
						</td>
					</tr>				
					
					<tr>
						<td class="flabel align-right">
							Skype:
						</td>
						<td class="fcontent">
							<?=$aProfile['skype'] ?>
						</td>
					</tr>				

					<tr>
						<td class="flabel align-right">
							Twitter:
						</td>
						<td class="fcontent">
							<?=$aProfile['twitter'] ?>
						</td>
					</tr>				

					<tr>
						<td class="flabel align-right">
							Yahoo:
						</td>
						<td class="fcontent">
							<?=$aProfile['yahoo'] ?>
						</td>
					</tr>				
					
				</table>
			
			</td>
		</tr>
	</table>
	
<?php

	include( '_footer.php' );

?>