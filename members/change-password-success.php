<?php

	if ( $_SERVER['REQUEST_METHOD'] != 'POST' ) { 
	
		redirect( 'change-pasword.php'); 
		
	}
	
	require_once( '_connect.php' ); // connect to tyfoon
	
	$cPageTitle = 'Success';

	include( '_header.php' ); 

?>	

	<h2 class="subcategory">
		<a href="/">
			<?=FOONSTER_SITE_TITLE ?>
		</a>
		 : 
		 <a href="/members">
		 	Members
		 </a>
	</h2>
	<h3 class="large-headline">
		<?=$cPageTitle ?>
	</h3>	
	<hr />
	
Congratulations, your password has been changed.

<?php

	include '../_footer.php';

?>