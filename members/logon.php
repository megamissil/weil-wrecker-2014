<?php

	require_once( $_SERVER['DOCUMENT_ROOT'] . '/tyfoon/connect.php' );
	
	$aForm = array(
		'handle' => array( 'label' => 'Username*' , 'style' => 'flabel'  , 'msg' => ''),
		'passwd' => array( 'label' => 'Password*' , 'style' => 'flabel'  , 'msg' => '')   
	);	
   
	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		if ( xmember_logon( $_POST['handle'] , $_POST['passwd'] , 0 , $_POST['error'] ) ) {
			redirect('/members/');
		} else {
			if ( $_POST['error'] == 'Your account has expired.' ) {
				$nNow = ( time() + 3600 );
				setcookie( 'RENEW', "$_POST[handle]<@>$_POST[passwd]" , $nNow , '/' );
				redirect( 'renew.php' );
				
			}
		}

	} else {
		$_POST['refer'] = $_SERVER['HTTP_REFERER'];
	}
	
$cPageTitle = 'Logon';
include '_header.php';

?>	
	<h1><?=$cPageTitle ?></h1>
	<br />
	<br />
<table cellpadding="5" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="50%" class="fcontent">
						
			<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="flogon">
			<input type="hidden" name="refer" value="<?=$_POST['refer'] ?>">
				<center>
				<table cellpadding="5" cellspacing="0" width="40%">				
					<tr>
						<td class="fheader">
							<div class="ferror" style="display: inline;"><?=$_POST['error'] ?></div>
						</td>
					</tr>
				
					<tr>
						<td class="<?=$aForm['handle']['style'] ?>">
							<?=$aForm['handle']['label'] ?> 
						</td>
					</tr>
					<tr>	
						<td width="*">
							<input type="text" name="handle" value="<?=$_POST['handle'] ?>" class="finput full" placeholder="enter your username" autofocus required>
						</td>
					</tr>
					<tr>
						<td class="<?=$aForm['passwd']['style'] ?>">
							<?=$aForm['passwd']['label'] ?> 
						</td>
					</tr>
					<tr>
						<td>
							<input type="password" name="passwd" autocomplete="off" class="finput full" placeholder="enter your password" required>
						</td>
					</tr>
					<tr>
						<td>
							<br />
							<button type="submit" name="sbtbtn" value="Logon" class="button">Logon</button>
						</td>
					</tr>
				</table>
				</center>
			</form>	
			<br />
			<ul>			
				<li><a href="account-retrieval.php">Having issues accessing your account</a>.</li>
				<li>Learn how to <a href="protect-account.php">protect your account</a>!</li>
			</ul>
			
		</td>
		<td width="50%">
		
			<!-- 
		
				<h2>Need&nbsp;an&nbsp;Account?</h2>
			
				Creating an Account is fast and easy.<p />
			
				<button class="button" onClick="location.href='join.php';" title="Create Account">Create Account</a>
				
			-->
			
		</td>
	</tr>
</table>                    
<?php include '_footer.php'; ?>