  <?php

	if ( $_SERVER['REQUEST_METHOD'] != 'POST' ) 
	{ 
	
		redirect( '/members/join.php'); 
	
	}
	
	$cPageTitle = 'Welcome to ' . FOONSTER_SITE_TITLE;

	include '../_header.php';
?>	

	<h1><?=$cPageTitle ?></h1>
	<br />
	<br />

<?php	
	echo 'Thank you, for joining.  You will recieve an email with your membership activation link soon.';

	include '../_footer.php';
	
?>