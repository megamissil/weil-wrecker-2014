<?php 
if (substr(strtolower($_SERVER['REQUEST_URI']), 0, 6) == '/parts') {    
    $parts = productCategory(strtoupper('PARTS'));      
    if (sizeof($parts['sub']) > 0) {
    ?>        
    <div class="faq-side">
        <a href="/parts"><span><nobr>PARTS</nobr></span></a> 
    </div>
    <div class="row">
        <div class="col-sm-12">
    <?php 
        foreach ( $parts['sub'] AS $cKey => $aValue ) {
            $url = '/parts/' . strtolower($aValue['code']). '/';
            ?>
            <br />
            <ul>
                <a href="<?= $url ?>"><?= $aValue['title']; ?></a><br />
            </ul>
            <?php echo !empty($aValue['msg']) ? $aValue['msg'] : false;
        }        
    }   
    ?>
        </div>
    </div>
    <?php 
}

if ($member['memberid'] > 0) {
?>
<div class="faq-side">
    <a href="#"><span><nobr>WELCOME</nobr></span></a> 
</div>

    <div class="row">
    	<div class="col-sm-12">
    	<div style="color: white; font-weight: bold; font-size: 18px;">Welcome, <?= $member['name_first'] ?></div>
    	<ul>    	
	    	<li><a href="/members/">My Wishlist</a></li>
    		<li><a href="/members/logout.php">Logout</a></li>
    	</ul>
    	</div>
	</div>

<?php 
} else {
?>
<div class="faq-side">
    <a href="/members/logon.php"><span><nobr>LOG ON</nobr></span></a> 
</div>
    <div class="row">
    	<div class="col-sm-12">

 <form action="/members/logon.php" method="POST" name="flogon">
	<input type="hidden" name="refer" value="<?=$_POST['refer'] ?>">
    
    <div class="row">
    	<div class="col-sm-12">
			<input type="text" name="handle" value="<?=$_POST['handle'] ?>" class="form-control" placeholder="enter your username" autofocus required>
    	</div>
    </div>
    <br />
    <div class="row">
    	<div class="col-sm-12">
			<input type="password" name="passwd" autocomplete="off" class="form-control" placeholder="enter your password" required>
    	</div>
    </div>
    <br />
    <br />
    <div class="row">
    	<div class="col-sm-12 text-center">
			<button type="submit" name="sbtbtn" value="Log on" class="button">Log on</button>
    	</div>
    </div>
    <br />
    <br />   
	</form>	
	<a href="/members/account-retrieval.php">Having issues accessing your account</a><br />
	Learn how to <a href="/members/protect-account.php">protect your account</a>!<br />
	<br />
	</div>
</div>    
<?php 
}
?>

