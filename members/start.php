<?php

	$aCategory = fn_Category( 'members_channels' , $_GET['cat'] );

	$aCategories = fn_Categories( 'members_channels' );
	
	$cPageTitle = 'Members';

	include( '_header.php' );

	include( $_SERVER['DOCUMENT_ROOT'] . '/members/_user.php' ); 
	
?>
	
	<h2 class="subcategory">
		<a href="/">
			<?=FOONSTER_SITE_TITLE ?>
		</a>
		<?php
		if ( sizeof( $aCategory['crumbtrail'] ) > 0 ) {
	
			foreach ( $aCategory['crumbtrail'] AS $nKey => $aValue ) {
		
				echo ' | ' . html_link( 'index.php?cat=' . $aValue['code'] , stripslashes( $aValue['title'] ) );
		
			}
			
		}
		?>
	</h2>
	<h3 class="large-headline">
		Members
	</h3>	
	
	<?php
			
	if ( $aCategory['listid'] > 0 ) {
		
		if ( sizeof( $aCategory['children'] ) > 0 ) {
	
			foreach ( $aCategory['children'] AS $nKey => $aValue ) {
		
				echo html_link( 'index.php?cat=' . $aValue['code'] , stripslashes( $aValue['title'] ) ) . ' | ';
		
			}
			
		}
		
		echo '<hr />';		
	
	}	
	else {
	
		foreach ( $aCategories AS $nKey => $aValue ) {
		
			if ( $aValue['prntid'] == 0 ) {
		
				echo html_link( 'index.php?cat=' . $aValue['code'] , stripslashes( $aValue['title'] ) ) . ' | ';
				
			}
		
		}
	
		// echo '<hr />';
	
	}
		
	
	
	$aMembers = membersByChannel( $_GET['cat'] );
	
	foreach ( $aMembers['members'] AS $nId => $aMember ) {
	
		echo html_link( '/members/' . $aMember['handle'] , $aMember['handle'] ) . '<br />';
	
	}	

?>

<?php
	include( '_footer.php' );

?>