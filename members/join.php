<?php

	require_once( $_SERVER['DOCUMENT_ROOT'] . '/tyfoon/connect.php' );
		   
	require_once( 'join-variables.php' );   
	
	if ( $_SERVER['HTTPS'] != "on" && $lSSL == 1 ) { redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); }
	      
	$nNow = time();
	
	$cResult = '';
              
	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	
		$aSize = preg_split( '/ /' , trim( $_POST['pw_question'] ) );
    			        
		if ( sizeof( $aSize ) >= 4 && $_POST['pw_answer'] != NULL && $_POST['passwd'] == $_POST['opasswd'] && $_POST['passwd'] != NULL && $_POST['handle'] != NULL && is_valid_email_address( $_POST['email'] ) && $_POST['name_first'] != NULL && $_POST['name_last'] != NULL ) {
				
			$cResult = xmember_join( $_POST , false , false );

			if ( $cResult == 1 ) {

				include( 'join-simple-screen-2.php' );			
				
				exit;
			
			}
			else {
			
				$_POST['error'] = $cResult;

				include( 'join-simple-screen-1.php' ); 
				
				exit;
			
			}
        
		}
		else {
        	
			$_POST['error'] = 'Your registration failed, please refer to the fields marked in RED.';
			if ( strlen( $_POST['handle'] ) < 8 ) { $aForm['handle']['style'] = 'ferror align-right'; $aForm['handle']['msg'] = 'username must be greater than 8 characters'; $aForm['ferr_name_first']['style'] = 'ferror';  }
			if ( $_POST['pw_answer'] == NULL ) { $aForm['pw_answer']['style'] = 'ferror align-right'; $aForm['pw_answer']['msg'] = 'required field'; }
			if ( sizeof( $aSize ) < 4 ) { $aForm['pw_question']['style'] = 'ferror align-right'; $aForm['pw_question']['msg'] = 'required field'; }
			if ( $_POST['name_first'] == NULL ) { $aForm['name_first']['style'] = 'ferror align-right'; $aForm['name_first']['msg'] = 'required field'; }
			if ( $_POST['name_last'] == NULL ) { $aForm['name_last']['style'] = 'ferror align-right'; $aForm['name_last']['msg'] = 'required field'; }
			if ( $_POST['passwd'] == NULL ) { $aForm['passwd']['style'] = 'ferror align-right'; $aForm['passwd']['msg'] = 'required field'; }
			if ( $_POST['passwd'] != $_POST['opasswd'] ) { $aForm['passwd']['style'] = 'ferror align-right'; $aForm['passwd']['msg'] = 'passwords do not match'; }
			if ( !is_valid_email_address( $_POST['email'] ) ) { $aForm['email']['style'] = 'ferror align-right'; $aForm['email']['msg'] = 'invalid email address'; }
             
			include( 'join-simple-screen-1.php' ); 
        	
			exit;
        
		}
    	 
	}
	else {
        
		include( 'join-simple-screen-1.php' );
        
        exit;
     
	}
  
// - that's all folks
?>  