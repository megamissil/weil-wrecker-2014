<?php

	include( '_header.php' );

?>	
	<h1><?=$cPageTitle ?></h1>
	<br />
	<br />
<table cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="50%">
			<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="flogon">
				<table cellpadding="5" cellspacing="0" width="100%" align="center">				
					<tr>
						<td class="fheader">
							<div class="ferror" style="display: inline;"><?=$_POST['error'] ?></div>
						</td>
					</tr>				
					<tr>
						<td class="<?=$aForm['handle']['style'] ?>">
							<?=$aForm['handle']['label'] ?> 
						</td>
					</tr>
					<tr>	
						<td width="*">
							<input type="text" name="handle" value="<?=$_POST['handle'] ?>" class="finput full" placeholder="Enter your username" required>
						</td>
					</tr>
					<tr>
						<td class="<?=$aForm['name_first']['style'] ?>">
							<?=$aForm['name_first']['label'] ?> 
						</td>
					</tr>
					<tr>	
						<td width="*">
							<input type="text" name="name_first" value="<?=$_POST['name_first'] ?>" class="finput full" placeholder="Enter your first name" required>
						</td>
					</tr>
					<tr>
						<td class="<?=$aForm['name_last']['style'] ?>">
							<?=$aForm['name_last']['label'] ?> 
						</td>
					</tr>
					<tr>	
						<td width="*">
							<input type="text" name="name_last" value="<?=$_POST['name_last'] ?>" class="finput full" placeholder="Enter your last name" required>
						</td>
					</tr>
					
					<tr>
						<td>
							<br />
							<input type="submit" class="fbtn" name="sbtbtn" value="Start Account Retrieval" /> 
						</td>
					</tr>
				</table>
			</form>	
						
		</td>
		<td width="50%" class="fcontent">		
				If you've forgotten your account credentials, this form will help you recover it. Enter the email address or username associated with your account, then provide the answer to your secret question and we will immediately email you a link to reset your password.
				<br />&nbsp;<br />	
				If you don't receive an email with a link to reset your account information, make sure you don't have a spam filter that would prevent delivery of emails from our domain.
				<br />&nbsp;<br />
				Also, we may have an invalid email address in our records, in which case you should write to us to have it changed.
				<br />&nbsp;<br />
				If you have not yet registered for our site, please click here to join! Creating an Account is fast and easy.
				<br />&nbsp;<br />
				<center>
					<button type="button" class="fbtn" name="njoin" onClick="location.href='join.php'">JOIN US NOW</button>
				</center>
		</td>
	</tr>
</table>                    
<?php include( '_footer.php' ); ?>