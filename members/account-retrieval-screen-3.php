<?php

	include '../_header.php';

?>	
	<h1><?=$cPageTitle ?></h1>
	<br />
	<br />
An email has been sent to the email address associated with this account and will include your username and new password.

<br>&nbsp;</br>

Please be patient; the delivery of email may be delayed, due to internet bandwidth.

<br>&nbsp;</br>

Additionally, Remember to confirm that the email above is correct and to check 
your junk / spam folder or filter if you do not receive this email.  If you are
behind a corporate firewall, you may need to contact your system administrator
for more information regarding your email server spam settings.


<?php include '../_footer.php'; ?>