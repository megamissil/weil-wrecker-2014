<?php

	require_once( '_connect.php' );
	
	if ( empty( $aMember['memberid'] ) ) { redirect( '/members/index.php' ); }
	
	$aForm = array(
		'required_npasswd' => array( 
			'label' => 'New&nbsp;Password*' , 
			'style' => 'flabel align-right'  , 
			'msg' => ''),
		'required_opasswd' => array( 
			'label' => 'Old&nbsp;Password*' , 
			'style' => 'flabel align-right'  , 
			'msg' => ''),
		'pw_question' => array( 
			'label' => 'Security&nbsp;Question*' , 
			'style' => 'flabel align-right'  , 
			'msg' => ''),
		'pw_answer' => array( 
			'label' => 'Security&nbsp;Password*' , 
			'style' => 'flabel align-right'  , 
			'msg' => ''),			
		'required_xpasswd' => array( 
			'label' => 'Confirm&nbsp;Password*' , 
			'style' => 'flabel align-right'  , 
			'msg' => '')   
	);	
   
	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	
		if ( $_POST['required_opasswd'] != NULL && $_POST['required_npasswd'] != NULL && $_POST['required_npasswd'] == $_POST['required_xpasswd'] ) {
		
			if ( xmember_changepasswd( $_POST['required_opasswd'] , $_POST['required_npasswd'] , $_POST['required_npasswd'] ) ) {
					
				include( 'change-password-success.php' );
				
				exit;
					
			}
			else {
			
				$_POST['error'] = 'There was an error changing your passowrd.';
			}
		}
		else {
		
			$_POST['error'] = 'Errors Detected & Marked.';
        
			if ( $_POST['required_opasswd'] == NULL ) { $aForm['required_opasswd']['style'] = 'ferror'; $aForm['required_opasswd']['msg'] = 'missing'; }
			if ( $_POST['required_npasswd'] == NULL ) { $aForm['required_npasswd']['style'] = 'ferror'; $aForm['required_npasswd']['msg'] = 'missing'; }
			if ( $_POST['required_npasswd'] != $_POST['required_xpasswd'] ) { $_POST['error'] = 'Your new passwords do not match.'; }
			
		
		}

	}

	$cPageTitle = html_link( '/members/' . $aMember['handle'] , stripslashes( $aMember['handle'] ) ) . ' : Change Password' ;   	
   	
	include( '_header.php' ); 

?>	
	<h1><?=$cPageTitle ?></h1>
	<br />
	<br />
<hr />
<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" name="flogon">

Enter your current password, and new password (twice for verification) then click on 
Change Password. Passwords must be at least 6 characters and can be mixed case.<p /> 

	<div class="ferror"><?=$_POST['error'] ?></div>

	<table cellpadding="5" cellspacing="0" width="100%">

		<tr>
			<td class="<?=$aForm['required_opasswd']['style'] ?> right" width="150">
				<?=$aForm['required_opasswd']['label'] ?>
			</td>
			<td width="*">
				<input type="text" name="required_opasswd" value="<?=$_POST['required_opasswd'] ?>" class="finput">
			</td>
			<td width="150">
				<div class="ferror"><?=$aForm['required_opasswd']['msg'] ?></div>
			</td>
		</tr>
		
		<tr>
			<td class="<?=$aForm['required_npasswd']['style'] ?> right">
				<?=$aForm['required_npasswd']['label'] ?>
			</td>
			<td>
				<input type="password" name="required_npasswd" autocomplete="off" class="finput">
			</td>
			<td>
				<div class="ferror"><?=$aForm['required_npasswd']['msg'] ?></div>
			</td>
		</tr>
		
		<tr>
			<td class="<?=$aForm['required_xpasswd']['style'] ?> right">
				<?=$aForm['required_xpasswd']['label'] ?>
			</td>
			<td>
				<input type="password" name="required_xpasswd" autocomplete="off" class="finput">
			</td>
			<td>
				<div class="ferror"><?=$aForm['required_xpasswd']['msg'] ?></div>
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<h2 class="headline">Security Information</h2>
				<hr />
			</td>
		</tr>		
		
		<tr>
			<td colspan="3" class="fcontent">
				This is a question/answer combonation that we will use that to verify your account information.  If you forget your
				account access credentials.<b>WE DO NOT HAVE ACCESS TO YOUR PASSWORD AND CAN ONLY RESET IT.</b><p />
						
				<ul> 
    				<li>Choose a question only you know the answer to - make sure the question isn't associated with your password.<p />
					<li>Pick a question that can't be answered through research (for example, avoid your mother's maiden name, your birth date, your first or last name, your social security number, your phone number, your pet's name, etc.).<p />
					<li>Make sure your answer is memorable, but not easy to guess. Use an answer that is a complete sentence for even more security.<p /> 						
				</ul>
			</td>
		</tr>		

		<tr>
			<td class="<?=$aForm['pw_question']['style'] ?>">
				<?=$aForm['pw_question']['label'] ?>
			</td>
			<td class="fcontent" colspan="2">
				<input type="text" name="pw_question" value="<?=$_POST['pw_question'] ?>" class="finput full">
			</td>
		</tr>		

		<tr>
			<td class="<?=$aForm['pw_answer']['style'] ?>">
				<?=$aForm['pw_answer']['label'] ?>
			</td>
			<td class="fcontent" colspan="2">
				<input type="text" name="pw_answer" value="<?=$_POST['pw_answer'] ?>" class="finput full"><br />
			</td>
		</tr>				
		
		
		
		<tr>
			<td>
				&nbsp;
			</td>
			<td colspan="2">
				<a class="cSave" href="javascript:;" onclick="document.flogon.submit();" title="Save"><span>Save</span></a>
				<input type="image" name="sbtbtn" src="/_images/widget.gif" width="1" height="1" /> 
			</td>
		</tr>
	</table>	
</form>	

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
                    
<?php include( '_footer.php' ); ?>