<?php

	require_once( '_connect.php' ); // connect to tyfoon
	
	$aForm = array(
		'handle' => array( 'label' => 'Username/Email*' , 'style' => 'flabel'  , 'msg' => ''),
		'name_first' => array( 'label' => 'First Name*' , 'style' => 'flabel'  , 'msg' => ''),
		'pw_question' => array( 'label' => 'Secret Question' , 'style' => 'flabel' , 'msg' => ''),
		'pw_answer' => array( 'label' => 'Secret Answer*' , 'style' => 'flabel'  , 'msg' => ''),
		'name_last' => array( 'label' => 'Last Name*' , 'style' => 'flabel'  , 'msg' => '')
	);	
	
	$cPageTitle = 'Account Retrieval' ;

	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	
		$_POST['cn'] = substr( substr( substr( $_POST['cn'] , 0 ) , 8 ) , 0, -8 );
			
		if ( $_POST['step'] == 'answer' && $_POST['cn'] > 0 ) {
		
			$aAccount = sql_query( "SELECT memberid , pw_question, pw_resets, handle FROM members WHERE memberid='$_POST[cn]' LIMIT 1" );
			
			$aAnswer = sql_query( "SELECT memberid FROM members WHERE pw_answer RLIKE '^$_POST[pw_answer]$' AND pw_resets < 5 AND memberid='$_POST[cn]' LIMIT 1" );
			
			if ( $aAnswer['memberid'] > 0 ) {
			
				xmember_resetpasswd( $aAccount['memberid'] );
				
				$cPageTitle = 'Process Complete' ;
			
				include( 'account-retrieval-screen-3.php' );	

				exit;
			
			}
			else {
			
				mysql_query( "UPDATE members SET pw_resets=pw_resets+1 WHERE memberid='$aAccount[memberid]' LIMIT 1" );
				
				if ( $_POST['pw_answer'] == NULL ) {
				
					$_POST['error'] = 'You did not provide an answer to the question.';				
				
				}
				else {
				
					$_POST['error'] = 'The answer that you provided was not correct.';

				}
				
				$_POST['pw_answer'] = '';
				
				$cPageTitle = 'Answer your secret question?' ;
			
				include( 'account-retrieval-screen-2.php' );	

				exit;
			
			}
		
		}
		else {
	
			if ( $_POST['handle'] != NULL && $_POST['name_last'] != NULL && $_POST['name_first'] != NULL ) {
		
				alphanum( $_POST['handle'] ); 
			
				alphanum( $_POST['name_last'] ); 
			
				alphanum( $_POST['name_first'] ); 
			
				$aAccount = array();
			
				if ( is_valid_email_address( $_POST['handle'] ) ) {
			
					$aAccount = sql_query( "SELECT memberid , pw_question, pw_resets FROM members WHERE email RLIKE '^$_POST[handle]$' AND name_first RLIKE '^$_POST[name_first]$' AND name_last RLIKE '^$_POST[name_last]$' LIMIT 1" );
				
				}
				else {
			
					$aAccount = sql_query( "SELECT memberid , pw_question, pw_resets FROM members WHERE handle RLIKE '^$_POST[handle]$' AND name_first RLIKE '^$_POST[name_first]$' AND name_last RLIKE '^$_POST[name_last]$' LIMIT 1" );
				
				}
								
				if ( $aAccount['memberid'] > 0 ) {
				
					$cPageTitle = 'Answer your secret question?' ;
				
					include( 'account-retrieval-screen-2.php' );	

					exit;
			
				}
				else {
			
					$_POST['error'] = 'We were not able to locate your account.';
				
					$_POST['handle'] = '';
				
					$_POST['name_first'] = '';
				
					$_POST['name_last'] = '';

					include( 'account-retrieval-screen-1.php' );	

					exit;
				
				}
	
			}
			else {
		
				$_POST['error'] = 'Errors Detected & Marked.';
        
				if ( $_POST['handle'] == NULL ) { $aForm['handle']['style'] = 'ferror'; $aForm['handle']['label'] = 'Username/Email* missing'; }
			
				if ( $_POST['name_first'] == NULL ) { $aForm['name_first']['style'] = 'ferror'; $aForm['name_first']['label'] = 'First Name* missing'; }
			
				if ( $_POST['name_last'] == NULL ) { $aForm['name_last']['style'] = 'ferror'; $aForm['name_last']['label'] = 'Last Name* missing'; }
			
				include( 'account-retrieval-screen-1.php' );	
			
				exit;
		
			}
			
		}

	}
	else {
	
		include( 'account-retrieval-screen-1.php' );	
		
		exit;
	
	}