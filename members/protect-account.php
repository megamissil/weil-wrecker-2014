<?php

	require_once( '_connect.php' ); // connect to tyfoon
	
	$aForm = array(
		'handle' => array( 'label' => 'Username*' , 'style' => 'flabel'  , 'msg' => ''),
		'passwd' => array( 'label' => 'Password*' , 'style' => 'flabel'  , 'msg' => '')   
	);	
   	
	$aOutput['title'] = 'Protect your account';   	

	$aOutput['crumbtrail'] = html_link( '/' , 'Home' ) . ' : ' . $aOutput['title'];
		
	include( '_header.php' );

?>	

	<h1><?=$aOutput['title'] ?></h1>
			
	<p class="fcontent">

	The following is our effort to provide you with the knowledge and tools necessary to identify and 
	report threats to your account's safety, to spotlight ways in which we work to fulfill our security commitment, and 
	to act as a helpful resource in case someone manages to steal account information from you.

	</p>
	
	<p class="fcontent">

	The first question on the minds of most account theft victims is, "How did this happen?" It's a good question, and answering it is the first step in regaining control of your account and plugging any security holes that may exist.
	
	<br />&nbsp;<br />

	Should your account have been compromised, the following list of types of account theft should help shed some light on how your account was stolen and what steps you can take to prevent any further damage.	
	
	</p>
	
	<h3 class="headline">Sharing Account Information</h3>	
	
	<p class="fcontent">
	
	The simplest form of account theft occurs when a user willingly shares login information with another user. This person may be someone you know and trust in real-life, or someone using a tactic called "social engineering" to get your information.
	
	</p>

	<h3 class="headline">Phishing Emails</h3>	
	
	<p class="fcontent">
	
	The practice of "phishing" is the most common strategy used by account thieves. Many cases of people claiming they were "hacked" can actually be traced back to phishing schemes. These emails and websites pretend to be official communications to trick you into willingly handing out your login information. Here are some signs that you may be dealing with a phishing attempt:
	<ul>
		<li> Phishing emails will ask you for password or login information. 
		<li> Phishing emails will make urgent appeals in regards to your account being under investigation for hacking/cheating and ask that you provide personal information to avoid these penalties.
		<li> Phishing emails and websites may also make offers that seem too good to be true, such as early entry into beta tests, special promotions, etc.
		<li> Phishing emails frequently contain grammatical and/or spelling mistakes. If you can spot obvious typos and grammatical/syntactical errors, you are likely dealing with a phishing email.
		<li> Some phishing emails do not request personal information but instead link to fake websites that look exactly like our account management websites. Check to make sure your browser's phishing filter is activated.
		<li> Some phishing emails will mask, or "spoof" their sending address, making it appear as though the emails are being sent from us. Check the email's header information to verify the sending address.
	</ul>
	
	</p>
	
	<h3 class="headline">Malicious Websites</h3>	

	<p class="fcontent">

	Malicious websites target your browser's weak spots by attempting to install harmful software on your computer, 
	with end results similar to malicious add-ons. These sites may be obvious, or they may be designed to look exactly
	like our website. One easy way to tell is by looking closely at the site's URL. If it's not a <?=$_SERVER['HTTP_HOST'] ?>
	but the site claims to be official, you are probably dealing with a malicious website. 
	
	</p>
	
	<p>&nbsp</p>
	
	<p>&nbsp</p>
					
<?php include( '_footer.php' ); ?>