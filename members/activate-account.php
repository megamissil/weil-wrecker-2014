<?php

	require_once( '_connect.php' ); // connect to tyfoon
	
	$cPageTitle = 'Account activated';
	
	include( '_header.php' );
		
?>	
	<h1><?=$cPageTitle ?></h1>
	<br />
	<br />

	
	<div class="fcontent">
	
	Your account is now activated and you are able to <a href="logon.php">logon</a>.
	
	</div>
	
<?php

	include( '_footer.php' );

?>