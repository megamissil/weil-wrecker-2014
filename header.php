<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/tyfoon/connect.php';

    $cSiteName = 'Weil Wrecker | Pro Tow';
    $cSiteCity = 'Birmingham';
    $cSiteState = 'AL';

    $display_login       = false;
    $is_dealer_subdomain = false;

    if(substr($_SERVER['SERVER_NAME'], 0, 6) == 'dealer') {
        $is_dealer_subdomain = true;

        if($member === false || $member['memberid'] == 0) {
            $display_login = true;
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="<?php echo $cMetaDesc; ?>" name="description">
    <meta content="<?php echo $cMetaKW; ?>" name="keywords">
    <title><?php if (!empty($cSEOTitle)) { echo $cSEOTitle; } else if (!empty($cPageTitle)) { ?><?=$cSiteName; ?> – <?php echo $cPageTitle; ?><?php } else { ?><?=$cSiteName; ?> – <?=$cSiteCity; ?>, <?=$cSiteState; ?><?php } ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/avicon.ico" type="image/x-icon">
    <link href="/stylesheets/styles.css" rel="stylesheet" type="text/css">
    <link href="/stylesheets/slick.css" rel="stylesheet" type="text/css">
    <link href="/stylesheets/zeekee-support.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/stylesheets/colorbox.css">
    <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700|Oswald:400,700' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="/js/modernizr.custom.75823.js"></script>
    
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-54682403-1', 'auto');
		  ga('send', 'pageview');

		</script>



</head>
<body>
<header role="banner">
    <section class="container header-contact">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <a href="index.php"><img src="/img/weil-logo.png" alt="weil wrecker logo" /></a>
            </div>
            <div class="col-md-4 hidden-xs col-sm-4">
                <p class="serving-since"><span>(205) 251-4060</span><br>
                Serving the Greater Birmingham<br> Area Since 1977</p>
            </div>
            <div class="col-xs-6 col-sm-4">
                <?php if($display_login): ?>
                    <a href="/members/logon.php">
                        <button type="submit" class="button radius form-btn pull-right">Sign in</button>
                    </a>
                <?php else: ?>
                    <?php if($is_dealer_subdomain): ?>
                        <div class="faq-side">
                            <a href="#"><span><nobr>WELCOME</nobr></span></a> 
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                            <div style="color: white; font-weight: bold; font-size: 18px;">Welcome, <?= $member['name_first'] ?></div>
                            <ul>        
                                <li><a href="/members/">My Wishlist</a></li>
                                <li><a href="/members/logout.php">Logout</a></li>
                            </ul>
                            </div>
                        </div>
                    <?php else: ?>
                        <a href="index.php"><img src="/img/protow-logo.png" alt="protow logo" class="pull-right" /></a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row visible-xs"><!-- MOBILE LAYOUT HEADER -->
            <div class="col-xs-10 col-center">
                <p class="serving-since"><span>(205) 251-4060</span><br>
                Serving the Greater Birmingham<br> Area Since 1977</p>
            </div>
        </div>
    </section>
    <section class="main-nav">
        <nav class="navbar navbar-default" role="navigation">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="/index.php">Home</a></li>
                <li><a href="/towing.php">Towing</a></li>
                <li><a href="/recovery.php">Recovery</a></li>
                <li><a href="/storage.php">Storage</a></li>
                <li><a href="/impounds.php">Impounds</a></li>
                <li><a href="/auctions/">Auctions</a></li>
                <li><a href="/services.php">Services</a></li>
                <li><a href="/contact-us.php">Contact</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </section>
</header>
