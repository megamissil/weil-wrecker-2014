<?php
	include('postman/_variables2.php');
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet( 69 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Satisfaction Survey';
	$cSEOTitle = '';
	$layout = 'sub';
?>

<?php
	include("header.php");
?>
<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <div class="content-box" id="survey-box">
                    <h1><span class="survey-icon"><img src="img/survey-icon.png" alt="welcome icon" /></span>Survey</h1>
                    <div class="survey">
            			<?=$aPage['msg']; ?>
            		</div>
                    	<div class="row">
                    	 <div class="col-md-12 col-sm-12">

                   		
		                	<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
		                	<div class="row">
		            			<div class="col-md-6">
							  		<div class="form-group">
							    		<label for="name">Name: (optional)</label>
							    		<input type="text" class="form-control" id="name" placeholder="Name" value="<?=$_POST['name'] ?>">
							  		</div>
							  	</div>
		            			<div class="col-md-6">
						  			<div class="form-group">
						   				<label for="email">Email:</label>
						   				<input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="<?=$_POST['email'] ?>">
						  			</div>
						  		</div>
							 </div>
							 <div class="row">
		            			<div class="col-md-6">
							  		<div class="form-group">
							  		<h4>Towing Experience:</h4>
							  			<input type="radio" class="form-control" id="very" name="experience" value="Very Satisfied">
							    		<label for="very">Very Satisfied</label>
							    		<input type="radio" class="form-control" id="satisfied" name="experience" value="Satisfied">
							    		<label for="satisfied">Satisfied</label>
										<input type="radio" class="form-control" id="neutral" name="experience" value="Neutral">
										<label for="neutral">Neutral</label>
										<input type="radio" class="form-control" id="un" name="experience" value="Unsatisfied">
										<label for="un">Unsatisfied</label>
										<input type="radio" class="form-control" id="veryun" name="experience" value="Very Unsatisfied">
										<label for="veryun">Very Unsatisfied</label>
							  		</div>
							  	</div>
		            			<div class="col-md-6">
							  		<div class="form-group">
							  		<h4>Customer Service:</h4>
							  		    <input type="radio" class="form-control" id="very2" name="service" value="Very Satisfied">
							    		<label for="very2">Very Satisfied</label>
							    		<input type="radio" class="form-control" id="satisfied2" name="service" value="Satisfied">
							    		<label for="satisfied2">Satisfied</label>
										<input type="radio" class="form-control" id="neutral2" name="service" value="Neutral">
										<label for="neutral2">Neutral</label>
										<input type="radio" class="form-control" id="un2" name="service" value="Unsatisfied">
										<label for="un2">Unsatisfied</label>
										<input type="radio" class="form-control" id="veryun2" name="service" value="Very Unsatisfied">
										<label for="veryun2">Very Unsatisfied</label>
							  		</div>
							  	</div>
							  </div>
							  <div class="row">
		            			<div class="col-md-8">
							  		<div class="form-group">
							    		<label for="survey-comment"><h4>Comments:</h4></label>
							    		<textarea name="msg" id="survey-comment" class="form-control" value="<?=$_POST['msg'] ?>"></textarea>
							  		</div>
							  	</div>
							  </div>
							  <button type="submit" name="sbmtbtn" id="sbmtbtn" class="button form-btn radius">Submit</button>
							</form>
							</div>
	                    </div>
					</div>
				</div>

			<aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include('sidebar-sub.php'); ?>
                </section>
            </aside>
      	</div>
    </section>
</main>
<?php include('footer.php'); ?>