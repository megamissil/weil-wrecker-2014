<?php
include $_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php';  
$cMetaDesc = '';
$cMetaKW = '';
$cPageTitle = 'Auctions';
$cSEOTitle = '';
$layout = 'home';
$products = productByCategory ( '','','','ASC', $_GET['page']*12, 400);	 

//
// this variable is used to restrict calls to only include unexpired products.
//
$today = date('Y-m-d');


// -- MySQL commands

include '../header.php'; ?>

<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="/img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    <h1><span class="headline-icon"><img src="/img/auction-icon.png" alt="towing icon" /></span><?=$cPageTitle ?></h1>
                    <div class="row auction-top">
                        <!-- START SUB CONTENT -->                        
                        <div class="col-xs-6">
                          	<form action="" method="GET" class="auction-search">
                          		<div class="col-xs-6 col-md-4">
									<label>
										Year
										<select name="year" id="year" class="form-control auction-select">
										<option value="">All</option>
										<?php 														
										for ( $i = date("Y"); $i >= (date("Y")-75); $i-- ) {   	
											echo '<option value ="' . $i . '"';      	
											echo ( $_GET['year'] == $i ) ? ' selected="selected"' : false;
											echo '>' . $i . '</option>';
			   
										}							
										?>
										</select> 
									</label>
								</div>
	                          	<div class="col-xs-6 col-md-4">
									<label>
										Make
										<select name="make" id="make" class="form-control auction-select">
										<option value="">All</option>
										<?php 
										$search = '';
										sql_quote($_GET);
										!empty($_GET['year']) ? $search .= " AND addl_addl_year2 = '$_GET[year]'" : false;

										$makes = mysql_query(
											"SELECT DISTINCT(addl_add_make) 
											FROM products_additional_values 
											WHERE addl_add_make != '' 
											$search 
											AND STR_TO_DATE( addl_expires, '%Y-%m-%d' ) >= '$today' 
											ORDER BY addl_add_make");
										
										while($row = mysql_fetch_object($makes)) {
											echo '<option value="' . strtoupper($row->addl_add_make) . '"';
											echo strtoupper($_GET['make']) == strtoupper($row->addl_add_make) ? ' selected="selected"' : false;
											echo '>' . propercase($row->addl_add_make) . '</option>';
										}
										?>
										</select> 
									</label>
								</div>
								
								<div class="col-xs-6 col-md-4">
									<label>
										Model										
										<select name="model" id="model" class="form-control auction-select">
										<option value="">All</option>
										<?php
										if (!empty($_GET['make'])) { 
											$search .= " AND addl_add_make RLIKE '^$_GET[make]'";
											$models = mysql_query("
											SELECT DISTINCT(addl_addl_model) 
											FROM products_additional_values 
											WHERE addl_addl_model != '' 
											AND STR_TO_DATE( addl_expires, '%Y-%m-%d' ) >= '$today' 
											$search
											ORDER BY addl_addl_model");							
											while($row = mysql_fetch_object($models)) {
												echo '<option value="' . strtoupper($row->addl_addl_model) . '"';
												echo strtoupper($_GET['model']) == strtoupper($row->addl_addl_model) ? ' selected="selected"' : false;
												echo '>' . propercase($row->addl_addl_model) . '</option>';
											}
										}										
										?>										
										</select> 
									</label>
								</div>
								<div class="col-xs-6 col-md-4">
									<label>
										Style
										<select name="style" id="style" class="form-control auction-select">
										<option value="">All</option>
										<?php
										if (!empty($_GET['model'])) { 
											$search .= " AND addl_addl_model RLIKE '^$_GET[model]'";
											$styles = mysql_query("
											SELECT DISTINCT(addl_addl_type) 
											FROM products_additional_values 
											WHERE addl_addl_type != '' 
											AND STR_TO_DATE( addl_expires, '%Y-%m-%d' ) >= '$today' 
											$search 
											ORDER BY addl_addl_type");
										
											while($row = mysql_fetch_object($styles)) {
												echo '<option value="' . strtoupper($row->addl_addl_type) . '"';
												echo strtoupper($_GET['style']) == strtoupper($row->addl_addl_type) ? ' selected="selected"' : false;
												echo '>' . propercase($row->addl_addl_type) . '</option>';
											}
										}
										?>											
										</select> 
									</label>
								</div>
								<div class="col-xs-6 col-md-4">
									<label>
										Type
										<select name="type" id="type" class="form-control auction-select">
										<option value="">All</option>
										<?php
										if(!empty($_GET['style'])) { 
											$search .= " AND addl_addl_type RLIKE '^$_GET[style]'";
											$types = mysql_query("
												SELECT DISTINCT(addl_type) 
												FROM products_additional_values 
												WHERE addl_type != '' 
												AND STR_TO_DATE( addl_expires, '%Y-%m-%d' ) >= '$today' 
												$search 
												ORDER BY addl_type");

											while($row = mysql_fetch_object($types)) {
												echo '<option value="' . strtoupper($row->addl_type) . '"';
												echo strtoupper($_GET['type']) == strtoupper($row->addl_type) ? ' selected="selected"' : false;
												echo '>' . propercase($row->addl_type) . '</option>';
											}
										}
										?>									
										</select> 
									</label>
								</div>
								<div class="col-xs-6 col-md-7">
									<input type="submit" value="Search" class="button" id="search-btn">
								</div>
							</form>
						</div>
						<div class="col-xs-6">
							<p class="strip-padding hidden-sm hidden-xs">Download Auction Lists</p>
							<div class="col-md-6 hidden-sm hidden-xs">
	                            <a href="/auctions/current-auction.pdf" class="button-icon radius" target="_blank"><sup class="lowercase"></sup>Auction List<span><img src="/img/pdf-custom-icon.svg" alt="" /></span></a>
	                            <br class="hidden-sm"><br class="hidden-sm">

	                            <p class="strip-padding"></p>
	                            <a href="/auctions/current-auction.xls" class="button-icon radius" target="_blank"><sup class="lowercase"></sup>Auction List<span><img src="/img/excel-download.png" alt="" /></span></a>
	                            <br class="hidden-sm"><br class="hidden-sm">
	                        </div>
	                        <div class="col-sm-6 col-center visible-sm visible-xs">
	                            <h4 class="strip-padding">Download Auction Lists</h4>
	                            <a href="/auctions/current-auction.pdf" class="button-icon radius" target="_blank"><sup class="lowercase"></sup>Auction List<span><img src="/img/pdf-custom-icon.svg" alt="" /></span></a>
	                            <br class="hidden-sm"><br class="hidden-sm">

	                            <p class="strip-padding"></p>
	                            <a href="/auctions/current-auction.xls" class="button-icon radius" target="_blank"><sup class="lowercase"></sup>Auction List<span><img src="/img/excel-download.png" alt="" /></span></a>
	                            <br class="hidden-sm"><br class="hidden-sm">
	                        </div>
	                        <div class="col-md-6 hidden-sm hidden-xs">
								<a href="../img/calendar-auction-web-2015.pdf" target="_blank"><img src="/img/calendar-auction-web-2015.png" alt="calendar" border="2" align="middle" longdesc="/img/calendar-auction-web-2015.png" class="auction-cal"></a>	                        </div>
<!-- 	                        <div class="col-sm-6 col-center visible-sm visible-xs"><a href="../calendar-auction-web-2015.pdf"><a href="../calendar-auction-web-2015.pdf"><img src="/img/calendar-auction-web-2015.png" alt="calendar" border="1" align="middle" longdesc="/img/Calendar-Auction.png" /></a></a></div> -->
					  	</div>
					</div>

					<div class="row auction-vehicles">
						<?php
						$filter = '';

						($_GET['year'] > 1900) ? $filter .= " AND pav.addl_addl_year2 = '$_GET[year]'" : false;

						!empty($_GET['make']) ? $filter .= " AND pav.addl_add_make RLIKE '^$_GET[make]'" : false;

						!empty($_GET['model']) ? $filter .= " AND pav.addl_addl_model RLIKE '^$_GET[model]'" : false;

						!empty($_GET['style']) ? $filter .= " AND pav.addl_addl_type RLIKE '^$_GET[style]'" : false;

						!empty($_GET['type']) ? $filter .= " AND pav.addl_type RLIKE '^$_GET[type]'" : false;

						$today = date('Y-m-d');

						$auctions = mysql_query(
							"SELECT pav.*, p.*, pp.* FROM products_additional_values pav 
							LEFT JOIN products p ON (pav.productid = p.productid)
							LEFT JOIN products_photos pp ON (pp.productid = pav.productid AND pp.sortorder = 1)
							WHERE p.active = 1 
							AND STR_TO_DATE( pav.addl_expires, '%Y-%m-%d' ) >= '$today'
							$filter
							ORDER BY pav.addl_addl_year2"
							); 

						while ($auction = mysql_fetch_object($auctions)) { ?>
							<div class="col-md-6">
								<div class="auction-list">
									<ul class="small-blocks">
										<li>
											<figure>
												<div class="image">
													<div class="capwrapper">
														<span id="sku">
															<?php 
																echo html_link('auction.php?cn=' . $auction->sku, $auction->sku . ' ') ;
															?>
														</span>
													</div>
													<?php 
														$img = '/tyfoon/site/products/images/' . $auction->photo_path;
														$thumb = '/tyfoon/site/products/thumbs/' . $auction->thumb_path;
														echo html_link('auction.php?cn=' . $auction->sku, '<img class="auctions-img" src=' . $thumb . '>') ;
													?>
												</div>

												<figcaption class="hidden-xs hidden-sm">
													<?php
														echo html_link('auction.php?cn=' . $auction->sku, $auction->addl_addl_year2 . ' ' . $auction->addl_add_make . ' ' . $auction->addl_addl_model . ' ') ;
													?>
												</figcaption>
											</figure>	
										</li>
									</ul>
								</div>
							</div>
						<?php } ?>								
                </div>               
            </div>
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include '../sidebar-sub.php'; ?>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="/survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script defer="defer">
$(document).ready(function() {
	
	$('#year').change(function(){
		$.get( 'makes.php', {year: $(this).val()},
			function(data) {                    
				$('#make').html(data);
				$('#model').html('<option value="">All</option>');
				$('#style').html('<option value="">All</option>');
				$('#type').html('<option value="">All</option>');
			}            
		);    
	});        	

	$('#make').change(function(){
		$.get( 'models.php', {year: $('#year').val(), make: $(this).val()},
			function(data) {                    
				$('#model').html(data);
				$('#style').html('<option value="">All</option>');
				$('#type').html('<option value="">All</option>');
			}            
		);    
	});        

	$('#model').change(function(){
		$.get( 'styles.php', {year: $('#year').val(), make: $('#make').val(), model: $(this).val()},
			function(data) {                    				
				$('#style').html(data);
				$('#type').html('<option value="">All</option>');
			}            
		);    
	});        

	$('#style').change(function(){
		$.get( 'types.php', {year: $('#year').val(), make: $('#make').val(), model: $('#model').val(), style: $(this).val()},
			function(data) {                    								
				$('#type').html(data);
			}            
		);    
	});        

});
</script>

<?php include '../footer.php';
