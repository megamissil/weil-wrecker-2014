<?php
include $_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php';  
$product = productGet($_GET['cn']);
$cMetaDesc = '';
$cMetaKW = '';
$cPageTitle = 'Auctions';
$cSEOTitle = '';
$layout = 'home';

// -- MySQL commands

include '../header.php'; ?>

<script>
    jQuery(document).ready(function () {
        jQuery('a.gallery').colorbox({ 
            opacity:0.5, 
            rel:'group1',
            width: "50%",
            close:'Exit'
             });
    });
</script>

<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="/img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    <h1>
                        <span class="headline-icon"><img src="/img/auction-icon.png" alt="towing icon" /></span>
                        <?php echo $product['addl_addl_year2']; ?>  <?php echo $product['addl_add_make']; ?>  <?php echo $product['addl_addl_model']; ?>
                    </h1>
                    <div class="row">
                        <!-- START SUB CONTENT -->
                        <div class="col-md-7">
                            <a class="gallery" href="<?=$product['images'][1]['image']; ?>"><img class="vehicle-img" src="<?=$product['images'][1]['image']; ?>" alt="vehicle"></a>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-5">
                                    <a href="/auctions/"><button type="submit" name="sbmtbtn" id="sbmtbtn" class="auctions-btn radius form-btn">Back to Auctions</button></a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-7">
                                    <a class="home-btn about-btn radius" href="/">Back to Home</a>
                                </div>
                            </div>
                            <?php 
                            if ($member['memberid'] > 0) { 
                                $id = mysql_fetch_object(mysql_query("SELECT productid FROM members_wishlist WHERE productid = '$product[productid]' AND memberid = '$member[memberid]' LIMIT 1"));
                            ?>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-5">
                                    <?php 
                                    if ($id->productid > 0) {
                                        echo '<a href="./remove-from-wishlist.php?cn=' . $product['sku'] . '" class="auctions-btn radius form-btn">Remove from Wishlist</a>';
                                    } else {
                                        echo '<a href="./add-to-wishlist.php?cn=' . $product['sku'] . '" class="auctions-btn radius form-btn">Add to Wishlist</a>';
                                    }
                                    ?>
                                    
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-7">
                                    &nbsp;                                    
                                </div>
                            </div>
                            <?php 
                            }
                            ?>
                        </div>

                        <div class="col-md-5">
                            <div class="info-container">                        
                                <div class="vehicle-info">
                                    <?php /* dumpVariable($product); */ ?>
                                    <p><span>Year:</span> <?php echo $product['addl_addl_year2']; ?></p>
                                    <p><span>Make:</span> <?php echo $product['addl_add_make']; ?></p>
                                    <p><span>Model:</span> <?php echo $product['addl_addl_model']; ?></p>
                                    <p><span>Color:</span> <?php echo $product['addl_exterior_color']; ?></p> 
                                    <p><span>Style:</span> <?php echo $product['addl_addl_type']; ?></p>
                                    <p><span>VIN:</span> <?php echo $product['addl_vin']; ?></p>
                                    <p><span>Barcode:</span> <?php echo $product['addl_barcode']; ?></p>
                                    <p><span>Auction#:</span> <?php echo $product['addl_auction']; ?></p>
                                    <p><span>Type:</span> <?php echo $product['addl_type']; ?></p>
                                    <p><span>Title:</span> <?php echo $product['addl_addl_title']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div>
                <div class="content-box" id="auction-info-box">
                    <h2 class="center-text">Auction Information</h2>
                    <p class="center-text">Auctions are held the 1<sup>st</sup> and 3<sup>rd</sup> of each month at:<br>
                        <span class="red-text">Weil Wrecker Service - 3401 Powell Ave, Birmingham AL 35222</span>
                    </p>
                        <div class="auction-info">

                            <p><strong>Regular Auction Vehicles</strong></p>
<ul>
                                <li>All vehicles are sold As-Is</li>
                                <li>Auctions are open to the public</li>
                                <li>No buyer's or entrance fees</li>
                                <li>Vehicles must be paid for immediately following the auction</li>
                                <li>Vehicles must be picked up by Wednesday of the following weeek</li>
                            </ul>
                            <p><strong>US Marshal Auction Vehicles</strong></p>
<ul>
                                <li>Sold at the beginning of each auction</li>
                                <li>$500 deposit is required to submit bid (applied to winning bid or returned)</li>
                                <li>All final bids must be approved by the US Marshal Service</li>
                                <li>USMS vehicles may be viewed on Monday & Tuesday prior to the sale</li>
                                <li>Telephone bids may be prearranged with Steve Weil at 205-251-4060</li>
                            </ul>
                        </div>
              </div>
                <div class="content-box-append hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-md-7 col-lg-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-md-5 col-lg-4">
                            <a href="/survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append-touch visible-xs"><!--MOBILE SURVEY SECTION -->
                    <a href="/survey.php">
                        <h2>Happy with your experience?</h2>
                    </a>
                </div>               
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include '../sidebar-sub.php'; ?>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include '../footer.php';
