<p>Weil Wrecker Satisfaction Survey<p />
<table>
		<tr>
		<td width="15%" align="right">
			Name:
		</td>
		<td width="85%">
			<?=$aOutput['name'] ?>
		</td>
	</tr>
	<tr>
		<td align="right">
			Email:
		</td>
		<td>
			<?=$aOutput['email'] ?>
		</td>
	</tr>
	<tr>
		<td align="right">
			Towing Experience:
		</td>
		<td>
			<?=nl2br( $aOutput['experience'] ) ?>
		</td>
	</tr>
	<tr>
		<td align="right">
			Customer Service:
		</td>
		<td>
			<?=nl2br( $aOutput['service'] ) ?>
		</td>
	</tr>
	<tr>
		<td align="right">
			Comments:
		</td>
		<td>
			<?=( $aOutput['msg'] ) ?>
		</td>
	</tr>
</table>