<?php
include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
  $aPage = pageGet( 70 );
  $aFeatured = pageByCategory( 'AUCTION' , 'ANY' , 0 , 999 , 'PUBL_ASC');
    $cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Auctions';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php include('header.php'); ?>

<main class="container">
    <section class="sub-header-img hidden-xs"><!-- START SLICK SLIDER -->
        <div class="row">
            <div class="col-xs-12">
                <img src="img/sub-header-img.jpg" alt="" width="100%" />
            </div>
        </div>
    </section><!-- END SLICK SLIDER -->

    <!-- START SUB CONTENT SECTION -->
    <section class="sub-inner-container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content-box">
                    <h1><span class="headline-icon"><img src="img/auction-icon.svg" alt="welcome icon" /></span>Upcoming Auctions</h1>
                  <div class="row">
                        <!-- START SUB CONTENT -->
                        <div class="col-md-9">
                            <?php echo $aPage['msg']; ?>
                        </div>
                        <!-- START PDF AUCTION LISTS -->
                        <div class="col-md-3 hidden-sm hidden-xs">
                            <p class="strip-padding">Download Auction Lists</p>
                            <a href="auctions/auctionlist150107.pdf" class="button-icon radius">Jan 7th, 2015<span><img src="img/pdf-custom-icon.svg" alt="" /></span></a>
                            <br class="hidden-sm"><br class="hidden-sm">
                        </div>
                        <div class="col-sm-6 col-center visible-sm visible-xs">
                            <p class="strip-padding">Download Auction Lists</p>
                            <a href="auctions/auctionlist150107.pdf" class="button-icon radius">Jan 7th, 2015<span><img src="img/pdf-custom-icon.svg" alt="" /></span></a>
                            <br class="hidden-sm"><br class="hidden-sm">
                        </div>
                        <div class="col-md-3 hidden-sm hidden-xs">
                            <p class="strip-padding"></p>
                            <a href="auctions/auctionlist150107.xls" class="button-icon radius">Jan 7th, 2015<span><img src="img/excel-download.png" alt="" /></span></a>
                            <br class="hidden-sm"><br class="hidden-sm">
                        </div>
                    <div class="col-sm-6 col-center visible-sm visible-xs">
                            <p class="strip-padding"></p>
                            <a href="auctions/auctionlist150107.xls" class="button-icon radius">Jan 7th, 2015<span><img src="img/excel-download.png" alt="" /></span></a>
                            <br class="hidden-sm"><br class="hidden-sm">
                    </div>
                    <p class="strip-padding"></p>
                    <p class="strip-padding"></p>
                        <a href="/calendar-auction-web.pdf"><a href="/calendar-auction-web.pdf"><img src="/img/Calendar-Auction.png" alt="calendar" border="1" align="middle" longdesc="/img/Calendar-Auction.png"></a></div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="auction-list">
                          <a name="vehicles" id="#vehicles"><h3>Current Vehicles for Auction</h3></a>
                            <div align="center">
                              <p><img src="/img/auction/No-Auction.png" border="0">                            </p>
                              <p>&nbsp;</p>
                            </div>
                            <ul class="small-blocks">
                                    <?php 
                                        foreach( $aFeatured as $cKey => $aValue ) {
                                        $aPageID = pageGet ($aValue[pageid]);
                                        $aPageImage = $aPageID['images'][1];
                                    ?>
                                        <li>
                                            <a href="<?=$aValue['url'] ?>" class="auction-item">
                                                <figure>
                                                    <div class="image">
                                                        <img src="/tyfoon/site/pages/images/<?=$aPageImage['photo_path']?>" alt="vehicle">                                                    </div>
                                                    <figcaption class="hidden-xs hidden-sm">
                                                        <div class="capwrapper">
                                                            <h3><?php echo $aValue['title']; ?></h3>
                                                                <?php echo $aValue['msg_short']; ?>                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                            <p style="color: #FFFFFF">Auction List</p>
                                        </li>
                                    <?php } ?>
                          </ul>
                        </div>
                    </div>

                    <!-- START AUCTION SLIDER -->
                    <!-- <div class="row">
                        <div class="col-xs-12">
                            <div class="auction-slider-wrapper">
                                <h2 class="headline-sub">Vehicles of Interest</h2>
                                <div>
                                    <img src="img/auction-vehicle-slider-1.jpg" alt="" />
                                </div>
                                <div>
                                    <img src="img/auction-vehicle-slider-1.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row --> 
                    <!-- END AUCTION SLIDER -->
                </div>
                <div class="content-box-append hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-md-7 col-lg-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-md-5 col-lg-4">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
                <div class="content-box-append-touch visible-xs"><!--MOBILE SURVEY SECTION -->
                    <a href="#">
                        <h2>Happy with your experience?</h2>
                    </a>
                </div>
            </div>
            <aside class="col-md-3 col-sm-4">
                <section class="sidebar">
                    <?php include('sidebar-sub.php'); ?>
                </section>
            </aside>
        </div><!-- /.row -->

        <!--TABLET SURVEY SECTION -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-box-append hidden-xs visible-sm">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Happy with your experience?<div class="hidden-xs"> Tell Us!</div></h2>
                        </div>
                        <div class="col-sm-4">
                            <a href="survey.php" class="button radius">Satisfaction Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include('footer.php'); ?>