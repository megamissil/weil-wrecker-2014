$(document).ready(function(){

    // SLICK SLIDER

    $('.slider-wrapper').slick({
        arrows: true,
        draggable: true,
        fade: true,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 700
    });

    $('.auction-slider-wrapper').slick({
        arrows: true,
        draggable: true,
        fade: true,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 700,
        dots: true
    });

     $('.sub-slider').slick({
        arrows: false,
        dots: false,
        draggable: true,
        fade: true,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 700
    });

});
